package br.com.mercadodemusica.dao;

import java.math.BigInteger;
import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Compra;

public interface CompraDao extends Dao<Compra> {

	List<Compra> obterComprasPorIdComprador(BigInteger idComprador, BigInteger limiteDeCompras);

	List<Compra> obterComprasPorIpComprador(String ipComprador, BigInteger limiteDeCompras);

}
