package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.HistoricoDatasDeCompra;

public interface HistoricoDatasDeCompraDao extends Dao<HistoricoDatasDeCompra>{

}
