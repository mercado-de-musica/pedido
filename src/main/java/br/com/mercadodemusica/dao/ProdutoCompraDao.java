package br.com.mercadodemusica.dao;

import java.util.List;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.Compra;
import br.com.mercadodemusica.entities.Produto;
import br.com.mercadodemusica.entities.ProdutoCompra;

public interface ProdutoCompraDao extends Dao<ProdutoCompra>{

	ProdutoCompra obterPorProduto(Produto produto);

	List<ProdutoCompra> obterPorCompra(Compra compra);

	List<ProdutoCompra> obterListaPorProduto(Produto produto);

}
