package br.com.mercadodemusica.dao;

import br.com.mercadodemusica.daoGeneric.Dao;
import br.com.mercadodemusica.entities.PorcentagemMercadoDeMusica;

public interface PorcentagemMercadoDeMusicaDao extends Dao<PorcentagemMercadoDeMusica> {

	PorcentagemMercadoDeMusica obterPorcentagemAtiva();

}
