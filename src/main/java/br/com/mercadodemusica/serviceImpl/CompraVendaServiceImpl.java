package br.com.mercadodemusica.serviceImpl;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.Mapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mercadodemusica.acessoHttp.HTTPRequestResponse;
import br.com.mercadodemusica.dao.CompraDao;
import br.com.mercadodemusica.dao.ProdutoCompraDao;
import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.CompraDTO;
import br.com.mercadodemusica.dto.PrecoDTO;
import br.com.mercadodemusica.dto.ProdutoCompraDTO;
import br.com.mercadodemusica.dto.ProdutoDTO;
import br.com.mercadodemusica.entities.Compra;
import br.com.mercadodemusica.entities.Produto;
import br.com.mercadodemusica.entities.ProdutoCompra;
import br.com.mercadodemusica.enums.ModuloSistemaEnum;
import br.com.mercadodemusica.exceptions.ProdutoException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.properties.EncryptConstantes;
import br.com.mercadodemusica.properties.UrlModulo;
import br.com.mercadodemusica.service.CompraVendaService;
import br.com.mercadodemusica.transformacaoDeDados.Encrypt;
import br.com.mercadodemusica.transformacaoDeDados.Precos;

@Service
public class CompraVendaServiceImpl implements CompraVendaService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private Mapper dozerBeanMapper;
	
	@Autowired
	private ProdutoCompraDao produtoCompraDao;
	
	@Autowired
	private CompraDao compraDao;
	
	@Autowired
	private HttpServletRequest request;
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private static UrlModulo urlModuloProduto = new UrlModulo(ModuloSistemaEnum.PRODUTO);
	
	private static final BigInteger LIMITE_DE_COMPRAS = BigInteger.valueOf(20);
	
	private final Logger logger = LogManager.getLogger(CompraVendaServiceImpl.class);
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void insercaoCompra(ProdutoCompraDTO produtoCompraDto) {
		
		ProdutoCompra produtoCompra = dozerBeanMapper.map(produtoCompraDto, ProdutoCompra.class);
		compraDao.save(produtoCompra.getCompra());
		produtoCompraDao.save(produtoCompra);
	}

	@Override
	public List<ApresentacaoProdutoDTO> obterComprasEfetuadasPelosUsuariosPorProduto(String idProdutoCrypt) throws ProdutoException, ClientProtocolException, JSONException, URISyntaxException, IOException, RegraDeNegocioException {
		if(idProdutoCrypt == null) {
			throw new ProdutoException("O id não pode ser vazio");
		}
		
		
		BigInteger idProduto = new BigInteger(Encrypt.decrypt(EncryptConstantes.KEY, EncryptConstantes.INIT_VECTOR, idProdutoCrypt));
		
		Produto produto = new Produto();
		produto.setId(idProduto);
		
		List<ProdutoCompra> listaDeProdutoCompra = produtoCompraDao.obterListaPorProduto(produto);		
		
		for(ProdutoCompra produtoCompra : listaDeProdutoCompra) {
			Compra compra = compraDao.selectOne(produtoCompra.getCompra().getId());
			
			List<CompraDTO> listaDeComprasDto = null;
			if(compra.getIdComprador() != null) {
				listaDeComprasDto = this.obterComprasPorIdComprador(compra.getIdComprador());
				if(listaDeComprasDto != null) {
					List<ProdutoDTO> listaDeProdutosDto = this.obterListaDeProdutosPorCompra(listaDeComprasDto);
					
					List<ApresentacaoProdutoDTO> listaDeApresentacaoRetornoDto = new ArrayList<ApresentacaoProdutoDTO>();
					for(ProdutoDTO produtoDto : listaDeProdutosDto) {
						if(!idProduto.equals(produtoDto.getId()) && listaDeApresentacaoRetornoDto.size() < 8) {
							
							ApresentacaoProdutoDTO apresentacaoDto = this.obterApresentacoesPorProduto(produtoDto);
							
							
							apresentacaoDto.getProduto().setIdCrypt(Encrypt.encrypt(EncryptConstantes.KEY, EncryptConstantes.INIT_VECTOR, apresentacaoDto.getProduto().getId().toString()));
							
							PrecoDTO precoDto = this.buscarPreco(apresentacaoDto.getProduto());
							apresentacaoDto.getProduto().setPreco(precoDto.getPreco());
							apresentacaoDto.getProduto().setPrecoString(new Precos().padraoAmericanoParaBrasileiro(precoDto.getPreco()));
							
							
							listaDeApresentacaoRetornoDto.add(apresentacaoDto);
						}
						
					}
					
					return listaDeApresentacaoRetornoDto;
					
				} else {
					return null;
				}
			} else {
				listaDeComprasDto = this.obterComprasPorIpComprador(compra.getIpComprador());
				if(listaDeComprasDto != null) {
					List<ProdutoDTO> listaDeProdutosDto = this.obterListaDeProdutosPorCompra(listaDeComprasDto);
					
					List<ApresentacaoProdutoDTO> listaDeApresentacaoRetornoDto = new ArrayList<ApresentacaoProdutoDTO>();
					for(ProdutoDTO produtoDto : listaDeProdutosDto) {
						if(!idProduto.equals(produtoDto.getId()) && listaDeApresentacaoRetornoDto.size() < 8) {
							
							produtoDto.setIdCrypt(Encrypt.encrypt(EncryptConstantes.KEY, EncryptConstantes.INIT_VECTOR, produtoDto.getId().toString()));
							ApresentacaoProdutoDTO apresentacaoDto = this.obterApresentacoesPorProduto(produtoDto);
							
							PrecoDTO precoDto = this.buscarPreco(apresentacaoDto.getProduto());
							
							apresentacaoDto.getProduto().setIdCrypt(Encrypt.encrypt(EncryptConstantes.KEY, EncryptConstantes.INIT_VECTOR, apresentacaoDto.getProduto().getId().toString()));
							
							apresentacaoDto.getProduto().setPreco(precoDto.getPreco());
							apresentacaoDto.getProduto().setPrecoString(new Precos().padraoAmericanoParaBrasileiro(precoDto.getPreco()));
							
							listaDeApresentacaoRetornoDto.add(apresentacaoDto);
						}
					}
					
					return listaDeApresentacaoRetornoDto;
					
				} else {
					return null;
				}
				
			}
		}
		return null;
	}
	
	@Transactional(readOnly = true)
	private List<ProdutoDTO> obterListaDeProdutosPorCompra(List<CompraDTO> listaDeComprasDto) {
		
		List<ProdutoDTO> listaDeProdutosRetornoDto = new ArrayList<ProdutoDTO>();
		for(CompraDTO compraDto : listaDeComprasDto) {
			Compra compra = dozerBeanMapper.map(compraDto, Compra.class);
			List<ProdutoCompra> listaDeProdutoCompra = produtoCompraDao.obterPorCompra(compra);
		
			for(ProdutoCompra produtoCompra : listaDeProdutoCompra) {
				ProdutoDTO produtoDto = dozerBeanMapper.map(produtoCompra.getProduto(), ProdutoDTO.class);
				listaDeProdutosRetornoDto.add(produtoDto);
			}
		}
		
		
		return listaDeProdutosRetornoDto;
	}

	private List<CompraDTO> obterComprasPorIdComprador(BigInteger idComprador) {
		
		List<Compra> listaDeCompras = compraDao.obterComprasPorIdComprador(idComprador, LIMITE_DE_COMPRAS);
		if(!listaDeCompras.isEmpty()) {
			return this.criarDtosCompra(listaDeCompras);
		}
		
		return null;
	}

	
	private List<CompraDTO> obterComprasPorIpComprador(String ipComprador) {
		List<Compra> listaDeCompras = compraDao.obterComprasPorIpComprador(ipComprador, LIMITE_DE_COMPRAS);
		if(!listaDeCompras.isEmpty()) {
			return this.criarDtosCompra(listaDeCompras);
		}
		
		return null;
	}
	
	
	@Transactional(readOnly = true)
	private List<CompraDTO> criarDtosCompra(List<Compra> listaDeCompras) {
		List<CompraDTO> retornoListaCompraDto = new ArrayList<CompraDTO>();
		for(Compra compra : listaDeCompras) {
			CompraDTO compraDto = dozerBeanMapper.map(compra, CompraDTO.class);
			retornoListaCompraDto.add(compraDto);
		}
		
		return retornoListaCompraDto;
	}
	
	
	private ApresentacaoProdutoDTO obterApresentacoesPorProduto(ProdutoDTO produtoDto) throws JSONException, ClientProtocolException, URISyntaxException, IOException, RegraDeNegocioException {
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		JSONObject produtoJson = new JSONObject(objectMapper.writeValueAsString(produtoDto));
		
		produtoJson.remove("descricao");
		
		String listaDeApresentacoesString = (String) HTTPRequestResponse.get(urlModuloProduto.getUrlModulo() + "apresentacao/obterlistaDeApresentacaoPorProduto", ("produtoDto=" + produtoJson), request.getRemoteAddr());
		
		List<ApresentacaoProdutoDTO> listaDeApresentacoesDto = objectMapper.readValue(listaDeApresentacoesString, new TypeReference<List<ApresentacaoProdutoDTO>>(){});
	
		for(ApresentacaoProdutoDTO apresentacaoDto : listaDeApresentacoesDto) {
			
			if(apresentacaoDto.getCapa()) {
				return apresentacaoDto;
			}
		}
		
		return listaDeApresentacoesDto.get(0);
	}
	
	
	
	private PrecoDTO buscarPreco(ProdutoDTO produtoDto) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("referer", request.getRemoteAddr());

		HttpEntity<?> httpEntity = new HttpEntity<Object>(headers);
		
		logger.info("preco/obterPrecoAtualPorIdProduto/\" + pesquisaDeProdutosDto.getId()");
		ResponseEntity<PrecoDTO> response = restTemplate.exchange(
				urlModuloProduto.getUrlModulo() + "preco/obterPrecoAtualPorIdProduto/" + produtoDto.getId(), HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<PrecoDTO>() {
				});
		
		return  response.getBody();
	
	}
	
	
//	@Autowired
//	private CompraVendaDao compraVendaDao;
//	
//	@Autowired
//	private CompraBoletoDao compraBoletoDao;
//	
//	@Autowired
//	private CompraCartaoDeCreditoDao compraCartaoDeCreditoDao;
//	
//	@Autowired
//	private HistoricoDatasDeCompraDao historicoDatasDeCompraDao;
//	
//	@Autowired
//	private PorcentagemMercadoDeMusicaDao porcentagemMercadoDeMusicaDao;
//	
//	@Autowired
//	private TransferenciaDeValoresService transferenciaDeValoresService;
//	
//	@Autowired
//	private Mapper dozerBeanMapper;
//	
//	private final static Logger logger = Logger.getLogger(CompraVendaServiceImpl.class);
//	
//	@Transactional(readOnly = true)
//	public List<InfosGeraisProdutoUsuarioDTO> produtosVendidosPorUsuarioLimite(UsuarioDTO usuarioDto, Integer limite, OrdemDaListaEnum ordemDaLista) {
//		
//		logger.info("produtosVendidosPorUsuarioLimite(UsuarioDTO usuarioDto, Integer limite, OrdemDaListaEnum ordemDaLista)");
//		
//		
//		List<InfosGeraisProdutoUsuarioDTO> listaDeVendasDto = new ArrayList<InfosGeraisProdutoUsuarioDTO>();
//		
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		List<InfosGeraisProdutoUsuario> listaDeProdutosVendidosRetorno = compraVendaDao.produtosVendidosPorUsuarioLimite(usuario, limite, ordemDaLista);
//		
//		for(InfosGeraisProdutoUsuario produtoVendidos : listaDeProdutosVendidosRetorno) {
//			
//			InfosGeraisProdutoUsuarioDTO produtoVendidosDto = dozerBeanMapper.map(produtoVendidos, InfosGeraisProdutoUsuarioDTO.class);			
//			produtoVendidosDto.setPrecoString( new Precos().padraoAmericanoParaBrasileiro(produtoVendidosDto.getPreco()));
//			
//			listaDeVendasDto.add(produtoVendidosDto);
//		}
//		
//		return listaDeVendasDto;
//	}
//	
//	
//	@Transactional(readOnly = true)
//	public List<InfosGeraisProdutoUsuarioDTO> produtosCompradosPorUsuarioLimite(UsuarioDTO usuarioDto, Integer limite, OrdemDaListaEnum ordemDaLista) {
//		
//		logger.info("produtosCompradosPorUsuarioLimite(UsuarioDTO usuarioDto, Integer limite, OrdemDaListaEnum ordemDaLista)");
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		
//		List<Compra> listaDeComprasRetorno = compraVendaDao.produtosCompradosPorUsuarioLimite(usuario, limite, ordemDaLista);
//		
//		ArrayList<InfosGeraisProdutoUsuarioDTO> produtosCompradosDto = new ArrayList<InfosGeraisProdutoUsuarioDTO>();
//		
//		for(Compra compraRetorno : listaDeComprasRetorno) {
//			for(InfosGeraisProdutoUsuario produtosCompradosCompras : compraRetorno.getProdutosComprados()) {	
//				InfosGeraisProdutoUsuarioDTO produtosCompradosComprasDto = dozerBeanMapper.map(produtosCompradosCompras, InfosGeraisProdutoUsuarioDTO.class);
//					
//				produtosCompradosComprasDto.setPrecoString(new Precos().padraoAmericanoParaBrasileiro(produtosCompradosComprasDto.getPreco()));
//				produtosCompradosDto.add(produtosCompradosComprasDto);
//			}
//		}
//		
//		if(produtosCompradosDto.size() < 10 && produtosCompradosDto.size() != 0){
//			
//			return produtosCompradosDto.subList(0, produtosCompradosDto.size());
//		
//		} else if (produtosCompradosDto.size() >= 10){
//		
//			return produtosCompradosDto.subList(0, 10);
//			
//		} else {
//			return null;
//		}
//	}
//	
//	@Override
//	@Transactional(readOnly = true)
//	public CompraDTO obterCompraCompleta(CompraDTO compraDto) throws CompraVendaException {		
//		
//		logger.info("obterCompraCompleta(CompraDTO compraDto)");
//		
//		Compra compra = dozerBeanMapper.map(compraDto, Compra.class);
//		
//		if(compra.getId() == null) {
//			throw new CompraVendaException("Não existe compra cadastrada. Favor entrar em contato conosco");
//		}
//		
//		compra = compraVendaDao.selectOne(compra.getId());
//		compraDto = dozerBeanMapper.map(compra, CompraDTO.class);
//		
//		List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosCompradosNovosDto = new ArrayList<InfosGeraisProdutoUsuarioDTO>();
//		
//		if(compraDto.getProdutosComprados() != null && compraDto.getProdutosComprados().size() > 0 ){
//			for(InfosGeraisProdutoUsuarioDTO produtoCompradoDto : compraDto.getProdutosComprados()) {
//				if(!listaDeProdutosCompradosNovosDto.contains(produtoCompradoDto)) {
//					produtoCompradoDto.setPrecoString(new Precos().padraoAmericanoParaBrasileiro(produtoCompradoDto.getPreco()));
//					
//					listaDeProdutosCompradosNovosDto.add(produtoCompradoDto);
//				}
//			}
//		}
//	
//		
//		compraDto.setProdutosComprados(listaDeProdutosCompradosNovosDto);		
//		return compraDto;
//	}
//
//
//	@Override
//	@Transactional(propagation=Propagation.REQUIRED)
//	public void inserirProdutosNaCompraFeitaRealizada(CompraDTO compraDto) {
//		
//		logger.info("inserirProdutosNaCompraFeitaRealizada(CompraDTO compraDto)");
//		
//		ArrayList<InfosGeraisProdutoUsuario> listaDeProdutosCompradosEntities = new ArrayList<InfosGeraisProdutoUsuario>();
//		
//		Compra compraEntity = dozerBeanMapper.map(compraDto, Compra.class);
//		
//		for(InfosGeraisProdutoUsuarioDTO produtoComprado : compraDto.getProdutosComprados()) {
//			
//			InfosGeraisProdutoUsuario produtoCompradoEntity = dozerBeanMapper.map(produtoComprado, InfosGeraisProdutoUsuario.class);			
//			produtoCompradoEntity.setCompra(compraEntity);
//			
//			listaDeProdutosCompradosEntities.add(produtoCompradoEntity);
//		}
//		
//		
//		compraEntity.setProdutosComprados(listaDeProdutosCompradosEntities);
//		
//		HistoricoDatasDeCompra historicoDeCompra = new HistoricoDatasDeCompra();
//		historicoDeCompra.setCompra(compraEntity);
//		historicoDeCompra.setDataAtualizada(Boolean.TRUE);
//		historicoDeCompra.setDataDaCompra(new GregorianCalendar());
//		
//		Boolean insercaoDaDataNova = Boolean.FALSE;
//		if(compraEntity.getDatasDaCompra() == null || compraEntity.getDatasDaCompra().isEmpty()) {
//			
//			ArrayList<HistoricoDatasDeCompra> listaDeHistoricosDeCompra = new ArrayList<HistoricoDatasDeCompra>();
//			
//			listaDeHistoricosDeCompra.add(historicoDeCompra);
//			compraEntity.setDatasDaCompra(listaDeHistoricosDeCompra);
//		
//		} else {
//			Calendar dataDeHoje = Calendar.getInstance();
//						
//			for(HistoricoDatasDeCompra dataDaTransacao : compraEntity.getDatasDaCompra()) {
//				if(Datas.diasCorridosEntreUmaDataEOutra(dataDaTransacao.getDataDaCompra(), dataDeHoje) > 0 && dataDaTransacao.getDataAtualizada().equals(Boolean.TRUE)) {
//					
//					dataDaTransacao.setDataAtualizada(Boolean.FALSE);
//					insercaoDaDataNova = Boolean.TRUE;
//				}
//			}
//			
//			if(insercaoDaDataNova) {
//				compraEntity.getDatasDaCompra().add(historicoDeCompra);
//			}
//		}
//		
//		compraVendaDao.update(compraEntity);
//	}
//
//
//	@Override
//	public void verificacaoDeUsuarios(CompraDTO compraDto, UsuarioDTO usuarioDto) throws UsuarioException {
//		
//		logger.info("verificacaoDeUsuarios(CompraDTO compraDto, UsuarioDTO usuarioDto)");
//		
//		if(!compraDto.getComprador().getId().equals(usuarioDto.getId())) {
//			throw new UsuarioException("Usuários não coincidem");
//		}
//	}
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public CompraDTO obterCompraNaoFinalizadaDoDia(UsuarioDTO usuarioDto) throws UsuarioException {
//		
//		logger.info("obterCompraNaoFinalizadaDoDia(UsuarioDTO usuarioDto)");
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		
//		compraVendaDao.clear();
//		List<Compra> listaDeCompras = compraVendaDao.obterCompraNaoFinalizadaPorUsuarioLimit(usuario, BigInteger.valueOf(50), OrdemDaListaEnum.DESC);
//		
//		CompraDTO compraDto = null;
//		for(Compra compra : listaDeCompras) {
//			
//			for(HistoricoDatasDeCompra dataDeCompra : compra.getDatasDaCompra()) {
//				
//				Calendar dataDeHoje = Calendar.getInstance();
//				if(Datas.diasCorridosEntreUmaDataEOutra(dataDeCompra.getDataDaCompra(), dataDeHoje) == 0 && dataDeCompra.getDataAtualizada().equals(Boolean.TRUE) && compra.getFinalizado().equals(Boolean.FALSE)) {
//					
//					compraDto = dozerBeanMapper.map(compra, CompraDTO.class);
//					return compraDto;
//				}
//				
//			}
//			
//		}
//		return compraDto;
//	}
//	
//	
//	@Override
//	@Transactional(readOnly = true)
//	public CompraDTO obterCompraFinalizadaDoDia(UsuarioDTO usuarioDto) throws UsuarioException {
//		
//		logger.info("obterCompraFinalizadaDoDia(UsuarioDTO usuarioDto)");
//		
//		List<Compra> listaPrimeiraIteracao = new ArrayList<Compra>();
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		
//		List<Compra> listaDeCompras = compraVendaDao.obterListaDeCompraFinalizadaPorComprador(usuario);
//		
//		for(Compra compraPrimeiraIteracao : listaDeCompras) {
//			for(HistoricoDatasDeCompra dataDeCompra : compraPrimeiraIteracao.getDatasDaCompra()) {
//				
//				Calendar dataDeHoje = Calendar.getInstance();
//				if(Datas.diasCorridosEntreUmaDataEOutra(dataDeCompra.getDataDaCompra(), dataDeHoje) == 0 && compraPrimeiraIteracao.getFinalizado().equals(Boolean.TRUE) ) {
//					
//					listaPrimeiraIteracao.add(compraPrimeiraIteracao);
//				}
//				
//			}
//		}
//		
//		CompraDTO compraDto = null;
//		if(!listaPrimeiraIteracao.isEmpty() && listaPrimeiraIteracao.size() > 1) {
//			for (int i = 0; i < listaPrimeiraIteracao.size() - 1; i++) {
//				for(HistoricoDatasDeCompra dataDeCompraPrimeiraCompra : listaPrimeiraIteracao.get(i).getDatasDaCompra()) {
//					for(HistoricoDatasDeCompra dataDeCompraSegundaCompra : listaPrimeiraIteracao.get(i + 1).getDatasDaCompra()) {
//						if(dataDeCompraPrimeiraCompra.getDataAtualizada() && dataDeCompraSegundaCompra.getDataAtualizada()) {
//							int resutadoComparacao = dataDeCompraPrimeiraCompra.getDataDaCompra().compareTo(dataDeCompraSegundaCompra.getDataDaCompra());
//						
//							if(resutadoComparacao == 1) {
//								compraDto = dozerBeanMapper.map(listaPrimeiraIteracao.get(i), CompraDTO.class);
//							} else if(resutadoComparacao == -1) {
//								compraDto = dozerBeanMapper.map(listaPrimeiraIteracao.get(i + 1), CompraDTO.class);
//							}
//						}
//					}
//				}
//			}
//		} else if(!listaPrimeiraIteracao.isEmpty() && listaPrimeiraIteracao.size() == 1){
//			compraDto = dozerBeanMapper.map(listaPrimeiraIteracao.get(0), CompraDTO.class);
//		}
//		
//	
////		comparacao de datas aqui
//		
//		return compraDto;
//	}
//
//
//	@Override
//	public String obterValorTotal(CompraDTO compraDto) {
//		
//		logger.info("obterValorTotal(CompraDTO compraDto)");
//		
//		BigDecimal valorTotalDecimal = BigDecimal.ZERO;
//		for(InfosGeraisProdutoUsuarioDTO produtoComprado : compraDto.getProdutosComprados()) {
//			
//			valorTotalDecimal = valorTotalDecimal.add(produtoComprado.getPreco());
//			
////			if(produtoComprado.getTaxaGatewayDePagamentoBoleto() != null) {
////				valorTotalDecimal = valorTotalDecimal.add(produtoComprado.getTaxaGatewayDePagamentoBoleto());
////			}
//			
//			if(produtoComprado.getCalculoFrete() != null && produtoComprado.getCalculoFrete().size() != 0) {
//				
//				
//				
//				
//				List<ValorFreteCorreioDTO> arrayTirarObjetosDuplicados = new ArrayList<ValorFreteCorreioDTO>();
//				/*
//				 * atributos vieram duplacados do database
//				 */
//				
//				for(ValorFreteCorreioDTO calculoFrete : produtoComprado.getCalculoFrete()) {
//					if(calculoFrete.getAtivoParaCompra().equals(Boolean.TRUE && !arrayTirarObjetosDuplicados.contains(calculoFrete))) { // codigo do PAC
//						valorTotalDecimal = valorTotalDecimal.add(calculoFrete.getValor());
//						arrayTirarObjetosDuplicados.add(calculoFrete);
//					}
//				}
//			}
//			
//		}
//		return new Precos().padraoAmericanoParaBrasileiro(valorTotalDecimal);
//	}
//	
//	@Override
//	public String obterValorTotalSemFrete(CompraDTO compraDto) {
//		
//		logger.info("obterValorTotalSemFrete(CompraDTO compraDto");
//		
//		BigDecimal valorTotalDecimal = BigDecimal.ZERO;
//		for(InfosGeraisProdutoUsuarioDTO produtoComprado : compraDto.getProdutosComprados()) {
//			
//			valorTotalDecimal = valorTotalDecimal.add(produtoComprado.getPreco());
//			
////			if(produtoComprado.getTaxaGatewayDePagamentoBoleto() != null) {
////				valorTotalDecimal = valorTotalDecimal.add(produtoComprado.getTaxaGatewayDePagamentoBoleto());
////			}
//		}
//		return new Precos().padraoAmericanoParaBrasileiro(valorTotalDecimal);
//	}
//
//
//	@Override
//	public void verificarCarrinhoVazio(CompraDTO compraDoDia) throws CarrinhoDeCompraException {
//		
//		logger.info("verificarCarrinhoVazio(CompraDTO compraDoDia)");
//		
//		if(compraDoDia == null) {
//			throw new CarrinhoDeCompraException("Não existe nenhum produto para o carrinho de compra.");
//		}
//	}
//
//
//	@Override
//	@Transactional(propagation=Propagation.REQUIRED)
//	public CompraDTO removerCompraSemProduto(CompraDTO compraDto) {
//		
//		logger.info("removerCompraSemProduto(CompraDTO compraDto)");
//		
//		Compra compra = dozerBeanMapper.map(compraDto, Compra.class);
//		
//		if(compra.getProdutosComprados().isEmpty()) {
//			
//			for(HistoricoDatasDeCompra dataDeCompra : compra.getDatasDaCompra()) {
//				historicoDatasDeCompraDao.delete(historicoDatasDeCompraDao.contains(dataDeCompra) ? dataDeCompra : historicoDatasDeCompraDao.update(dataDeCompra));
//			}
//			
//			CompraBoleto compraBoleto = compraBoletoDao.obterCompraBoletoPorCompra(compra);
//			
//			if(compraBoleto != null) {
//				compraBoletoDao.delete(compraBoletoDao.contains(compraBoleto) ?  compraBoleto :compraBoletoDao.update(compraBoleto));
//			}
//			
//			compraVendaDao.delete(compraVendaDao.contains(compra) ? compra : compraVendaDao.update(compra));
//			return null;
//		} else {
//			return dozerBeanMapper.map(compra, CompraDTO.class);
//		}
//	}
//
//
//	@Override
//	@Transactional(propagation=Propagation.REQUIRED)
//	public void updateNaPoliticaDeCompra(CompraDTO compraDto, List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosComAvisoDeEntrega) {
//		
//		logger.info("updateNaPoliticaDeCompra(CompraDTO compraDto, List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosComAvisoDeEntrega)");
//		
//		Compra compra = dozerBeanMapper.map(compraDto, Compra.class);
//		Compra compraDatabase = compraVendaDao.selectOne(compra.getId());
//		Boolean update = false;
//		Boolean insercaoNovo = true;
//		
//		List<RiscosDaTransacao> listaDeRiscosDaTransacao = new ArrayList<RiscosDaTransacao>();
//		for(InfosGeraisProdutoUsuarioDTO produtoComAvisoDeEntrega : listaDeProdutosComAvisoDeEntrega) {
//			for(RiscosDaTransacao riscoDaTransacaoDatabase : compraDatabase.getRiscosDaTransacao()) {
//				
//				Calendar dataDeHoje = Calendar.getInstance();
//				if(riscoDaTransacaoDatabase.getInfosGeraisProdutoUsuario().getId().equals(produtoComAvisoDeEntrega.getId()) && 
//						(dataDeHoje.get(Calendar.YEAR) == riscoDaTransacaoDatabase.getDataDoAceite().get(Calendar.YEAR) && 
//						 dataDeHoje.get(Calendar.MONTH) == riscoDaTransacaoDatabase.getDataDoAceite().get(Calendar.MONTH) && 
//						 dataDeHoje.get(Calendar.DAY_OF_MONTH) == riscoDaTransacaoDatabase.getDataDoAceite().get(Calendar.DAY_OF_MONTH)) ) {
//					
//					insercaoNovo = false;
//				}
//			}
//		
//			if(insercaoNovo && compraDatabase.getRiscosDaTransacao() != null) {
//				listaDeRiscosDaTransacao = compraDatabase.getRiscosDaTransacao();
//			} 
//			
//			if(insercaoNovo) {
//				RiscosDaTransacao riscoDaTransacao = new RiscosDaTransacao();
//				
//				riscoDaTransacao.setInfosGeraisProdutoUsuario(dozerBeanMapper.map(produtoComAvisoDeEntrega, InfosGeraisProdutoUsuario.class));
//				riscoDaTransacao.setCompra(compraDatabase);
//				riscoDaTransacao.setAceite(Boolean.TRUE);
//				riscoDaTransacao.setDataDoAceite(Calendar.getInstance());
//				riscoDaTransacao.setCompra(compraDatabase);
//				
//				listaDeRiscosDaTransacao.add(riscoDaTransacao);
//			}
//			
//		}
//		
//		compraDatabase.setRiscosDaTransacao(listaDeRiscosDaTransacao);
//		
//		if(update) {
//			compraVendaDao.update(compra);
//		}
//	}
//
//
//	@Override
//	public List<String> obterAnosDeVencimentoDoCartaoDeCredito() {
//		
//		logger.info("obterAnosDeVencimentoDoCartaoDeCredito()");
//		
//		Calendar dataDeHoje = Calendar.getInstance();
//		
//		ArrayList<String> listaDeAnos = new ArrayList<String>();
//		listaDeAnos.add(String.valueOf(dataDeHoje.get(Calendar.YEAR)));
//		
//		for(int i = 0; i < 10; i++) {
//			dataDeHoje.set(Calendar.YEAR, dataDeHoje.get(Calendar.YEAR) + 1);
//			listaDeAnos.add(String.valueOf(dataDeHoje.get(Calendar.YEAR)));
//		}
//		
//		return listaDeAnos;
//	}
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<CompraDTO> obterListaDeComprasFinalizadasPorUsuarioComprador(UsuarioDTO usuarioDto) {
//		
//		logger.info("obterListaDeComprasFinalizadasPorUsuarioComprador(UsuarioDTO usuarioDto)");
//		
//		Usuario usuarioEntity = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		
//		List<Compra> listaDeComprasEntities = compraVendaDao.obterListaDeCompraFinalizadaPorComprador(usuarioEntity);
//		
//		ArrayList<CompraDTO> listaDeComprasRetornoDto = new ArrayList<CompraDTO>();
//		for(Compra compraEntity : listaDeComprasEntities) {
//			
//			CompraBoletoDTO compraBoletoDto = null;
//			if(compraEntity.getTipoPagamento().equals(TipoPagamentoEnum.BoletoBancario)) {
//				compraBoletoDto = dozerBeanMapper.map(compraBoletoDao.obterCompraBoletoPorCompra(compraEntity), CompraBoletoDTO.class);
//			}
//			
//			
//			CompraDTO compraDto = dozerBeanMapper.map(compraEntity, CompraDTO.class);
//			
//			if(compraDto.getTipoPagamento().equals(TipoPagamentoEnum.BoletoBancario)) {
//				compraDto.setCompraBoleto(compraBoletoDto);
//			}
//			
//			
//			for(InfosGeraisProdutoUsuarioDTO produtoComprado : compraDto.getProdutosComprados()) {
//			
//				ArrayList<TelefoneDTO> listaTelefones = new ArrayList<TelefoneDTO>(produtoComprado.getUsuario().getTelefones());
//				produtoComprado.getUsuario().setTelefonesList(listaTelefones);
//			}
//			
//			listaDeComprasRetornoDto.add(compraDto);
//		}
//		
//		return listaDeComprasRetornoDto;
//	}
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<CompraDTO> verificarProdutosComComprasFinalizadas(List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosInativosDto) {
//		
//		logger.info("verificarProdutosComComprasFinalizadas(List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosInativosDto)");
//		
//		List<CompraDTO> listaDeCompras = new ArrayList<CompraDTO>();
//		for( InfosGeraisProdutoUsuarioDTO infoGeralProdutoUsuarioInativoDto : listaDeProdutosInativosDto ){
//			InfosGeraisProdutoUsuario infoGeralProdutoUsuarioInativo = dozerBeanMapper.map(infoGeralProdutoUsuarioInativoDto, InfosGeraisProdutoUsuario.class);
//			
//			if(infoGeralProdutoUsuarioInativo.getCompra() != null) {
//				Compra compraBancoDeDados = compraVendaDao.selectOne(infoGeralProdutoUsuarioInativo.getCompra().getId());
//				if(compraBancoDeDados.getStatusTransacao() != null && 
//						compraBancoDeDados.getStatusTransacao().equals(StatusTransacaoEnum.APROVADO) && compraBancoDeDados.getFinalizado() ) {
//					CompraDTO compraBancoDeDadosDto = dozerBeanMapper.map(compraBancoDeDados, CompraDTO.class);
//					
//					/*
//					 * verificacao nas lista de retorno de elemento iguais 
//					 */
//					Boolean insereCompra = true;
//					if(listaDeCompras.isEmpty()) {
//						/*
//						 * metodo feito para acabar com produtos iguais por causa do hibernate fetch eager
//						 */
//						compraBancoDeDadosDto = this.ajustesDeApresentacaoDaTela(compraBancoDeDadosDto);
//						
//						listaDeCompras.add(compraBancoDeDadosDto);
//					} else {
//						for(CompraDTO compraIncluida : listaDeCompras) {
//							if(compraIncluida.getId().equals(compraBancoDeDadosDto.getId())) {
//								insereCompra = false;
//							}
//						}
//						if(insereCompra) {
//							/*
//							 * metodo feito para acabar com produtos iguais por causa do hibernate fetch eager
//							 */
//							compraBancoDeDadosDto = this.ajustesDeApresentacaoDaTela(compraBancoDeDadosDto);
//							listaDeCompras.add(compraBancoDeDadosDto);
//						}
//					}
//					
//				}
//			}
//		}
//		
//		return listaDeCompras;
//	}
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<CompraDTO> obterListaDeComprasNaoFinalizadas(UsuarioDTO usuarioDto) {
//		
//		logger.info("obterListaDeComprasNaoFinalizadas(UsuarioDTO usuarioDto)");
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		
//		List<Compra> listaDeCompras = compraVendaDao.obterCompraNaoFinalizadaPorUsuarioLimit(usuario, BigInteger.valueOf(50), OrdemDaListaEnum.DESC);
//		
//		ArrayList<CompraDTO> arrayCompraDto = new ArrayList<CompraDTO>();
//		for(Compra compra : listaDeCompras) {
//			
//			for(HistoricoDatasDeCompra dataDeCompra : compra.getDatasDaCompra()) {
//				if(Datas.diasCorridosEntreUmaDataEOutra(dataDeCompra.getDataDaCompra(), Calendar.getInstance()) > 0 && dataDeCompra.getDataAtualizada()) {
//					
//					CompraDTO compraDto = dozerBeanMapper.map(compra, CompraDTO.class);
//					arrayCompraDto.add(compraDto);
//				
//				}
//			}		
//		}
//		
//		return arrayCompraDto;
//	}
//	
//	private CompraDTO ajustesDeApresentacaoDaTela(CompraDTO compraBancoDeDadosDto) {
//		
//		logger.info("ajustesDeApresentacaoDaTela(CompraDTO compraBancoDeDadosDto)");
//		
//		
//		HashSet<InfosGeraisProdutoUsuarioDTO> setProdutosComprados = new HashSet<InfosGeraisProdutoUsuarioDTO>(compraBancoDeDadosDto.getProdutosComprados());
//		ArrayList<InfosGeraisProdutoUsuarioDTO> produtosCompradosList = new ArrayList<InfosGeraisProdutoUsuarioDTO>(setProdutosComprados);		
//		compraBancoDeDadosDto.setProdutosComprados(produtosCompradosList);
//		
//		HashSet<HistoricoDatasDeCompraDTO> setDatasDeCompra = new HashSet<HistoricoDatasDeCompraDTO>(compraBancoDeDadosDto.getDatasDaCompra());
//		compraBancoDeDadosDto.setDatasDaCompra(new ArrayList<HistoricoDatasDeCompraDTO>(setDatasDeCompra));
//		
//		
//		for(InfosGeraisProdutoUsuarioDTO produtoComprado : compraBancoDeDadosDto.getProdutosComprados()) {
//			produtoComprado.getUsuario().setTelefonesList( new ArrayList<TelefoneDTO>(produtoComprado.getUsuario().getTelefones()));
//		}
//		
//		return compraBancoDeDadosDto;
//	}
//
//
//	@Override
//	@Transactional(propagation=Propagation.REQUIRED)
//	public void updateCompraNaoRealizadaComCompraDoDia(CompraDTO compraDoDiaDto, CompraDTO compraParaFinalizarDto) {
//		
//		logger.info("updateCompraNaoRealizadaComCompraDoDia(CompraDTO compraDoDiaDto, CompraDTO compraParaFinalizarDto)");
//		
//		Compra compraParaFinalizar = dozerBeanMapper.map(compraParaFinalizarDto, Compra.class);
//		
//		compraParaFinalizar = compraVendaDao.selectOne(compraParaFinalizar.getId());
//		
//		if(compraDoDiaDto != null) {
//			Compra compraDoDia = dozerBeanMapper.map(compraDoDiaDto, Compra.class);
//			compraDoDia = compraVendaDao.selectOne(compraDoDia.getId());
//			
//			List<InfosGeraisProdutoUsuario> listaDeProdutosCompradosParaFinalizar = compraParaFinalizar.getProdutosComprados();
//			
//			for(InfosGeraisProdutoUsuario produto : listaDeProdutosCompradosParaFinalizar) {
//				produto.setCompra(compraDoDia);
//			}
//			
//			compraDoDia.getProdutosComprados().addAll(listaDeProdutosCompradosParaFinalizar);
//			compraParaFinalizar.setProdutosComprados(null);
//			
//			
//			HashSet<InfosGeraisProdutoUsuario> hashSetProdutosComprados = new HashSet<InfosGeraisProdutoUsuario>(compraDoDia.getProdutosComprados());
//			compraDoDia.setProdutosComprados(new ArrayList<InfosGeraisProdutoUsuario>(hashSetProdutosComprados));
//			
//			compraVendaDao.update(compraDoDia);
//			compraVendaDao.flush();
//			
//			compraVendaDao.delete(compraParaFinalizar);
//		} else {
//			for(HistoricoDatasDeCompra dataDeCompra : compraParaFinalizar.getDatasDaCompra()) {
//				dataDeCompra.setDataAtualizada(false);
//			}
//			
//			
//			/*
//			 * caso aqui a compra já tenha sido cancelada por algum motivo e o usuário torne a realizá-la, 
//			 * lancamos uma nova para que haja um histórico no sistema 
//			 */
//			
//			if(compraParaFinalizar.getStatusTransacao() != null && compraParaFinalizar.getStatusTransacao().equals(StatusTransacaoEnum.CANCELADO)) {
//				
//			
//				
//				/*
//				 * recompra
//				 */
//				
//				Compra compraNovaStatusCancelado = (Compra) SerializationUtils.clone(compraParaFinalizar);
//				compraNovaStatusCancelado.setId(null);
//				compraNovaStatusCancelado.setAceitePoliticaDeCompra(null);
//				compraNovaStatusCancelado.setCriacao(Calendar.getInstance());
//				compraNovaStatusCancelado.setFinalizado(Boolean.FALSE);
//				compraNovaStatusCancelado.setIdPedido(null);
//				compraNovaStatusCancelado.setParcelas(null);
//				compraNovaStatusCancelado.setStatusPagamento(null);
//				compraNovaStatusCancelado.setStatusTransacao(null);
//				compraNovaStatusCancelado.setTipoPagamento(null);
//				compraNovaStatusCancelado.setTotalPago(null);
//				compraNovaStatusCancelado.setDatasDaCompra(null);
//				
//				
//				compraNovaStatusCancelado = this.colocarHistoricoUpdateCompra(compraNovaStatusCancelado);
//				
//				this.finalizacaoUpdateCompra(compraNovaStatusCancelado);
//				this.finalizacaoUpdateCompra(compraParaFinalizar);
//			
//			} else {
//				
//				compraParaFinalizar = this.colocarHistoricoUpdateCompra(compraParaFinalizar);
//				this.finalizacaoUpdateCompra(compraParaFinalizar);
//			}
//		}	
//	}
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<CompraDTO> obterCompraEmAnalise() {
//		
//		logger.info("obterCompraEmAnalise()");
//		
//		List<Compra> listaDeCompras = compraVendaDao.obterCompraEmAnalise();
//		
//		List<CompraDTO> listaDeCompraDto = new ArrayList<CompraDTO>();
//		for(Compra compra : listaDeCompras) {
//			CompraDTO compraDto = dozerBeanMapper.map(compra, CompraDTO.class);
//			
//			listaDeCompraDto.add(compraDto);
//		}
//		
//		return listaDeCompraDto;
//	}
//
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void updateNaCompra(CompraDTO compraDto) {
//		
//		logger.info("updateNaCompra(CompraDTO compraDto)");
//		
//		Compra compra = dozerBeanMapper.map(compraDto, Compra.class);
//		
//		compraVendaDao.update(compra);
//    	compraVendaDao.flush();
//	}
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<InfosGeraisProdutoUsuarioDTO> obterComprasLiberadasParaPagamento(List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosFinalizadosDto) throws RegraDeNegocioException {
//		
//		logger.info("obterComprasLiberadasParaPagamento(List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosFinalizadosDto)");
//		
//		PorcentagemMercadoDeMusica porcentagemAtiva = porcentagemMercadoDeMusicaDao.obterPorcentagemAtiva();
//		
//		if(porcentagemAtiva == null) {
//			throw new RegraDeNegocioException("Não foi possível encontrar a porcentagem cadastrada");
//		}
//		
//		List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosRetornoDto = new ArrayList<InfosGeraisProdutoUsuarioDTO>();
//		
//		Calendar dataDeHoje = Calendar.getInstance();
//		
//		for(InfosGeraisProdutoUsuarioDTO produtoComprado : listaDeProdutosFinalizadosDto) {
//			TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto = transferenciaDeValoresService.obterTransferenciaPorProduto(produtoComprado);
//			
//			StatusTransferenciaDeValorDTO statusTransferenciaAtiva = new StatusTransferenciaDeValorDTO();
//			if(transferenciaValoresUsuarioDto != null) {
//				for(StatusTransferenciaDeValorDTO statusTransferencia : transferenciaValoresUsuarioDto.getListaStatusTransferenciaDeValor()){
//					if(statusTransferencia.getAtivo()) {
//						statusTransferenciaAtiva = statusTransferencia;
//					}
//				}
//			}
//			
//			
//			
//			
//			if(produtoComprado.getTipoEnvio().equals(TipoEnvioEnum.CORREIOS) && produtoComprado.getRastreamento() != null &&  produtoComprado.getRastreamento().getDesistencia().equals(Boolean.FALSE) && 
//					(transferenciaValoresUsuarioDto == null || transferenciaValoresUsuarioDto != null && statusTransferenciaAtiva.getStatusTransferenciaDeValoresGatewayDePagamento() != null && 
//							statusTransferenciaAtiva.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED))) {
//				RastreamentoProdutoCorreiosDTO rastreamentoProdutoCorreios = (RastreamentoProdutoCorreiosDTO) produtoComprado.getRastreamento();
//				
//				for(StatusPostagemProdutoCorreioDTO statusRastreamentoCorreios : rastreamentoProdutoCorreios.getStatusPostagemProdutoCorreio()) {
//					if(statusRastreamentoCorreios.getAtivo() && statusRastreamentoCorreios.getStatus().equals(StatusPostagemCorreiosEnum.Entregue) && 
//							!produtoComprado.getDesistenciaDeCompra() && Datas.diasUteisEntreUmaDataEOutra(statusRastreamentoCorreios.getDataDoProcessamento(), dataDeHoje) > DiasDeEsperaParaTransferenciaDeValoresOuDesistencia.DIAS) {
//						
//						produtoComprado.setPorcentagem(porcentagemAtiva.getPorcentagem());
//						produtoComprado.setPrecoString(new Precos().padraoAmericanoParaBrasileiro(produtoComprado.getPreco()));
//						
////						TODO: verificar esse metodo para futuras promocoes
//						
//						BigDecimal precoLiquido = produtoComprado.getPreco().multiply(porcentagemAtiva.getPorcentagem());
//						precoLiquido = precoLiquido.divide(BigDecimal.valueOf(100));
//						
//						produtoComprado.setPrecoLiquidoString(new Precos().padraoAmericanoParaBrasileiro(produtoComprado.getPreco().subtract(precoLiquido)));
//						
//						listaDeProdutosRetornoDto.add(produtoComprado);
//					} 
//				}
//				
//			} else if(produtoComprado.getTipoEnvio().equals(TipoEnvioEnum.EM_MAOS) && produtoComprado.getRastreamento() != null && !produtoComprado.getDesistenciaDeCompra() &&  produtoComprado.getRastreamento().getDesistencia().equals(Boolean.FALSE) && 
//					(transferenciaValoresUsuarioDto == null || transferenciaValoresUsuarioDto != null && statusTransferenciaAtiva.getStatusTransferenciaDeValoresGatewayDePagamento() != null && 
//					statusTransferenciaAtiva.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED))) {
////				if feito por causa do cast de rastreamento
//				
//				RastreamentoProdutoDiretoDTO rastreamentoProdutoDiretoDto = (RastreamentoProdutoDiretoDTO) produtoComprado.getRastreamento();
//				
//				if(rastreamentoProdutoDiretoDto.getCodigoInseridoComprador() != null && !rastreamentoProdutoDiretoDto.getCodigoInseridoComprador().equals("") && 
//						Datas.diasUteisEntreUmaDataEOutra(rastreamentoProdutoDiretoDto.getDataInsercaoCodigoComprador(), dataDeHoje) > DiasDeEsperaParaTransferenciaDeValoresOuDesistencia.DIAS) {
//					
//					produtoComprado.setPorcentagem(porcentagemAtiva.getPorcentagem());
//					produtoComprado.setPrecoString(new Precos().padraoAmericanoParaBrasileiro(produtoComprado.getPreco()));
//					
//					BigDecimal precoLiquido = produtoComprado.getPreco().multiply(porcentagemAtiva.getPorcentagem());
//					precoLiquido = precoLiquido.divide(BigDecimal.valueOf(100));
//					
//					produtoComprado.setPrecoLiquidoString(new Precos().padraoAmericanoParaBrasileiro(produtoComprado.getPreco().subtract(precoLiquido)));
//					
//					listaDeProdutosRetornoDto.add(produtoComprado);
//				}
//			}
//		}
//
//		return listaDeProdutosRetornoDto;
//	}
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<InfosGeraisProdutoUsuarioDTO> obterComprasIndisponiveisParaPagamento(List<CompraDTO> listaDeComprasFinalizadasDto) {
//		
//		logger.info("obterComprasIndisponiveisParaPagamento(List<CompraDTO> listaDeComprasFinalizadasDto)");
//		
//		PorcentagemMercadoDeMusica porcentagemAtiva = porcentagemMercadoDeMusicaDao.obterPorcentagemAtiva();
//		PorcentagemMercadoDeMusicaDTO porcentagemAtivaDto = dozerBeanMapper.map(porcentagemAtiva, PorcentagemMercadoDeMusicaDTO.class);
//		
//		List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosRetornoDto = new ArrayList<InfosGeraisProdutoUsuarioDTO>();
//		for(CompraDTO compraFinalizadaDto : listaDeComprasFinalizadasDto) {
//			for(InfosGeraisProdutoUsuarioDTO produtoComprado : compraFinalizadaDto.getProdutosComprados()) {
//				Precos precosUtil = new Precos();
//				
//				produtoComprado.setPorcentagem(porcentagemAtivaDto.getPorcentagem());
//				produtoComprado.setPrecoString(precosUtil.padraoAmericanoParaBrasileiro(produtoComprado.getPreco()));
//				
//				BigDecimal porcentagemValor = produtoComprado.getPreco().multiply(porcentagemAtivaDto.getPorcentagem()).divide(BigDecimal.valueOf(100));
//				BigDecimal precoComPorcentagem = produtoComprado.getPreco().subtract(porcentagemValor);
//				
//				produtoComprado.setPrecoLiquidoString(precosUtil.padraoAmericanoParaBrasileiro(precoComPorcentagem));
//				
//				Calendar dataDeHoje = Calendar.getInstance();
//				
//				Integer diasRestantesParaLiberacao = null;
//				if(produtoComprado.getTipoEnvio().equals(TipoEnvioEnum.EM_MAOS) && produtoComprado.getRastreamento() != null && produtoComprado.getRastreamento().getDesistencia().equals(Boolean.FALSE)) {
//					RastreamentoProdutoDiretoDTO rastreamentoProdutoDiretoDto = (RastreamentoProdutoDiretoDTO) produtoComprado.getRastreamento();
//					
//					if(rastreamentoProdutoDiretoDto.getCodigoInseridoComprador() != null && !rastreamentoProdutoDiretoDto.getCodigoInseridoComprador().equals("")
//						&& rastreamentoProdutoDiretoDto.getDataInsercaoCodigoComprador() != null && 
//						Datas.diasUteisEntreUmaDataEOutra(rastreamentoProdutoDiretoDto.getDataInsercaoCodigoComprador(), dataDeHoje) <= DiasDeEsperaParaTransferenciaDeValoresOuDesistencia.DIAS) {
//						
//						diasRestantesParaLiberacao = DiasDeEsperaParaTransferenciaDeValoresOuDesistencia.DIAS - Datas.diasUteisEntreUmaDataEOutra(rastreamentoProdutoDiretoDto.getDataInsercaoCodigoComprador(), dataDeHoje) + 1;
//						produtoComprado.setDiasParaLiberacaoDePagamento(diasRestantesParaLiberacao);
//						listaDeProdutosRetornoDto.add(produtoComprado);
//					
////					caso nao exista nenhum codigo colocado ainda	
//					} else if(rastreamentoProdutoDiretoDto.getDataInsercaoCodigoComprador() == null) {
//						
//						produtoComprado.setDiasParaLiberacaoDePagamento(DiasDeEsperaParaTransferenciaDeValoresOuDesistencia.DIAS);
//						listaDeProdutosRetornoDto.add(produtoComprado);
//					
//					}
// 					
//				}
//
//				
//				if(produtoComprado.getTipoEnvio().equals(TipoEnvioEnum.CORREIOS) && produtoComprado.getRastreamento() != null && produtoComprado.getRastreamento().getDesistencia().equals(Boolean.FALSE)) {
//					RastreamentoProdutoCorreiosDTO rastreamentoProdutoCorreiosDto = (RastreamentoProdutoCorreiosDTO) produtoComprado.getRastreamento();
//					
//					for(StatusPostagemProdutoCorreioDTO statusRastreamentoCorreios : rastreamentoProdutoCorreiosDto.getStatusPostagemProdutoCorreio()) {
//						if(statusRastreamentoCorreios.getAtivo() && statusRastreamentoCorreios.getStatus().equals(StatusPostagemCorreiosEnum.Entregue) && Datas.diasUteisEntreUmaDataEOutra(statusRastreamentoCorreios.getDataDoProcessamento(), dataDeHoje) <= DiasDeEsperaParaTransferenciaDeValoresOuDesistencia.DIAS) {
//							diasRestantesParaLiberacao = DiasDeEsperaParaTransferenciaDeValoresOuDesistencia.DIAS - Datas.diasUteisEntreUmaDataEOutra(statusRastreamentoCorreios.getDataDoProcessamento(), dataDeHoje) + 1;
//							produtoComprado.setDiasParaLiberacaoDePagamento(diasRestantesParaLiberacao);
//							listaDeProdutosRetornoDto.add(produtoComprado);
//						
//						} else if (statusRastreamentoCorreios.getAtivo() && statusRastreamentoCorreios.getStatus().equals(StatusPostagemCorreiosEnum.EmBranco) || statusRastreamentoCorreios.getStatus().equals(StatusPostagemCorreiosEnum.Postado)) {
//							produtoComprado.setDiasParaLiberacaoDePagamento(DiasDeEsperaParaTransferenciaDeValoresOuDesistencia.DIAS);
//							listaDeProdutosRetornoDto.add(produtoComprado);
//						}
//					}	
//				}
//			}
//		}
//		
//		return listaDeProdutosRetornoDto;
//	}
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public CompraBoletoDTO obterCompraPorBoleto(CompraDTO compraDto) {
//		
//		logger.info("obterCompraPorBoleto(CompraDTO compraDto)");
//		
//		Compra compra = dozerBeanMapper.map(compraDto, Compra.class);
//		CompraBoleto compraBoleto = compraBoletoDao.obterCompraBoletoPorCompra(compra);
//		
//		CompraBoletoDTO compraBoletoDto = null;
//		if(compraBoleto != null) {
//			compraBoletoDto = dozerBeanMapper.map(compraBoleto, CompraBoletoDTO.class);
//		}
//		
//		return compraBoletoDto;
//	}
//
//	
//	@Override
//	@Transactional(readOnly = true)
//	public CompraDTO obterCompraPorProduto(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto) {
//		
//		logger.info("obterCompraPorProduto(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto)");
//		
//		InfosGeraisProdutoUsuario infosGeraisProdutoUsuario = dozerBeanMapper.map(infosGeraisProdutoUsuarioDto, InfosGeraisProdutoUsuario.class);
//		Compra compra = compraVendaDao.selectOne(infosGeraisProdutoUsuario.getCompra().getId());
//		
//		return dozerBeanMapper.map(compra, CompraDTO.class);
//	}
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public CompraCartaoDeCreditoDTO obterCompraPorCartaoDeCredito(CompraDTO compraDto) {
//		
//		logger.info("obterCompraPorCartaoDeCredito(CompraDTO compraDto)");
//		
//		
//		Compra compra = dozerBeanMapper.map(compraDto, Compra.class);
//		CompraCartaoDeCredito compraCartaoDeCredito = compraCartaoDeCreditoDao.obterCompraCartaoDeCreditoPorCompra(compra);
//		
//		CompraCartaoDeCreditoDTO compraCartaoDeCreditoDto = null;
//		if(compraCartaoDeCredito != null) {
//			compraCartaoDeCreditoDto = dozerBeanMapper.map(compraCartaoDeCredito, CompraCartaoDeCreditoDTO.class);
//		}
//		
//		return compraCartaoDeCreditoDto;
//	}	
//	
//	
//	@Transactional(propagation=Propagation.REQUIRED)
//	public String verificacaoGatewayDePagamentoMudancaStatus(CompraDTO compraDto) throws GatewayDePagamentoException, IOException {
//		
//		logger.info("verificacaoGatewayDePagamentoMudancaStatus(CompraDTO compraDto)");
//		
//	    HttpClient client = new HttpClient();
//	    PostMethod method = new PostMethod(TrayCheckoutAutenticacao.CONSULTA_TRANSACAO);
//	    
//	    method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
//	        new DefaultHttpMethodRetryHandler(3, false));
//	    byte[] responseBody = null;
//
//	    try {
//	        NameValuePair[] data = {
//	            /* Token da Conta do Lojista */
//	            new NameValuePair("token_account", TrayCheckoutAutenticacao.TOKEN),
//
//	            /* Token da Transação */
//	            new NameValuePair("token_transaction", compraDto.getTokenPedido())
//	        };
//
//	        method.setRequestBody(data);
//
//	        // Execute the method.
//	        int statusCode = client.executeMethod(method);
//
//	        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_CREATED) {
//	        	throw new GatewayDePagamentoException(method.getStatusText());
//	        }
//	        
//	        responseBody = method.getResponseBody();
//	        
//	    } catch (HttpException e) {
//	    	method.releaseConnection();
//	    	throw new GatewayDePagamentoException(e.getMessage());
//	    } catch (IOException e) {
//	    	method.releaseConnection();
//	    	throw new GatewayDePagamentoException(e.getMessage());
//	    }
//	    
//	    method.releaseConnection();
//	    String xmlResposta = new String(responseBody);
//	    
//	    JSONObject jsonRetorno = XML.toJSONObject(xmlResposta);
//	    JSONObject response = jsonRetorno.getJSONObject("response");
//	    
//	    if(response.has("error_response")) {
//	    	JSONObject errorResponse = response.getJSONObject("error_response");
//	    	JSONObject errors = errorResponse.getJSONObject("errors");
//	    	JSONObject error = errors.getJSONObject("error");
//	    	throw new GatewayDePagamentoException(error.getString("message"));
//	    }
//	    
////	    correto
////	    {"response":{"data_response":{"transaction":{"status_id":{"type":"integer","content":87},"status_name":"Em Monitoramento","order_number":63362,"payment":{"url_payment":"","payment_method_id":{"type":"integer","content":4},"split":{"type":"integer","content":11},"price_original":{"type":"decimal","content":11000},"payment_response":"Mensagem de venda fake","linha_digitavel":{"nil":true},"payment_method_name":"Mastercard","price_payment":{"type":"decimal","content":12356.52},"tid":1233},"free":{"nil":true},"token_transaction":"7965f1188f211cd6b1a4d48c66beec71","customer":{"addresses":{"address":{"number":385,"completion":"","city":"São Paulo","street":"Rua Arão Reis","neighborhood":"Jardim São Cristóvão","state":"SP","postal_code":"03930000"},"type":"array"},"company_name":"","name":"Erik Scaranello 2","cpf":38159408935,"cnpj":"","email":"erikscaranello@mercadodemusica.com.br","contacts":{"contact":{"type_contact":"M","value":11985785538},"type":"array"},"trade_name":""}}},"message_response":{"message":"success"}}}
//	    JSONObject dataResponse = response.getJSONObject("data_response");
//	    JSONObject transaction = dataResponse.getJSONObject("transaction");
//	    JSONObject statudId = transaction.getJSONObject("status_id");
//	    
//	    return String.valueOf(statudId.getInt("content"));
//	}
//	
//	
//	private Compra colocarHistoricoUpdateCompra(Compra compra) {
//		
//		logger.info("colocarHistoricoUpdateCompra(Compra compra)");
//		
//		HistoricoDatasDeCompra historicoDatasDeCompra = new HistoricoDatasDeCompra();
//		
//		historicoDatasDeCompra.setDataAtualizada(true);
//		historicoDatasDeCompra.setDataDaCompra(Calendar.getInstance());
//		historicoDatasDeCompra.setCompra(compra);
//		
//		if(compra.getDatasDaCompra() == null) {
//			compra.setDatasDaCompra(new ArrayList<HistoricoDatasDeCompra>());
//		}
//		
//		compra.getDatasDaCompra().add(historicoDatasDeCompra);
//		
//		return compra;
//	}
//	
//	
//	@Transactional(propagation=Propagation.NESTED)
//	private void finalizacaoUpdateCompra (Compra compra) {
//		
//		logger.info("finalizacaoUpdateCompra (Compra compra)");
//		
//		compraVendaDao.update(compra);
//		compraVendaDao.flush();
//	}
}
		