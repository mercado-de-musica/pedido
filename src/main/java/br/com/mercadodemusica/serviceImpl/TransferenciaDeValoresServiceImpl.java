package br.com.mercadodemusica.serviceImpl;

import br.com.mercadodemusica.service.TransferenciaDeValoresService;

//@Service
public class TransferenciaDeValoresServiceImpl implements TransferenciaDeValoresService {
	
//	@Autowired
//	private TransferenciaValoresUsuarioDao transferenciaValoresUsuarioDao;
//	
//	@Autowired
//	private StatusTransferenciaDeValorDao statusTransferenciaDeValorDao;
//	
//	@Autowired
//	private ContaBancariaDao contaBancariaDao;
//	
//	@Autowired
//	private PorcentagemMercadoDeMusicaDao porcentagemMercadoDeMusicaDao;
//	
//	@Autowired
//	private Mapper dozerBeanMapper;
//
//	private final static Logger logger = Logger.getLogger(TransferenciaDeValoresServiceImpl.class);
//	
//	@Override
//	@Transactional(readOnly = true)
//	public List<ContaBancariaDTO> verificarContaBancariaTransferenciaDeValores(UsuarioDTO usuarioDto) throws ContaBancariaGatewayDePagamentoException {
//		
//		logger.info("verificarContaBancariaTransferenciaDeValores(UsuarioDTO usuarioDto)");
//		
//		/*
//		 * TODO dormi duas horas apenas. Tenho que verificar esse método de novo
//		 */
//		
//	
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		TransferenciaValoresUsuario transferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAbertoPorUsuario(usuario, Boolean.FALSE);
//		
//		List<ContaBancariaDTO> arrayContaBancariaGatewayDePagamentoDto = new ArrayList<ContaBancariaDTO>();
//		if(transferenciaEmAberto != null && transferenciaEmAberto.getContaUsadaNaTransferencia() != null) {
//			
//			ContaBancariaDTO contaBancariaGatewayDePagamentoDto = dozerBeanMapper.map(transferenciaEmAberto.getContaUsadaNaTransferencia(), ContaBancariaDTO.class);
//			arrayContaBancariaGatewayDePagamentoDto.add(contaBancariaGatewayDePagamentoDto);
//			
//			return arrayContaBancariaGatewayDePagamentoDto;
//		
//		} else {
//			
//			List<ContaBancaria> listaDeContasBancarias = contaBancariaDao.obterContasDePagamentoPorUsuario(usuario);
//		
//			if(listaDeContasBancarias == null || listaDeContasBancarias.isEmpty()) {
//				
//				throw new ContaBancariaGatewayDePagamentoException("Não existe conta bancária cadastrada");
//				
//			} else {
//				for(ContaBancaria contaBancaria : listaDeContasBancarias) {
//					ContaBancariaDTO contaBancariaGatewayDePagamentoDto = dozerBeanMapper.map(contaBancaria, ContaBancariaDTO.class);
//					arrayContaBancariaGatewayDePagamentoDto.add(contaBancariaGatewayDePagamentoDto);
//				}
//				
//				return arrayContaBancariaGatewayDePagamentoDto;
//			}
//		} 
//	}
//	
//	
//	
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void inserirProdutoVendidoNaTransferenciaDeValores(RastreamentoDTO rastreamentoDto, ContaBancariaDTO contaBancariaGatewayDePagamentoDto, Boolean desistencia) throws TransferenciaValoresUsuarioException {
//		
//		logger.info("inserirProdutoVendidoNaTransferenciaDeValores(RastreamentoDTO rastreamentoDto, ContaBancariaDTO contaBancariaGatewayDePagamentoDto, Boolean desistencia)");
//		
//		
//		Rastreamento rastreamento = dozerBeanMapper.map(rastreamentoDto, Rastreamento.class);
//		
//		ContaBancaria contaBancariaGatewayDePagamento = dozerBeanMapper.map(contaBancariaGatewayDePagamentoDto, ContaBancaria.class);
//
//		TransferenciaValoresUsuario transferenciaEmAberto = null;
//		if(desistencia) {
//			
//			Usuario compradorDto = dozerBeanMapper.map(rastreamentoDto.getComprador(), Usuario.class);
//			transferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAbertoPorUsuario(compradorDto, desistencia);
//		} else {
//			Usuario vendedorDto = dozerBeanMapper.map(rastreamentoDto.getInfosGeraisProdutoUsuario().getUsuario(), Usuario.class);
//			transferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAbertoPorUsuario(vendedorDto, desistencia);
//		}
//		
//		this.finalizarInserirProdutoTransferencia(transferenciaEmAberto, contaBancariaGatewayDePagamento, rastreamento, desistencia);
//		
//	}
//	
//	
//	/*
//	 * metodo usado no retorno de valores para o usuario
//	 */
//	
////	@Override
////	@Transactional(propagation = Propagation.REQUIRED)
////	public void inserirProdutoVendidoNaTransferenciaDeValoresUsuario(UsuarioDTO compradorDto, ContaBancariaGatewayDePagamentoDTO contaBancariaGatewayDePagamentoDto, InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto, Boolean desistencia) {
////		
////		Usuario comprador = dozerBeanMapper.map(compradorDto, Usuario.class);
////		InfosGeraisProdutoUsuario infosGeraisProdutoUsuario = dozerBeanMapper.map(infosGeraisProdutoUsuarioDto, InfosGeraisProdutoUsuario.class);
////		
////		ContaBancariaGatewayDePagamento contaBancariaGatewayDePagamento = null;
////		if(contaBancariaGatewayDePagamentoDto != null) {
////			contaBancariaGatewayDePagamento = dozerBeanMapper.map(contaBancariaGatewayDePagamentoDto, ContaBancariaGatewayDePagamento.class);
////		}
////		
////
////		this.finalizarInserirProdutoTransferencia(null, contaBancariaGatewayDePagamento, infosGeraisProdutoUsuario, comprador, desistencia);
////	}
//	
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<InfosGeraisProdutoUsuarioDTO> verificarComprasFinalizadasComTransferencias(List<CompraDTO> listaDeComprasFinalizadasDto) throws UsuarioException, CompraVendaException {
//		
//		logger.info("verificarComprasFinalizadasComTransferencias(List<CompraDTO> listaDeComprasFinalizadasDto)");
//		
//		List<InfosGeraisProdutoUsuarioDTO> listaProdutosRetornoDto = new ArrayList<InfosGeraisProdutoUsuarioDTO>();
//		
//		
//		for(CompraDTO compraDto : listaDeComprasFinalizadasDto) {
//			UsuarioDTO usuariodto = compraDto.getProdutosComprados().get(0).getUsuario();
//		
//			if(usuariodto == null) {
//				
//				throw new UsuarioException("Não foi encontrado o usuario deste produto");
//			}
//		
//
//			Usuario usuario = dozerBeanMapper.map(usuariodto, Usuario.class);
//			
//			List<TransferenciaValoresUsuario> listaDeTransferenciaValoresUsuarioDatabase = transferenciaValoresUsuarioDao.obterTransferenciaPorUsuario(usuario);
//			
//			List<TransferenciaValoresUsuarioDTO> listaDeTransferenciaValoresUsuarioDto = new ArrayList<TransferenciaValoresUsuarioDTO>();
//			for(TransferenciaValoresUsuario transferenciaValoresUsuario : listaDeTransferenciaValoresUsuarioDatabase) {
//				for(StatusTransferenciaDeValor statusTransferencia : transferenciaValoresUsuario.getListaStatusTransferenciaDeValor()) {
//					if(statusTransferencia.getAtivo() && statusTransferencia.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED)) {
//						TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto = dozerBeanMapper.map(transferenciaValoresUsuario, TransferenciaValoresUsuarioDTO.class);
//						
//						listaDeTransferenciaValoresUsuarioDto.add(transferenciaValoresUsuarioDto);
//					}
//				}
//			}
//			
//			
//			for(InfosGeraisProdutoUsuarioDTO produtoDto : compraDto.getProdutosComprados()) {
//				InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto = this.verificacaoInfosGeraisProdutosUsuariosComTransferencia(produtoDto, listaDeTransferenciaValoresUsuarioDto);
//			
//				if(infosGeraisProdutoUsuarioDto != null) {
//					listaProdutosRetornoDto.add(infosGeraisProdutoUsuarioDto);
//				}
//			}
//		
//		}
//		
//	
//		return listaProdutosRetornoDto;
//	}
//
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void atualizarProdutoVendidoNaTransferenciaDeValores(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto, Boolean desistencia) {
//		
//		logger.info("atualizarProdutoVendidoNaTransferenciaDeValores(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto, Boolean desistencia)");
//		
//		InfosGeraisProdutoUsuario infosGeraisProdutoUsuario = dozerBeanMapper.map(infosGeraisProdutoUsuarioDto, InfosGeraisProdutoUsuario.class);
//		TransferenciaValoresUsuario transferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAbertoPorUsuario(infosGeraisProdutoUsuario.getUsuario(), desistencia);
//		
//		if(transferenciaEmAberto != null) {
//			List<StatusTransferenciaDeValor> listaTransferenciaUsuario = statusTransferenciaDeValorDao.obterStatusTransferenciaPorTransferencia(transferenciaEmAberto);
//			
//			Boolean atualizarDados = Boolean.TRUE;
//			for(StatusTransferenciaDeValor transferenciaUsuario : listaTransferenciaUsuario) {
//				if(transferenciaUsuario.getAtivo() && !transferenciaUsuario.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED)) {
//					atualizarDados = Boolean.FALSE;
//				}
//			}
//			
//			
//			if(atualizarDados) {
//				ArrayList<InfosGeraisProdutoUsuario> listaInfosGeraisProdutoUsuarioDatabaseRemovidos = new ArrayList<InfosGeraisProdutoUsuario>();
//				for(InfosGeraisProdutoUsuario infosGeraisProdutoUsuarioDatabase : transferenciaEmAberto.getListaInfosGeraisProdutoUsuario()) {
//					
//					if(infosGeraisProdutoUsuarioDatabase.getId().equals(infosGeraisProdutoUsuario.getId())) {
//						listaInfosGeraisProdutoUsuarioDatabaseRemovidos.add(infosGeraisProdutoUsuarioDatabase);
//					}
//				}
//				
//				for(InfosGeraisProdutoUsuario infosGeraisProdutoUsuarioDatabaseRemovido : listaInfosGeraisProdutoUsuarioDatabaseRemovidos) {
//					transferenciaEmAberto.getListaInfosGeraisProdutoUsuario().remove(infosGeraisProdutoUsuarioDatabaseRemovido);
//				}
//				
//				
//				transferenciaValoresUsuarioDao.update(transferenciaEmAberto);
//				transferenciaValoresUsuarioDao.flush();
//			}
//		}
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void excluirProdutoVendidoNaTransferenciaDeValores(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto, Boolean desistencia) {
//		
//		logger.info("excluirProdutoVendidoNaTransferenciaDeValores(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto, Boolean desistencia)");
//		
//		InfosGeraisProdutoUsuario infosGeraisProdutoUsuario = dozerBeanMapper.map(infosGeraisProdutoUsuarioDto, InfosGeraisProdutoUsuario.class);
//		TransferenciaValoresUsuario transferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAbertoPorUsuario(infosGeraisProdutoUsuario.getUsuario(), desistencia);
//
//		Boolean deletarTransferencia = Boolean.TRUE;
//		if(transferenciaEmAberto != null) {
//			List<StatusTransferenciaDeValor> listaTransferenciaUsuario = statusTransferenciaDeValorDao.obterStatusTransferenciaPorTransferencia(transferenciaEmAberto);
//			
//			for(StatusTransferenciaDeValor transferenciaUsuario : listaTransferenciaUsuario) {
//				if(transferenciaUsuario.getAtivo() && !transferenciaUsuario.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED)) {
//					deletarTransferencia = Boolean.FALSE;
//				}
//			}
//		}
//		
//		
//		
//		
//		if((transferenciaEmAberto.getListaInfosGeraisProdutoUsuario() == null || transferenciaEmAberto.getListaInfosGeraisProdutoUsuario().isEmpty()) && deletarTransferencia) {
//			
//			transferenciaValoresUsuarioDao.delete(transferenciaEmAberto);
//		
//		}
//	}
//
//
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void fecharTransferenciaDeValores(UsuarioDTO usuarioDto, Boolean desistencia) throws TransferenciaValoresUsuarioException, IOException, GatewayDePagamentoException, UsuarioException {
//		
//		logger.info("fecharTransferenciaDeValores(UsuarioDTO usuarioDto, Boolean desistencia)");
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		TransferenciaValoresUsuario transferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAbertoPorUsuario(usuario, desistencia);
//		
//		if(desistencia) {
//			transferenciaEmAberto.setPorcentagem(BigDecimal.ZERO);
//		} else {
//			PorcentagemMercadoDeMusica porcentagemAtiva = porcentagemMercadoDeMusicaDao.obterPorcentagemAtiva();
//			transferenciaEmAberto.setPorcentagem(porcentagemAtiva.getPorcentagem());
//		}
//		
//		
//		UsuarioDTO usuarioDonoDaTransferencia = dozerBeanMapper.map(transferenciaEmAberto.getUsuario(), UsuarioDTO.class);
//		
//		
//		VerificacaoDeUsuarios.verificarUsuariosParaCrud(usuarioDto, usuarioDonoDaTransferencia);
//		
//		if(transferenciaEmAberto != null && transferenciaEmAberto.getDesistencia().equals(Boolean.FALSE)) {
//			List<StatusTransferenciaDeValor> listaTransferenciaUsuario = statusTransferenciaDeValorDao.obterStatusTransferenciaPorTransferencia(transferenciaEmAberto);
//			
//			for(StatusTransferenciaDeValor transferenciaUsuario : listaTransferenciaUsuario) {
//				if(transferenciaUsuario.getAtivo() && !transferenciaUsuario.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED)) {
//					transferenciaEmAberto = null;
//					throw new TransferenciaValoresUsuarioException("Apontamos um problema com uma de suas transferências. Por favor entre em contato conosco");
//				}
//			}
//		}
//		
//		this.transferenciaEmAbertoParaPedidoDeRequisicao(transferenciaEmAberto);
//	}
//
//	
//	@Override
//	@Transactional(readOnly = true)
//	public HashMap<String, Object> obterProdutosPagos(UsuarioDTO usuarioDto) {
//			
//		logger.info("obterProdutosPagos(UsuarioDTO usuarioDto)");
//		
//		HashMap<String, Object> hashRetorno = new HashMap<String, Object>();
//		BigDecimal valorTotal = BigDecimal.ZERO;
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);		
//		
//		/*
//		 * produtos já finalizados pelo gateway de pagamento e aqueles que foram enviados mas ainda não foram depositados
//		 */
//		List<TransferenciaValoresUsuario> listaDeTransferencias = transferenciaValoresUsuarioDao.obterListaTransferenciaEmAbertoPorUsuario(usuario);
//		listaDeTransferencias.addAll(transferenciaValoresUsuarioDao.obterTransferenciaFinalizadasPorUsuario(usuario));
//		
//		
//		List<TransferenciaValoresUsuarioDTO> listaDeTransferenciasDto = new ArrayList<TransferenciaValoresUsuarioDTO>();
//		
//		for(TransferenciaValoresUsuario transferencia : listaDeTransferencias) {
//			
//			/*
//			 * primeiro passamos um for nos status para ver se existe apenas o started
//			 */
//			for(StatusTransferenciaDeValor statusDatabase : transferencia.getListaStatusTransferenciaDeValor()) {
//				if(statusDatabase.getAtivo() && !statusDatabase.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED)) {
//					
//					TransferenciaValoresUsuarioDTO transferenciaDto = dozerBeanMapper.map(transferencia, TransferenciaValoresUsuarioDTO.class);
//					
//					for(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto : transferenciaDto.getListaInfosGeraisProdutoUsuario()) {
//						BigDecimal precoComPorcentagem = infosGeraisProdutoUsuarioDto.getPreco().multiply(transferenciaDto.getPorcentagem()).divide(BigDecimal.valueOf(100));
//						BigDecimal precoFinal = infosGeraisProdutoUsuarioDto.getPreco().subtract(precoComPorcentagem);
//						
//						infosGeraisProdutoUsuarioDto.setPrecoString(new Precos().padraoAmericanoParaBrasileiro(infosGeraisProdutoUsuarioDto.getPreco()));
//						infosGeraisProdutoUsuarioDto.setPrecoLiquidoString(new Precos().padraoAmericanoParaBrasileiro(precoFinal));
//						
//						valorTotal = valorTotal.add(precoFinal);
//					}
//					
//					
//					listaDeTransferenciasDto.add(transferenciaDto);	
//					
//				}
//			}
//		}
//		
//		hashRetorno.put("preco", new Precos().padraoAmericanoParaBrasileiro(valorTotal));
//		hashRetorno.put("lista", listaDeTransferenciasDto);
//		return hashRetorno;
//	}
//
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<TransferenciaValoresUsuarioDTO> obterTransferenciasEmAbertoEnviadasGatewayDePagamento() {
//		
//		logger.info("obterTransferenciasEmAbertoEnviadasGatewayDePagamento()");
//		
//		List<TransferenciaValoresUsuario> listaDeTransferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAberto();
//		
//		List<TransferenciaValoresUsuarioDTO> listaTransferenciaEmAbertoDto = new ArrayList<TransferenciaValoresUsuarioDTO>();
//		for(TransferenciaValoresUsuario transferenciaEmAberto : listaDeTransferenciaEmAberto) {
//			for(StatusTransferenciaDeValor statusTransferenciaValor : transferenciaEmAberto.getListaStatusTransferenciaDeValor()) {
//				
//				if(statusTransferenciaValor.getAtivo() && !statusTransferenciaValor.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED) && !statusTransferenciaValor.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.COMPLETED)) {
//					TransferenciaValoresUsuarioDTO transferenciaEmAbertoDto = dozerBeanMapper.map(transferenciaEmAberto, TransferenciaValoresUsuarioDTO.class);
//					listaTransferenciaEmAbertoDto.add(transferenciaEmAbertoDto);
//				}
//			}
//		}
//		
//		return listaTransferenciaEmAbertoDto;
//	}
//
//
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void atualizarTransferenciaValoresUsuario(TransferenciaValoresUsuarioDTO transferenciaDeValoresUsuarioDto) {
//		
//		logger.info("atualizarTransferenciaValoresUsuario(TransferenciaValoresUsuarioDTO transferenciaDeValoresUsuarioDto)");
//		
//		TransferenciaValoresUsuario transferenciaDeValoresUsuario = dozerBeanMapper.map(transferenciaDeValoresUsuarioDto, TransferenciaValoresUsuario.class);
//		
//		transferenciaValoresUsuarioDao.update(transferenciaDeValoresUsuario);
//		transferenciaValoresUsuarioDao.flush();
//	}
//	
//	
////	@Override
////	@Transactional(readOnly = true)
////	public List<TransferenciaValoresUsuarioDTO> obterTransferenciaEmAbertoDesistenciaPorUsuario(UsuarioDTO usuarioDto) {
////		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
////		
////		List<TransferenciaValoresUsuario> listaDeTransferenciaEmAberto = transferenciaValoresUsuarioDao.obterListaTransferenciaEmAbertoPorUsuario(usuario);
////		
////		List<TransferenciaValoresUsuarioDTO> listaTransferenciaEmAbertoDto = new ArrayList<TransferenciaValoresUsuarioDTO>();
////		for(TransferenciaValoresUsuario transferenciaEmAberto : listaDeTransferenciaEmAberto) {
////			for(StatusTransferenciaDeValor statusTransferenciaValor : transferenciaEmAberto.getListaStatusTransferenciaDeValor()) {
////				
////				if(statusTransferenciaValor.getAtivo() && statusTransferenciaValor.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED) &&
////						transferenciaEmAberto.getDesistencia().equals(Boolean.TRUE)) {
////					TransferenciaValoresUsuarioDTO transferenciaEmAbertoDto = dozerBeanMapper.map(transferenciaEmAberto, TransferenciaValoresUsuarioDTO.class);
////					
////					for(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto : transferenciaEmAbertoDto.getListaInfosGeraisProdutoUsuario()) {
////						infosGeraisProdutoUsuarioDto.setPrecoString(new Precos().padraoAmericanoParaBrasileiro(infosGeraisProdutoUsuarioDto.getPreco()));
////					}
////					listaTransferenciaEmAbertoDto.add(transferenciaEmAbertoDto);
////				}
////			}
////		}
////		
////		return listaTransferenciaEmAbertoDto;
////	}
//
//
////	@Override
////	public void atualizarTransferenciaDeValoresParaGatewayDePagamento(TransferenciaValoresUsuarioDTO transferenciaDeValoresUsuarioDto) {
//		
//		/*
//	     * OAUTH do Mercado de Música 
//	     */
//	    
////	    OAuth auth = new OAuth(MoipAutenticacao.TOKEN);
////	    
////	    
////		try {
////			 URL urlPagamentoTransferencia = new URL("https://" + MoipAutenticacao.LUGAR + "/v2/transfers/" + transferenciaDeValoresUsuarioDto.getTransferenciaGatewayDePagamento());
////			 
////			 HttpURLConnection connectionTransferencia = (HttpURLConnection) urlPagamentoTransferencia.openConnection();            
////				connectionTransferencia.setRequestMethod("GET");
////				connectionTransferencia.setDoInput(true);
////				
////				connectionTransferencia.setRequestProperty("Accept-Charset","utf-8");		
////				connectionTransferencia.setRequestProperty("Authorization","OAuth " + auth.getAccessToken());  
////				connectionTransferencia.setRequestProperty("Content-Type", "application/json;charset=utf-8");
////				
////				connectionTransferencia.connect();
////				
////				JSONObject retornoTransferencia = null;
////				if(connectionTransferencia.getResponseCode() == HttpURLConnection.HTTP_OK) {
////					
////					BufferedReader brTransferencia = new BufferedReader(new InputStreamReader(connectionTransferencia.getInputStream()));    
////				    String newDataTransferencia = "";    
////				    String sTransferencia = "";    
////				    while (null != ((sTransferencia = brTransferencia.readLine()))) {    
////				    	newDataTransferencia += sTransferencia;    
////				    }    
////				    brTransferencia.close();    
////				    
////				    retornoTransferencia = new JSONObject(newDataTransferencia);
////				    
////				    String statusTransferencia = retornoTransferencia.getString("status");
////				    
////				    for(StatusTransferenciaDeValorDTO statusValor : transferenciaDeValoresUsuarioDto.getListaStatusTransferenciaDeValor()) {
////				    	statusValor.setAtivo(Boolean.FALSE);
////				    }
////				    
////				    Calendar dataDeHoje = Calendar.getInstance();
////				    
////				    StatusTransferenciaDeValorDTO statusTransferenciaDeValorDto = new StatusTransferenciaDeValorDTO();
////				    statusTransferenciaDeValorDto.setAtivo(Boolean.TRUE);
////				    statusTransferenciaDeValorDto.setDataTransferencia(dataDeHoje);
////				    statusTransferenciaDeValorDto.setStatusTransferenciaDeValoresGatewayDePagamento(StatusTransferenciaDeValoresGatewayDePagamentoEnum.valueOf(statusTransferencia.toUpperCase()));
////				    statusTransferenciaDeValorDto.setTransferenciaValoresUsuario(transferenciaDeValoresUsuarioDto);
////				    
////				    transferenciaDeValoresUsuarioDto.getListaStatusTransferenciaDeValor().add(statusTransferenciaDeValorDto);
////				    
////				    if(statusTransferencia.equals("COMPLETED")) {
////				    	transferenciaDeValoresUsuarioDto.setFinalizado(Boolean.TRUE);
////				    }
////				    
////				    this.atualizarTransferenciaValoresUsuario(transferenciaDeValoresUsuarioDto);
////				
////				} else {
////					
////					logger.error("erro de transferencia de valores Cron");
////					
////					InputStream erro = connectionTransferencia.getErrorStream();
////					
////					BufferedReader br = new BufferedReader(new InputStreamReader(erro));    
////				    String newData = "";    
////				    String s = "";    
////				    while (null != ((s = br.readLine()))) {    
////				        newData += s;    
////				    }    
////				    br.close();    
////				    
////				    JSONObject retorno = new JSONObject(newData);
////				    JSONArray erros = (JSONArray) retorno.get("errors");
////				    
////				    
////				   Boolean erroNormal = false;
////				   String descricaoDeErros = "";
////				   for(int i = 0; i < erros.length(); i++) {
////				    	JSONObject erroObjeto = erros.getJSONObject(i);
////				    	
////				    	erroNormal = true;
////				    	descricaoDeErros = descricaoDeErros + ", " + erroObjeto.getString("description");
////				    }
////				    
////				   	
////				    if(erroNormal) {
////				    	
////				    	logger.error("Não foi transferir os valores pelo MOIP. Erro(s): " + descricaoDeErros );		    	
////				    }
////					
////				}
////			 
////		} catch (IOException e) {
////			
////			logger.error(e.getMessage());
////		}
//		
////	}
//	
//	@Override
//	@Transactional(readOnly = true)
//	public TransferenciaValoresUsuarioDTO obterPorTransferencia(TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto) {
//		
//		logger.info("obterPorTransferencia(TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto)");
//		
//		TransferenciaValoresUsuario transferenciaValoresUsuario = dozerBeanMapper.map(transferenciaValoresUsuarioDto, TransferenciaValoresUsuario.class);
//		
//		transferenciaValoresUsuario = transferenciaValoresUsuarioDao.selectOne(transferenciaValoresUsuario.getId());
//		
//		return dozerBeanMapper.map(transferenciaValoresUsuario, TransferenciaValoresUsuarioDTO.class);
//	}
//
//
//
////	@Override
////	@Transactional(propagation = Propagation.REQUIRED)
////	public void fecharTransferenciaDeValoresDesistencia(TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto) throws TransferenciaValoresUsuarioException, GatewayDePagamentoException, IOException, CompraVendaException {
////		
////		logger.info("fecharTransferenciaDeValoresDesistencia(TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto)");
////		
////		TransferenciaValoresUsuario transferenciaValoresUsuario = dozerBeanMapper.map(transferenciaValoresUsuarioDto, TransferenciaValoresUsuario.class);
////		
////		if(transferenciaValoresUsuario != null && transferenciaValoresUsuario.getDesistencia().equals(Boolean.TRUE)) {
////			List<StatusTransferenciaDeValor> listaTransferenciaUsuario = statusTransferenciaDeValorDao.obterStatusTransferenciaPorTransferencia(transferenciaValoresUsuario);
////			
////			for(StatusTransferenciaDeValor transferenciaUsuario : listaTransferenciaUsuario) {
////				if(transferenciaUsuario.getAtivo() && !transferenciaUsuario.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED)) {
////					transferenciaValoresUsuario = null;
////				}
////			}
////		}
////		
////		
////		if(transferenciaValoresUsuario == null || (transferenciaValoresUsuario != null && transferenciaValoresUsuario.getDesistencia().equals(Boolean.FALSE))) {
////			throw new TransferenciaValoresUsuarioException("Não existe transferencia para esse usuario");
////		}
////		
////		this.enviarReembolso(transferenciaValoresUsuario);
////	}
//
//	@Override	
//	public Boolean verificarEscolhaDeContaBancaria(CompraBoletoDTO compraBoletoDto, CompraCartaoDeCreditoDTO compraCartaoDeCreditoDto) throws TransferenciaValoresUsuarioException {
//		
//		logger.info("verificarEscolhaDeContaBancaria(CompraBoletoDTO compraBoletoDto, CompraCartaoDeCreditoDTO compraCartaoDeCreditoDto)");
//		
//		if(compraBoletoDto != null && compraCartaoDeCreditoDto == null) {
//			
//			return Boolean.TRUE;
//		
//		} else if(compraBoletoDto == null && compraCartaoDeCreditoDto != null) {
//		
//			return Boolean.FALSE;
//		
//		} else {
//			throw new TransferenciaValoresUsuarioException("Não existem os tipo de pagamento associados a essa compra");
//		}
//	}
//
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public Boolean verificarTransferenciaAtiva(UsuarioDTO usuarioDto) {
//		
//		logger.info("verificarTransferenciaAtiva(UsuarioDTO usuarioDto)");
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		List<TransferenciaValoresUsuario> listaDeTransferenciaValoresUsuarioDatabase = transferenciaValoresUsuarioDao.obterTransferenciaPorUsuario(usuario);
//
//		for(TransferenciaValoresUsuario transferenciaValoresUsuarioDatabase : listaDeTransferenciaValoresUsuarioDatabase) {
//			TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto = dozerBeanMapper.map(transferenciaValoresUsuarioDatabase, TransferenciaValoresUsuarioDTO.class);
//			
//			for(StatusTransferenciaDeValorDTO statusTransferenciaDeValorDto : transferenciaValoresUsuarioDto.getListaStatusTransferenciaDeValor()) {
//				if(!transferenciaValoresUsuarioDto.getFinalizado() && (statusTransferenciaDeValorDto.getAtivo() && statusTransferenciaDeValorDto.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED))) {
//					return Boolean.TRUE;
//				}
//			}
//		}
//		
//		return Boolean.FALSE;
//	}
//
////	@Override
////	public void inserirTransferenciaDeValoresDesistencia(DesistenciaProdutoDTO desistenciaDto, ContaBancariaDTO contaBancariaDto) {
////		TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto = new TransferenciaValoresUsuarioDTO();
////		
////		if(contaBancariaGatewayDePagamentoDto != null) {
////			transferenciaValoresUsuarioDto.setContaUsadaNaTransferencia(contaBancariaGatewayDePagamentoDto);
////		}
////		
////		if(desistenciaDto.getRastreamento().getFinalizado().equals(Boolean.TRUE)) {
////			transferenciaValoresUsuarioDto.setDesistencia(Boolean.TRUE);
////			
////			List<InfosGeraisProdutoUsuarioDTO> listaDeInfosGeraisProdutoUsuario = new ArrayList<InfosGeraisProdutoUsuarioDTO>();
////			listaDeInfosGeraisProdutoUsuario.add(desistenciaDto.getRastreamento().getInfosGeraisProdutoUsuario());
////			transferenciaValoresUsuarioDto.setListaInfosGeraisProdutoUsuario(listaDeInfosGeraisProdutoUsuario);
////			
////			StatusTransferenciaDeValorDTO statusTransferenciaDeValorDto = new StatusTransferenciaDeValorDTO();
////			statusTransferenciaDeValorDto.setAtivo(Boolean.TRUE);
////			statusTransferenciaDeValorDto.setDataTransferencia(Calendar.getInstance());
////			statusTransferenciaDeValorDto.setStatusTransferenciaDeValoresGatewayDePagamento(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED);
////			statusTransferenciaDeValorDto.setTransferenciaValoresUsuario(transferenciaValoresUsuarioDto);
////			
////			List<StatusTransferenciaDeValorDTO> listaDeTransferenciaDeValorDto = new ArrayList<StatusTransferenciaDeValorDTO>();
////			listaDeTransferenciaDeValorDto.add(statusTransferenciaDeValorDto);
////			
////			
////			transferenciaValoresUsuarioDto.setListaStatusTransferenciaDeValor(listaDeTransferenciaDeValorDto);
////			transferenciaValoresUsuarioDto.setPorcentagem(BigDecimal.ZERO);
////			transferenciaValoresUsuarioDto.setUsuario(desistenciaDto.getRastreamento().getComprador());
////			
////			
////			/*
////			 * aqui verificamos se o produto é pelos correios ou nao. Também verificamos se ele foi enviado ao comprador ou nao
////			 */
////			
////			if(desistenciaDto.getRastreamento() instanceof RastreamentoProdutoCorreiosDTO) {
////				RastreamentoProdutoCorreiosDTO rastreamentoCorreiosDto = (RastreamentoProdutoCorreiosDTO) desistenciaDto.getRastreamento();
////				
////				Boolean entregue = Boolean.FALSE;
////				for(StatusPostagemProdutoCorreioDTO statusPostagemProdutoCorreios : rastreamentoCorreiosDto.getStatusPostagemProdutoCorreio()) {
////					if(statusPostagemProdutoCorreios.getStatus().equals(StatusPostagemCorreiosEnum.Entregue)) {
////						entregue = Boolean.TRUE;
////					}
////				}
////				
////				if(entregue) {
////					transferenciaValoresUsuarioDto.setValorTransferido(rastreamentoCorreiosDto.getValorPostagemProdutoCorreio().add(rastreamentoCorreiosDto.getInfosGeraisProdutoUsuario().getPreco()));
////				} else {
////					transferenciaValoresUsuarioDto.setValorTransferido(rastreamentoCorreiosDto.getInfosGeraisProdutoUsuario().getPreco());
////				}				
////			
////			} else {
////				transferenciaValoresUsuarioDto.setValorTransferido(desistenciaDto.getRastreamento().getInfosGeraisProdutoUsuario().getPreco());
////			}
////			
////			
////			TransferenciaValoresUsuario transferenciaValoresUsuario = dozerBeanMapper.map(transferenciaValoresUsuarioDto, TransferenciaValoresUsuario.class);
////			transferenciaValoresUsuarioDao.update(transferenciaValoresUsuario);
////			
////		} 
////	}
//	
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<TransferenciaValoresUsuarioDTO> obterTransferenciasPorUsuario(UsuarioDTO usuarioDto) {
//		
//		logger.info("obterTransferenciasPorUsuario(UsuarioDTO usuarioDto)");
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		
//		ArrayList<TransferenciaValoresUsuarioDTO> listaTransferenciaValoresUsuarioDto = new ArrayList<TransferenciaValoresUsuarioDTO>();
//		
//		
//		List<TransferenciaValoresUsuario> listaDeTransferenciaValoresUsuarioDatabase = transferenciaValoresUsuarioDao.obterTransferenciaPorUsuario(usuario);
//	
//		for(TransferenciaValoresUsuario transferenciaValorUsuario : listaDeTransferenciaValoresUsuarioDatabase) {
//			TransferenciaValoresUsuarioDTO transferenciaValorUsuarioDto = dozerBeanMapper.map(transferenciaValorUsuario, TransferenciaValoresUsuarioDTO.class);
//		
//			listaTransferenciaValoresUsuarioDto.add(transferenciaValorUsuarioDto);
//		}
//	
//		return listaTransferenciaValoresUsuarioDto;
//	}
//	
//	
//	@Override
//	@Transactional(readOnly = true)
//	public InfosGeraisProdutoUsuarioDTO verificacaoInfosGeraisProdutosUsuariosComTransferencia(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDTO, List<TransferenciaValoresUsuarioDTO> listaDeTransferenciaValoresUsuarioDto) {
//		
//		logger.info("verificacaoInfosGeraisProdutosUsuariosComTransferencia(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDTO, List<TransferenciaValoresUsuarioDTO> listaDeTransferenciaValoresUsuarioDto)");
//		
//		Boolean temArquivoDeTransferencia = Boolean.FALSE;
//		Boolean transferenciaFinalizada = Boolean.FALSE;
//		
//		for(TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto : listaDeTransferenciaValoresUsuarioDto) {
//			
//			for(StatusTransferenciaDeValorDTO statusTransferenciaDeValorDto : transferenciaValoresUsuarioDto.getListaStatusTransferenciaDeValor()) {
//				if(transferenciaValoresUsuarioDto.getFinalizado() || (statusTransferenciaDeValorDto.getAtivo() && !statusTransferenciaDeValorDto.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED))) {
//					transferenciaFinalizada = Boolean.TRUE;
//				}
//			}
//			
//			for(InfosGeraisProdutoUsuarioDTO infoProdutoDatabaseDto : transferenciaValoresUsuarioDto.getListaInfosGeraisProdutoUsuario()) {
//				if(infosGeraisProdutoUsuarioDTO.getId().equals(infoProdutoDatabaseDto.getId())) {
//					temArquivoDeTransferencia = Boolean.TRUE;
//				}
//			}
//			
//		}
//		
//		
////		verificacao caso não tenha no arquivo de transferencia ou o arquivo não é finalizado
//		if(!temArquivoDeTransferencia) {
//			
//			infosGeraisProdutoUsuarioDTO.setArquivoDeTransferencia(Boolean.FALSE);		
//			return infosGeraisProdutoUsuarioDTO;
//			
//		} else if(temArquivoDeTransferencia && !transferenciaFinalizada) {
//			
//			infosGeraisProdutoUsuarioDTO.setArquivoDeTransferencia(Boolean.TRUE);		
//			return infosGeraisProdutoUsuarioDTO;
//		
//		} else {
//			return null;
//		}
//	}
//	
//	@Override
//	@Transactional(readOnly = true)
//	public TransferenciaValoresUsuarioDTO obterTransferenciaPorProduto(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto) {
//		
//		logger.info("obterTransferenciaPorProduto(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto)");
//		
//		InfosGeraisProdutoUsuario infosGeraisProdutoUsuario = dozerBeanMapper.map(infosGeraisProdutoUsuarioDto, InfosGeraisProdutoUsuario.class);
//		TransferenciaValoresUsuario transferenciaValoresUsuario = transferenciaValoresUsuarioDao.obterPorInfosGeraisProdutoUsuario(infosGeraisProdutoUsuario);
//		if(transferenciaValoresUsuario != null) {
//			return dozerBeanMapper.map(transferenciaValoresUsuario, TransferenciaValoresUsuarioDTO.class);
//		}
//		
//		return null;
//	}
//
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<TransferenciaValoresUsuarioDTO> obterTransferenciasRequisitadasGatewayDePagamento() {
//		
//		logger.info("obterTransferenciasRequisitadasGatewayDePagamento()");
//		
//		
//		List<TransferenciaValoresUsuario> listaDeTransferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAberto();
//		
//		List<TransferenciaValoresUsuarioDTO> listaTransferenciaEmAbertoDto = new ArrayList<TransferenciaValoresUsuarioDTO>();
//		for(TransferenciaValoresUsuario transferenciaEmAberto : listaDeTransferenciaEmAberto) {
//			for(StatusTransferenciaDeValor statusTransferenciaValor : transferenciaEmAberto.getListaStatusTransferenciaDeValor()) {
//				
//				if(statusTransferenciaValor.getAtivo() && statusTransferenciaValor.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.REQUESTED)) {
//					TransferenciaValoresUsuarioDTO transferenciaEmAbertoDto = dozerBeanMapper.map(transferenciaEmAberto, TransferenciaValoresUsuarioDTO.class);
//					listaTransferenciaEmAbertoDto.add(transferenciaEmAbertoDto);
//				}
//			}
//		}
//		
//		return listaTransferenciaEmAbertoDto;
//	}
//	
//	
//	@Transactional(propagation = Propagation.NESTED)
//	private void transferenciaEmAbertoParaPedidoDeRequisicao(TransferenciaValoresUsuario transferenciaValoresUsuario) throws IOException, GatewayDePagamentoException {
//		logger.info("transferenciaEmAbertoParaPedidoDeRequisicao(TransferenciaValoresUsuario transferenciaValoresUsuario)");
//		
//		for(StatusTransferenciaDeValor statusTransferenciaValor : transferenciaValoresUsuario.getListaStatusTransferenciaDeValor()){
//			statusTransferenciaValor.setAtivo(Boolean.FALSE);
//		}
//		
//		StatusTransferenciaDeValor statusTransferenciaDeValor = new StatusTransferenciaDeValor();
//		statusTransferenciaDeValor.setAtivo(Boolean.TRUE);
//		statusTransferenciaDeValor.setDataTransferencia(Calendar.getInstance());
//		statusTransferenciaDeValor.setTransferenciaValoresUsuario(transferenciaValoresUsuario);
//		statusTransferenciaDeValor.setStatusTransferenciaDeValoresGatewayDePagamento(StatusTransferenciaDeValoresGatewayDePagamentoEnum.REQUESTED);
//		transferenciaValoresUsuario.getListaStatusTransferenciaDeValor().add(statusTransferenciaDeValor);
//		
//		transferenciaValoresUsuarioDao.update(transferenciaValoresUsuario);
//	}
//	
//	
////	@Transactional(propagation = Propagation.REQUIRED)
////	private void enviarReembolso(TransferenciaValoresUsuario transferenciaValoresUsuario) throws CompraVendaException, IOException, GatewayDePagamentoException {
//		
//	    /*
//	     * OAUTH do Mercado de Música 
//	     */
////	    Compra compra = null;
////	    JSONObject jsonFinal = new JSONObject();
////	    BigDecimal valorTotalRetorno = BigDecimal.ZERO;
////	    ArrayList<BigInteger> listaDeIdsParaPagamento = new ArrayList<BigInteger>();
////	    for(InfosGeraisProdutoUsuario infosGeraisProdutoUsuario : transferenciaValoresUsuario.getListaInfosGeraisProdutoUsuario()) {
////	    	compra  = infosGeraisProdutoUsuario.getCompra();
////	    	
////	    	/*
////	    	 * está chegando objetos duplicados
////	    	 */
////	    	Boolean inserirArrayDeId = Boolean.TRUE;
////	    	for(BigInteger idPagamento : listaDeIdsParaPagamento) {
////	    		
////	    		if(idPagamento.equals(infosGeraisProdutoUsuario.getId())) {
////
////	    			inserirArrayDeId = Boolean.FALSE;
////		    	}
////	    	}
////	    	
////	    	if(inserirArrayDeId) {
////	    		valorTotalRetorno = valorTotalRetorno.add(infosGeraisProdutoUsuario.getPreco());
////	    		listaDeIdsParaPagamento.add(infosGeraisProdutoUsuario.getId());
////	    	}
////	    }
////	    
////	    
////	    if(transferenciaValoresUsuario.getContaUsadaNaTransferencia() != null && transferenciaValoresUsuario.getContaUsadaNaTransferencia().getId() != null ) {
////	    	
////	    	JSONObject refundingInstrument = new JSONObject();
////	    	JSONObject bankAccount = new JSONObject();
////	    	
////	    	bankAccount.put("type", "CHECKING");
////	    	bankAccount.put("bankNumber", transferenciaValoresUsuario.getContaUsadaNaTransferencia().getNumeroDoBanco().getNumero());
////	    	bankAccount.put("agencyNumber", transferenciaValoresUsuario.getContaUsadaNaTransferencia().getNumeroDaAgencia());
////	    	bankAccount.put("agencyCheckNumber", transferenciaValoresUsuario.getContaUsadaNaTransferencia().getDigitoDaAgencia());
////	    	bankAccount.put("accountNumber", transferenciaValoresUsuario.getContaUsadaNaTransferencia().getNumeroDaConta());
////	    	bankAccount.put("accountCheckNumber", transferenciaValoresUsuario.getContaUsadaNaTransferencia().getDigitoDaConta());
////	    	
////	    	
////	    	JSONObject holder = new JSONObject();
////	    	JSONObject taxDocument = new JSONObject();
////	    	
////	    	if(transferenciaValoresUsuario.getUsuario() instanceof UsuarioPessoaJuridica) {
////	    		
////	    		UsuarioPessoaJuridica usuarioPj = (UsuarioPessoaJuridica) transferenciaValoresUsuario.getUsuario();
////	    		holder.put("fullname", usuarioPj.getRazaoSocial());
////	    		taxDocument.put("type", "CNPJ");
////	    		taxDocument.put("number", usuarioPj.getCnpj());
////	    		
////	    	} else {
////	    		UsuarioPessoaFisica usuarioPf = (UsuarioPessoaFisica) transferenciaValoresUsuario.getUsuario();
////	    		holder.put("fullname", usuarioPf.getNome() + " " + usuarioPf.getSobrenome());
////	    		taxDocument.put("type", "CPF");
////	    		taxDocument.put("number", usuarioPf.getCpf());
////	    	}
////	    	
////	    	holder.put("taxDocument", taxDocument);
////	    	bankAccount.put("holder", holder);
////	    	
////	    	refundingInstrument.put("method", "BANK_ACCOUNT");
////	    	refundingInstrument.put("bankAccount", bankAccount);
////	    	jsonFinal.put("refundingInstrument", refundingInstrument);
////		}	
////	    
////	    valorTotalRetorno = valorTotalRetorno.setScale(2, BigDecimal.ROUND_HALF_EVEN);
////    
////	    String valorTotalString = valorTotalRetorno.toString();
////	    valorTotalString = StringUtils.replace(valorTotalString, ".", "");
////		
////	    jsonFinal.put("amount", valorTotalString);
////		
////	    if(compra == null) {
////	    	throw new CompraVendaException("Não foi achado uma compra relacionada a esta transferência de valores dessitidos");
////	    }
////	    
////	    URL urlPagamentoTransferencia = new URL("https://" + MoipAutenticacao.LUGAR + "/v2/orders/" + compra.getIdPedido() +"/refunds");
////		
////		HttpURLConnection connectionTransferencia = (HttpURLConnection) urlPagamentoTransferencia.openConnection();            
////		connectionTransferencia.setRequestMethod("POST");
////		connectionTransferencia.setDoInput(true);
////		connectionTransferencia.setDoOutput(true);
////		connectionTransferencia.setRequestProperty("Accept-Charset","utf-8");		
////		connectionTransferencia.setRequestProperty("Authorization","Basic " + Base64.getEncoder().encodeToString(MoipAutenticacao.TOKEN_E_CHAVE.getBytes()));  
////		connectionTransferencia.setRequestProperty("Content-Type", "application/json;charset=utf-8");
////		
////		
////		OutputStream output = connectionTransferencia.getOutputStream();
////		output.write(jsonFinal.toString().getBytes("UTF-8"));
////		
////		output.flush();
////		output.close();
////		connectionTransferencia.connect();
////		
////		JSONObject retornoTransferencia = null;
////		if(connectionTransferencia.getResponseCode() == HttpURLConnection.HTTP_CREATED || connectionTransferencia.getResponseCode() == HttpURLConnection.HTTP_OK) {
////			
////			
//////			ajustar para conta corrente
////			
////			BufferedReader brTransferencia = new BufferedReader(new InputStreamReader(connectionTransferencia.getInputStream()));    
////		    String newDataTransferencia = "";    
////		    String sTransferencia = "";    
////		    while (null != ((sTransferencia = brTransferencia.readLine()))) {    
////		    	newDataTransferencia += sTransferencia;    
////		    }    
////		    brTransferencia.close();    
////		    
////		    retornoTransferencia = new JSONObject(newDataTransferencia);
////			
////		    Calendar dataDeHoje = Calendar.getInstance();
////		    
////		    transferenciaValoresUsuario.setTransferenciaGatewayDePagamento(retornoTransferencia.getString("id"));
////		    transferenciaValoresUsuario.setFinalizado(Boolean.TRUE);
////		    
////		    StatusTransferenciaDeValor statusTransferenciaDeValor = new StatusTransferenciaDeValor();
////		    statusTransferenciaDeValor.setDataTransferencia(dataDeHoje);
////		    statusTransferenciaDeValor.setStatusTransferenciaDeValoresGatewayDePagamento(StatusTransferenciaDeValoresGatewayDePagamentoEnum.valueOf(retornoTransferencia.getString("status").toUpperCase()));
////		    statusTransferenciaDeValor.setTransferenciaValoresUsuario(transferenciaValoresUsuario);
////		    statusTransferenciaDeValor.setAtivo(Boolean.TRUE);
////		    
////		    for(StatusTransferenciaDeValor statusTransferenciaDeValorDatabase : transferenciaValoresUsuario.getListaStatusTransferenciaDeValor()) {
////		    	statusTransferenciaDeValorDatabase.setAtivo(Boolean.FALSE);
////		    }
////		    
////		    transferenciaValoresUsuario.getListaStatusTransferenciaDeValor().add(statusTransferenciaDeValor);
////		    
////		    transferenciaValoresUsuarioDao.update(transferenciaValoresUsuario);
////		    transferenciaValoresUsuarioDao.flush();
////		    
////		} else if(connectionTransferencia.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
////			
////			throw new GatewayDePagamentoException("Não autorizado o reembolso");
////		
////		} else {
////			
////			InputStream erro = connectionTransferencia.getErrorStream();
////			
////			BufferedReader brTransferencia = new BufferedReader(new InputStreamReader(erro));    
////			String newDataTransferencia = "";    
////		    String sTransferencia = "";    
////		    while (null != ((sTransferencia = brTransferencia.readLine()))) {    
////		    	newDataTransferencia += sTransferencia;    
////		    }    
////		    brTransferencia.close();    
////		    
////		    retornoTransferencia = new JSONObject(newDataTransferencia); 
////		    JSONArray erros = (JSONArray) retornoTransferencia.get("errors");
////		    
////		    JSONObject erroObject = erros.getJSONObject(0);			    
////		    throw new GatewayDePagamentoException("Ocorreu um erro no reembolso. Erro(s): " +  erroObject.getString("description"));				
////		}		
////	}
//	
//	
//	@Transactional(propagation = Propagation.REQUIRED)
//	private void finalizarInserirProdutoTransferencia(TransferenciaValoresUsuario transferenciaEmAberto, ContaBancaria contaBancariaGatewayDePagamento, Rastreamento rastreamento, Boolean desistencia) {
//		
//		logger.info("finalizarInserirProdutoTransferencia(TransferenciaValoresUsuario transferenciaEmAberto, ContaBancaria contaBancariaGatewayDePagamento, Rastreamento rastreamento, Boolean desistencia)");
//		
//		Boolean inserirProdutoEmTransferencia = Boolean.TRUE;
//		if(transferenciaEmAberto != null) {
//			List<StatusTransferenciaDeValor> listaTransferenciaUsuario = statusTransferenciaDeValorDao.obterStatusTransferenciaPorTransferencia(transferenciaEmAberto);
//			
//			for(StatusTransferenciaDeValor transferenciaUsuario : listaTransferenciaUsuario) {
//				if(transferenciaUsuario.getAtivo() && !transferenciaUsuario.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED)) {
//					inserirProdutoEmTransferencia = Boolean.FALSE;
//				}
//			}
//		}
//		
//		
//		if(transferenciaEmAberto == null) {
//			Calendar dataDeHoje = Calendar.getInstance();
//			transferenciaEmAberto = new TransferenciaValoresUsuario();
//			
//			List<InfosGeraisProdutoUsuario> listaInfosGeraisProdutoUsuarioDto = new ArrayList<InfosGeraisProdutoUsuario>();
//			listaInfosGeraisProdutoUsuarioDto.add(rastreamento.getInfosGeraisProdutoUsuario());
//			
//			transferenciaEmAberto.setListaInfosGeraisProdutoUsuario(listaInfosGeraisProdutoUsuarioDto);
//			
//			if(contaBancariaGatewayDePagamento != null) {
//				transferenciaEmAberto.setContaUsadaNaTransferencia(contaBancariaGatewayDePagamento);
//			}
//			
//			StatusTransferenciaDeValor statusTransferenciaDeValor = new StatusTransferenciaDeValor();
//			statusTransferenciaDeValor.setDataTransferencia(dataDeHoje);
//			statusTransferenciaDeValor.setStatusTransferenciaDeValoresGatewayDePagamento(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED);
//			statusTransferenciaDeValor.setAtivo(Boolean.TRUE);
//			statusTransferenciaDeValor.setTransferenciaValoresUsuario(transferenciaEmAberto);
//			
//			List<StatusTransferenciaDeValor> listaStatus = new ArrayList<StatusTransferenciaDeValor>();
//			listaStatus.add(statusTransferenciaDeValor);
//			
//			transferenciaEmAberto.setListaStatusTransferenciaDeValor(listaStatus);
//			
//			
//			if(desistencia) {
//				transferenciaEmAberto.setUsuario(rastreamento.getComprador());
//			} else {
//				transferenciaEmAberto.setUsuario(rastreamento.getInfosGeraisProdutoUsuario().getUsuario());
//			}
//			
//			
//			transferenciaEmAberto.setDesistencia(desistencia);
//			
////			&& transferenciaEmAberto.getDesistencia().equals(desistencia)
//		} else if(transferenciaEmAberto != null && inserirProdutoEmTransferencia){
//			transferenciaEmAberto.getListaInfosGeraisProdutoUsuario().add(rastreamento.getInfosGeraisProdutoUsuario());
//
//			if(!transferenciaEmAberto.getContaUsadaNaTransferencia().getId().equals(contaBancariaGatewayDePagamento.getId())) {
//				transferenciaEmAberto.setContaUsadaNaTransferencia(contaBancariaGatewayDePagamento);
//			}
//		}
//		
//		transferenciaValoresUsuarioDao.update(transferenciaEmAberto);
//		transferenciaValoresUsuarioDao.flush();
//		
//	}
//
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<TransferenciaValoresUsuarioDTO> obterTransferenciaComArquivosCriados() {
//		
//		logger.info("obterTransferenciaComArquivosCriados()");
//		
//		List<TransferenciaValoresUsuario> listaDeTransferenciaEmAberto = transferenciaValoresUsuarioDao.obterTransferenciaEmAberto();
//		
//		List<TransferenciaValoresUsuarioDTO> listaTransferenciaEmAbertoDto = new ArrayList<TransferenciaValoresUsuarioDTO>();
//		for(TransferenciaValoresUsuario transferenciaEmAberto : listaDeTransferenciaEmAberto) {
//			for(StatusTransferenciaDeValor statusTransferenciaValor : transferenciaEmAberto.getListaStatusTransferenciaDeValor()) {
//				
//				if(statusTransferenciaValor.getAtivo() && statusTransferenciaValor.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.FILE_CREATED)) {
//					TransferenciaValoresUsuarioDTO transferenciaEmAbertoDto = dozerBeanMapper.map(transferenciaEmAberto, TransferenciaValoresUsuarioDTO.class);
//					
////					apenas para passar a dara no padrao brasileiro
//					for(StatusTransferenciaDeValorDTO statusDto : transferenciaEmAbertoDto.getListaStatusTransferenciaDeValor()) {
//						statusDto.setDataTransferenciaString(new DatasTransformacao().calendarToString(statusDto.getDataTransferencia()));
//					}
//					
//					listaTransferenciaEmAbertoDto.add(transferenciaEmAbertoDto);
//				}
//			}
//		}
//		
//		return listaTransferenciaEmAbertoDto;
//	}
//
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public void fecharTransferencia(TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto) {
//		
//		logger.info("fecharTransferencia(TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto)");
//		
//		transferenciaValoresUsuarioDto.setFinalizado(Boolean.TRUE);
//		
//		for (StatusTransferenciaDeValorDTO statusDto : transferenciaValoresUsuarioDto.getListaStatusTransferenciaDeValor()) {
//			statusDto.setAtivo(Boolean.FALSE);
//		}
//		
//		StatusTransferenciaDeValorDTO statusTransferenciaDeValorDto = new StatusTransferenciaDeValorDTO();
//		statusTransferenciaDeValorDto.setDataTransferencia(Calendar.getInstance());
//		statusTransferenciaDeValorDto.setAtivo(Boolean.TRUE);
//		statusTransferenciaDeValorDto.setTransferenciaValoresUsuario(transferenciaValoresUsuarioDto);
//		statusTransferenciaDeValorDto.setStatusTransferenciaDeValoresGatewayDePagamento(StatusTransferenciaDeValoresGatewayDePagamentoEnum.COMPLETED);
//	
//	
//		transferenciaValoresUsuarioDto.getListaStatusTransferenciaDeValor().add(statusTransferenciaDeValorDto);
//		
//		this.atualizarTransferenciaValoresUsuario(transferenciaValoresUsuarioDto);
//	}
//
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<DesistenciaProdutoDTO> obterDesistenciasSemListaDeTransferencias(List<DesistenciaProdutoDTO> listaDeDesistenciasDto) {
//		
//		logger.info("obterDesistenciasSemListaDeTransferencias(List<DesistenciaProdutoDTO> listaDeDesistenciasDto)");
//		
//		ArrayList<DesistenciaProdutoDTO> listaDeDesistenciaRetornoDto = new ArrayList<DesistenciaProdutoDTO>();
//		
//		for (DesistenciaProdutoDTO desistenciaDto : listaDeDesistenciasDto) {
//			TransferenciaValoresUsuarioDTO transferenciaValoresUsuarioDto = this.obterTransferenciaPorProduto(desistenciaDto.getRastreamento().getInfosGeraisProdutoUsuario());
//			if(transferenciaValoresUsuarioDto == null) {
//				listaDeDesistenciaRetornoDto.add(desistenciaDto);
//			} else if(transferenciaValoresUsuarioDto.getDesistencia()){
//				for (StatusTransferenciaDeValorDTO statusTransferenciaDeValor : transferenciaValoresUsuarioDto.getListaStatusTransferenciaDeValor()) {
//					if(statusTransferenciaDeValor.getAtivo() && statusTransferenciaDeValor.getStatusTransferenciaDeValoresGatewayDePagamento().equals(StatusTransferenciaDeValoresGatewayDePagamentoEnum.STARTED)) {
//						listaDeDesistenciaRetornoDto.add(desistenciaDto);
//					}
//				}
//			}	
//		}		
//		return listaDeDesistenciaRetornoDto;
//	}	
}
