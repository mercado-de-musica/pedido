package br.com.mercadodemusica.serviceImpl;

import br.com.mercadodemusica.service.GatewayDePagamentoService;

//@Service
//@EnableOAuth2Client
public class GatewayDePagamentoServiceImpl implements GatewayDePagamentoService {

	/**
	 * 	
	 */
	private static final long serialVersionUID = 640247339874599262L;
	
//	@Autowired
//	private CustoGatewayPagamentoDao custoGatewayPagamentoDao;
//	
//	@Autowired
//	private Mapper dozerBeanMapper;
//
////	@Autowired
////	private ParcelamentoDeValorDao parcelamentoDeValorDao;
//	
//	@Autowired
//	private ContaBancariaDao contaBancariaDao;
//	
//	@Autowired
//	private CompraVendaDao compraVendaDao;
//	
//	@Autowired
//	private CompraCartaoDeCreditoDao compraCartaoDeCreditoDao;
//	
//	@Autowired
//	private CompraBoletoDao compraBoletoDao;
//	
//	@Autowired
//	private EnvioEmails envioEmails;
//	
//	@Autowired
//	private TemplateEmails templateEmails;
//	
//	private final static Logger logger = Logger.getLogger(GatewayDePagamentoServiceImpl.class);
//	
//	@Override
//	public List<BandeiraValoresParcelasGatewayDePagamentoDTO> obterValorDoParcelamento(ParcelamentoEnum quantidadeDeParcelas, InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto, TipoPagamentoEnum tipoPagamentoEnum, InstituicaoEnum instituicaoEnum) throws GatewayDePagamentoException {
//		
//		logger.info("obterValorDoParcelamento(ParcelamentoEnum quantidadeDeParcelas, InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto, TipoPagamentoEnum tipoPagamentoEnum, InstituicaoEnum instituicaoEnum)");
//		
//		/*
//		 * Com correios
//		 */
//		
//		BigDecimal precoProduto = infosGeraisProdutoUsuarioDto.getPreco();
//		
//		if(infosGeraisProdutoUsuarioDto.getCalculoFrete() != null && !infosGeraisProdutoUsuarioDto.getCalculoFrete().isEmpty()) {
//			for(ValorFreteCorreioDTO calculoDeFrete : infosGeraisProdutoUsuarioDto.getCalculoFrete()) {
//				if(calculoDeFrete.getAtivoParaCompra()) {
//					precoProduto = precoProduto.add(calculoDeFrete.getValor());
//				}
//			}
//		}
//		
//		List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaBandeiraValoresParcelasGatewayDePagamentoDto = this.obterParcelamentoPorNumeroDeParcela(precoProduto);
//		
//		if(instituicaoEnum != null) {
//			for (BandeiraValoresParcelasGatewayDePagamentoDTO bandeiraValoresParcelasGatewayDePagamentoDto : listaBandeiraValoresParcelasGatewayDePagamentoDto) {
//				if(bandeiraValoresParcelasGatewayDePagamentoDto.getInstituicao().equals(instituicaoEnum)) {
////					zeramos a lista e colocamos apenas o que nos interessa 
//					listaBandeiraValoresParcelasGatewayDePagamentoDto = new ArrayList<BandeiraValoresParcelasGatewayDePagamentoDTO>();
//					listaBandeiraValoresParcelasGatewayDePagamentoDto.add(bandeiraValoresParcelasGatewayDePagamentoDto);
//				}
//			}
//		}
//		
//		
////		Retirada de parcelas que nao interessam
//		for(BandeiraValoresParcelasGatewayDePagamentoDTO bandeiraValoresParcelasGatewayDePagamentoDto : listaBandeiraValoresParcelasGatewayDePagamentoDto) {
//			ArrayList<ParcelasValoresGatewayDePagamentoDTO> listaParcelasValoresGatewayDePagamentoRetornoDto = new ArrayList<ParcelasValoresGatewayDePagamentoDTO>();
//			for(ParcelasValoresGatewayDePagamentoDTO parcelasValoresGatewayDePagamentoDto : bandeiraValoresParcelasGatewayDePagamentoDto.getListaDeParcelasValoresGatewayDePagamento()) {
//				if(parcelasValoresGatewayDePagamentoDto.getMapVezesValor().containsKey(BigInteger.valueOf(quantidadeDeParcelas.getValor()))) {
//					listaParcelasValoresGatewayDePagamentoRetornoDto.add(parcelasValoresGatewayDePagamentoDto);
//				}
//			}
//			
//			bandeiraValoresParcelasGatewayDePagamentoDto.setListaDeParcelasValoresGatewayDePagamento(listaParcelasValoresGatewayDePagamentoRetornoDto);
//		}
//		
//		return listaBandeiraValoresParcelasGatewayDePagamentoDto;
//	}
//
//
//	@Override
//	public CustoGatewayPagamentoDTO obterPorcentagemGatewayDePagamentoValorAVista() {
//		
//		
//		logger.info("obterPorcentagemGatewayDePagamentoValorAVista()");
//		
//		List<CustoGatewayPagamento> listaDeCustosDePagamentos = custoGatewayPagamentoDao.obterCustoDePagamentos();
//		
//		for(CustoGatewayPagamento custoDePagamento : listaDeCustosDePagamentos) {
//			CustoGatewayPagamentoDTO custoDePagamentoDto = dozerBeanMapper.map(custoDePagamento, CustoGatewayPagamentoDTO.class);
//			
//			if(custoDePagamentoDto.getTipoDePagamento().equals(TipoCartaoEnum.DEBITO)) {
//				return custoDePagamentoDto;
//			}
//		}
//		return null;
//		
//	}
//
//	
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void efetuarPagamentoComCartaoDeCredito(PagamentoCartoesDTO pagamentoCartoesDto, CompraDTO compraDto, List<BandeiraValoresParcelasGatewayDePagamentoDTO> parcelasValoresGatewayDePagamentoDto, HttpServletRequest request) throws GatewayDePagamentoException, MessagingException{
//		
//		logger.info("efetuarPagamentoComCartaoDeCredito(PagamentoCartoesDTO pagamentoCartoesDto, CompraDTO compraDto, List<BandeiraValoresParcelasGatewayDePagamentoDTO> parcelasValoresGatewayDePagamentoDto)");
//		
//		for (BandeiraValoresParcelasGatewayDePagamentoDTO parcelaValoresGatewayDePagamentoDto : parcelasValoresGatewayDePagamentoDto) {
//			
//			for (ParcelasValoresGatewayDePagamentoDTO parcelaDto : parcelaValoresGatewayDePagamentoDto.getListaDeParcelasValoresGatewayDePagamento()) {
//				compraDto.setTotalPago(parcelaDto.getTotalParcelamento());
//			}
//		}
//		
//		
//		
//		Compra compraEntity = dozerBeanMapper.map(compraDto, Compra.class);
//		
//		compraEntity = compraVendaDao.selectOne(compraEntity.getId());
//		
//		compraEntity.setTotalPago(compraDto.getTotalPago());
//		
//		/*
//		 * setado harded code pois o usuário não consegue finalizar a compra caso não aceite a política de compra
//		 */
//		compraEntity.setAceitePoliticaDeCompra(Boolean.TRUE);
//		
//		
//		this.pagamentoTrayCheckout(pagamentoCartoesDto, compraEntity, request);
//	}
//	
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void efetuarPagamentoComBoleto(CompraDTO compraDto, HttpServletRequest request) throws CompraVendaException, EnderecoException, IOException, GatewayDePagamentoException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, MessagingException {
//		
//		logger.info("efetuarPagamentoComBoleto(CompraDTO compraDto)");
//		
//		/*
//		 * Com correios
//		 */
//		
//		BigDecimal totalPago = BigDecimal.ZERO;
//		for (InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuarioDto : compraDto.getProdutosComprados()) {
//			totalPago = totalPago.add(infosGeraisProdutoUsuarioDto.getPreco());
//			
//			if(infosGeraisProdutoUsuarioDto.getCalculoFrete() != null && !infosGeraisProdutoUsuarioDto.getCalculoFrete().isEmpty()) {
//				for(ValorFreteCorreioDTO calculoDeFrete : infosGeraisProdutoUsuarioDto.getCalculoFrete()) {
//					if(calculoDeFrete.getAtivoParaCompra()) {
//						totalPago = totalPago.add(calculoDeFrete.getValor());
//					}
//				}
//			}
//		}
//		
//		
//		compraDto.setTotalPago(totalPago);
//		
//		Compra compraEntity = dozerBeanMapper.map(compraDto, Compra.class);
//		compraEntity = compraVendaDao.selectOne(compraEntity.getId());
//		compraEntity.setTotalPago(compraDto.getTotalPago());	
//		
//		/*
//		 * setado harded code pois o usuário não consegue finalizar a compra caso não aceite a política de compra
//		 */
//		compraEntity.setAceitePoliticaDeCompra(Boolean.TRUE);
//		
//		this.pagamentoTrayCheckout(null, compraEntity, request);
//		
//	}
//	
//
//	@Override
//	@Transactional(readOnly = true)
//	public List<ContaBancariaDTO> obterContasBancariasPorUsuario(UsuarioDTO usuarioDto) {
//		
//		logger.info("obterContasBancariasPorUsuario(UsuarioDTO usuarioDto)");
//		
//		Usuario usuario = dozerBeanMapper.map(usuarioDto, Usuario.class);
//		
//		List<ContaBancaria> listaDeContasDePagamento = contaBancariaDao.obterContasDePagamentoPorUsuario(usuario);
//		List<ContaBancariaDTO> listaRetorno = new ArrayList<ContaBancariaDTO>();
//		
//		for(ContaBancaria contaDePagamento : listaDeContasDePagamento) {
//			ContaBancariaDTO contaDePagamentoDto = dozerBeanMapper.map(contaDePagamento, ContaBancariaDTO.class);	
//			listaRetorno.add(contaDePagamentoDto);
//		}
//		
//		return listaRetorno;
//	}
//
//	@Override
//	@Transactional(propagation=Propagation.REQUIRED)
//	public void cadastrarContaBancaria(ContaBancariaDTO contaBancariaGatewayDePagamentoDto) throws GatewayDePagamentoException, IOException {
//		
//		logger.info("cadastrarContaBancaria(ContaBancariaDTO contaBancariaGatewayDePagamentoDto)");
//		
//		if(contaBancariaGatewayDePagamentoDto.getNumeroDoBanco() == null || contaBancariaGatewayDePagamentoDto.getNumeroDoBanco().equals("") || 
//				contaBancariaGatewayDePagamentoDto.getNumeroDoBanco().equals(BancoEnum._000)) {
//			
//			throw new GatewayDePagamentoException("Para cadastrar uma conta, é necessário o número do banco");
//		}
//		
//		if(contaBancariaGatewayDePagamentoDto.getNumeroDaAgencia() == null || contaBancariaGatewayDePagamentoDto.getNumeroDaAgencia().equals("") || 
//				contaBancariaGatewayDePagamentoDto.getNumeroDaAgencia().length() > 5) {
//			
//			throw new GatewayDePagamentoException("A agência não pode estar vazia ou ter mais que 5 dígitos");
//		}
//		
//		if(contaBancariaGatewayDePagamentoDto.getNumeroDaConta() == null || contaBancariaGatewayDePagamentoDto.getNumeroDaConta().equals("") || 
//				contaBancariaGatewayDePagamentoDto.getNumeroDaConta().length() > 20) {
//			
//			throw new GatewayDePagamentoException("O número da conta não pode estar vazio ou ter mais que 20 dígitos");
//		}
//		
//		if(contaBancariaGatewayDePagamentoDto.getDigitoDaConta() == null || contaBancariaGatewayDePagamentoDto.getDigitoDaConta().equals("") || 
//				contaBancariaGatewayDePagamentoDto.getDigitoDaConta().length() > 2) {
//			
//			throw new GatewayDePagamentoException("O dígito da conta não pode estar vazio ou ter mais que 20 dígitos");
//		}
//		
//		 ContaBancaria contaBancariaGatewayDePagamento = dozerBeanMapper.map(contaBancariaGatewayDePagamentoDto, ContaBancaria.class);
//		 contaBancariaDao.update(contaBancariaGatewayDePagamento);
//		
//	}
//
//	@Override
//	public List<BandeiraValoresParcelasGatewayDePagamentoDTO> parcelamentoTotal(List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaParcelamentoTotalDto, List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaBandeiraValoresParcelasGatewayDePagamentoDto) {
//		
//		logger.info("parcelamentoTotal(List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaParcelamentoTotalDto, List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaBandeiraValoresParcelasGatewayDePagamentoDto)");
//		
//		
//		/*
//		 *	Seguinte: aqui é complicado...
//		 *	se a lista de parcelamento total estiver vazia, retornamos a lista que vem do tray checkout.
//		 *
//		 *  caso ela nao esteja vazia, significa que temos dois produtos no carrinho de compras e devemos somá-los 
//		 *  nos valores retornados a cada 2x, 3x etc que o gateway de pagamento retornar
//		 *  
//		 *  entao eu iterei sobre as duas listas iniciais e caso a bandeira seja a mesma (VISA x VISA), ele envia a lista que existe dentro dele (1x, 2x, 3x) 
//		 *  para que possamos somar os valores
//		 *  
//		 */
//		
//	
//		if(listaParcelamentoTotalDto.isEmpty()) {
//			return listaBandeiraValoresParcelasGatewayDePagamentoDto;
//		} else {
//			
//			ArrayList<BandeiraValoresParcelasGatewayDePagamentoDTO> listaBandeiraValoresParcelasGatewayDePagamentoRetornoDto = new ArrayList<BandeiraValoresParcelasGatewayDePagamentoDTO>();
//			for (BandeiraValoresParcelasGatewayDePagamentoDTO parcelamentoTotalDto : listaParcelamentoTotalDto) {
//				
//				for(BandeiraValoresParcelasGatewayDePagamentoDTO parcelamentoGatewayDePagamentoDto : listaBandeiraValoresParcelasGatewayDePagamentoDto) {
//					
//					
//					if(parcelamentoTotalDto.getInstituicao().equals(parcelamentoGatewayDePagamentoDto.getInstituicao())) {
//						
//						BandeiraValoresParcelasGatewayDePagamentoDTO bandeiraValoresParcelasGatewayDePagamentoRetornoDto = new BandeiraValoresParcelasGatewayDePagamentoDTO();
//						
//						List<ParcelasValoresGatewayDePagamentoDTO> listaRetorno = this.adicionarValoresPrestacoes(parcelamentoTotalDto.getListaDeParcelasValoresGatewayDePagamento(), parcelamentoGatewayDePagamentoDto.getListaDeParcelasValoresGatewayDePagamento());
//					
//						bandeiraValoresParcelasGatewayDePagamentoRetornoDto.setInstituicao(parcelamentoTotalDto.getInstituicao());
//						bandeiraValoresParcelasGatewayDePagamentoRetornoDto.setListaDeParcelasValoresGatewayDePagamento(listaRetorno);
//						
//						
//						listaBandeiraValoresParcelasGatewayDePagamentoRetornoDto.add(bandeiraValoresParcelasGatewayDePagamentoRetornoDto);
//					}
//				}
//			}
//			
//			return listaBandeiraValoresParcelasGatewayDePagamentoRetornoDto;
//		}
//	}
//
//	@Override
//	@Transactional(readOnly = true)
//	public ContaBancariaDTO obterContaBancaria(ContaBancariaDTO contaBancariaGatewayDePagamentoDto) throws GatewayDePagamentoException {
//		
//		logger.info("obterContaBancaria(ContaBancariaDTO contaBancariaGatewayDePagamentoDto)");
//		
//		if(contaBancariaGatewayDePagamentoDto == null || contaBancariaGatewayDePagamentoDto.getId() == null) {
//			throw new GatewayDePagamentoException("Conta bancária é nula.");
//		}
//		
//		ContaBancaria contaBancariaGatewayDePagamento = dozerBeanMapper.map(contaBancariaGatewayDePagamentoDto, ContaBancaria.class);
//		
//		contaBancariaGatewayDePagamento = contaBancariaDao.selectOne(contaBancariaGatewayDePagamento.getId());
//		
//		if(contaBancariaGatewayDePagamento == null) {
//			throw new GatewayDePagamentoException("Conta bancária é nula.");
//		}
//		
//		
//		contaBancariaGatewayDePagamentoDto = dozerBeanMapper.map(contaBancariaGatewayDePagamento, ContaBancariaDTO.class);
//		
//		return contaBancariaGatewayDePagamentoDto;
//	}
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void excluirContaBancaria(ContaBancariaDTO contaBancariaDto) throws GatewayDePagamentoException, JSONException, IOException {
//		
//		logger.info("excluirContaBancaria(ContaBancariaDTO contaBancariaDto)");
//		
//		ContaBancaria contaBancaria = dozerBeanMapper.map(contaBancariaDto, ContaBancaria.class);		
//		
//		contaBancaria.setUsuario(null);
//		contaBancariaDao.delete(contaBancariaDao.contains(contaBancaria) ? contaBancaria : contaBancariaDao.update(contaBancaria));  
//	}
//	
//	
//	@Override
//	public InstituicaoEnum verificarBandeiraCartaoDeCredito(String bandeiraCartao) {
//		
//		logger.info("verificarBandeiraCartaoDeCredito(String bandeiraCartao)");
//		
//		switch (bandeiraCartao) {
//			case "mastercard":
//				return InstituicaoEnum.MASTERCARD;
//			case "visa":
//				return InstituicaoEnum.VISA;
//			case "amex":
//				return InstituicaoEnum.AMEX;
//			case "elo":
//				return InstituicaoEnum.ELO;
//			case "hipercard":
//				return InstituicaoEnum.HIPERCARD;
//			case "hiper":
//				return InstituicaoEnum.HIPER;
//			default:
//				return null;
//		}
//	}
//
//
//	@Override
//	public void verificarBandeiraCartaoDeCreditoNula(InstituicaoEnum instituicao) throws GatewayDePagamentoException {
//		logger.info("verificarBandeiraCartaoDeCreditoNula(InstituicaoEnum instituicao)");
//		
//		if(instituicao == null) {
//			throw new GatewayDePagamentoException("Não existe bandeira deste cartão");
//		}
//		
//	}
//	
//	@Transactional(propagation = Propagation.NESTED)
//	private void pagamentoTrayCheckout(PagamentoCartoesDTO pagamentoCartoesDto, Compra compraEntity, HttpServletRequest request) throws GatewayDePagamentoException, MessagingException {
//		
////		JSONObject jsonCarrinhoDeCompra = this.criarCarrinhoDeComprasTrayCheckout(pagamentoCartoesDto, compraEntity);
//		
//		logger.info("pagamentoTrayCheckout(PagamentoCartoesDTO pagamentoCartoesDto, Compra compraEntity)");
//		
//	    HttpClient client = new HttpClient();
//	    PostMethod method = new PostMethod(TrayCheckoutAutenticacao.PAGAMENTO);
//	    
//	    method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
//	        new DefaultHttpMethodRetryHandler(3, false));
//	    byte[] responseBody = null;
//
//	    try {
//	    	
//	    	List<NameValuePair> dataList = new ArrayList<NameValuePair>();
//	    	dataList.add(new NameValuePair("token_account", TrayCheckoutAutenticacao.TOKEN));
//	    	
//	    	if(compraEntity.getComprador() instanceof UsuarioPessoaJuridica) {
//            	UsuarioPessoaJuridica usuarioPessoaJuridica = (UsuarioPessoaJuridica) compraEntity.getComprador();
//            	
//            	dataList.add(new NameValuePair("customer[name]", usuarioPessoaJuridica.getNomeTitular() + " " + usuarioPessoaJuridica.getSobrenomeTitular()));
//            	dataList.add(new NameValuePair("customer[cpf]", usuarioPessoaJuridica.getCpfTitular()));
//            	
//            	if(usuarioPessoaJuridica.getNomeFantasia() != null) {
//            		dataList.add(new NameValuePair("customer[trade_name]", usuarioPessoaJuridica.getNomeFantasia()));
//            	}
//            	
//            	dataList.add(new NameValuePair("customer[company_name]", usuarioPessoaJuridica.getRazaoSocial()));
//            	dataList.add(new NameValuePair("customer[cnpj]", usuarioPessoaJuridica.getCnpj()));
//            		
//            } else {
//            	UsuarioPessoaFisica usuarioPessoaFisica = (UsuarioPessoaFisica) compraEntity.getComprador();
//            	
//            	dataList.add(new NameValuePair("customer[name]", usuarioPessoaFisica.getNome() + " " + usuarioPessoaFisica.getSobrenome()));
//            	dataList.add(new NameValuePair("customer[cpf]", usuarioPessoaFisica.getCpf()));
//            	dataList.add(new NameValuePair("customer[birth_date]", new DatasTransformacao().calendarToString(usuarioPessoaFisica.getDataDeNacimento())));            	
//            }
//	    	
//	    	
//	    	dataList.add(new NameValuePair("customer[email]", compraEntity.getComprador().getEmail()));
//	    	
//	    	Iterator<Telefone> telefoneIterator = compraEntity.getComprador().getTelefones().iterator();
//	    	
//	    	while (telefoneIterator.hasNext()) {
//	    		Telefone telefone = telefoneIterator.next();
//	    		
//	    		if(telefone.getTipoDeTelefone().equals(TipoDeTelefoneEnum.RESIDENCIA)) {
//	    			dataList.add(new NameValuePair("customer[contacts][][type_contact]", "H"));
//	    		} else if(telefone.getTipoDeTelefone().equals(TipoDeTelefoneEnum.CELULAR)) {
//	    			dataList.add(new NameValuePair("customer[contacts][][type_contact]", "M"));
//	    		} else if(telefone.getTipoDeTelefone().equals(TipoDeTelefoneEnum.TRABALHO)) {
//	    			dataList.add(new NameValuePair("customer[contacts][][type_contact]", "W"));
//	    		}
//	    		dataList.add(new NameValuePair("customer[contacts][][number_contact]", telefone.getDdd() + telefone.getTelefone()));
//	    	}
//	    	
//	    	Iterator<Endereco> enderecoIterator = compraEntity.getComprador().getEnderecos().iterator();
//	    	
//	    	while(enderecoIterator.hasNext()) {
//	    		Endereco endereco = enderecoIterator.next();
//	    		
//	    		if(endereco.getEnderecoDefault()) {
//	    			dataList.add(new NameValuePair("customer[addresses][][type_address]", "D"));
//	    			dataList.add(new NameValuePair("customer[addresses][][postal_code]", endereco.getCep()));
//	    			dataList.add(new NameValuePair("customer[addresses][][street]", endereco.getLogradouro()));
//	    			dataList.add(new NameValuePair("customer[addresses][][number]", endereco.getNumero()));
//	    			dataList.add(new NameValuePair("customer[addresses][][neighborhood]", endereco.getBairro()));
//	    			dataList.add(new NameValuePair("customer[addresses][][city]", endereco.getCidade().getNome()));
//	    			dataList.add(new NameValuePair("customer[addresses][][state]", endereco.getCidade().getEstado().getNome()));
//	    			
//	    			if(endereco.getComplemento() != null) {
//	    				dataList.add(new NameValuePair("customer[addresses][][completion]", endereco.getComplemento()));
//	    			}
//	    		}
//	    	}
//	    	
//	    	dataList.add(new NameValuePair("transaction[available_payment_methods]", "3,4,5,6,16,20,25"));
//	    	
//	    	/*
//	    	 * verificacao de ips do cliente
//	    	 */
//	    	String headerIp = request.getHeader("x-forwarded-for");
//	    	if(headerIp == null) {
//	    		headerIp = request.getHeader("X_FORWARDED_FOR");
//	    	}
//	    	if(headerIp == null) {
//	    		headerIp = request.getRemoteAddr();
//	    	}
//	    	if(headerIp == null) {
//	    		headerIp = "200.200.200.200";
//	    	}
//	    	
//	    	dataList.add(new NameValuePair("transaction[customer_ip]", headerIp));
//	    	
//	    	/*
//	    	 * ajuste porque o Spring Data está triplicando o meu array inicial
//	    	 */
//	    	List<InfosGeraisProdutoUsuario> listaDeProdutosCompradosAjuste = new ArrayList<InfosGeraisProdutoUsuario>();
//	    	for(InfosGeraisProdutoUsuario produtoComprado : compraEntity.getProdutosComprados()) {
//	    		
//	    		Boolean existeNoArray = Boolean.FALSE;
//	    		for(InfosGeraisProdutoUsuario produtoCompradoAjuste : listaDeProdutosCompradosAjuste) {
//	    			if(produtoComprado.getId().equals(produtoCompradoAjuste.getId())) {
//	    				existeNoArray = Boolean.TRUE;
//	    			}
//	    		}
//	    		
//	    		if(! existeNoArray) {
//	    			
//	    			dataList.add(new NameValuePair("transaction_product[][description]", produtoComprado.getInfosGeraisProduto().getInstrumentoAcessorio().getNome() + 
//		    				" " + produtoComprado.getInfosGeraisProduto().getMarca().getNome() + " " + produtoComprado.getInfosGeraisProduto().getProduto().getNome() + 
//		    				" " + produtoComprado.getInfosGeraisProduto().getModelo().getNome()));
//		    		
//		    		dataList.add(new NameValuePair("transaction_product[][quantity]", "1"));
//		    		
//		    		BigDecimal precoDeCompra = produtoComprado.getPreco();
//		    		for(ValorFreteCorreio calculoDeFrete: produtoComprado.getCalculoFrete()) {
//		    			if(calculoDeFrete.getAtivoParaCompra()) {
//		    				precoDeCompra = precoDeCompra.add(calculoDeFrete.getValor());
//		    			}
//		    		}
//		    		
//		    		dataList.add(new NameValuePair("transaction_product[][price_unit]", precoDeCompra.toString()));
//		    		listaDeProdutosCompradosAjuste.add(produtoComprado);
//	    		}	
//	    	}
//	    	
//	    	if(pagamentoCartoesDto != null) {
//	    		
//	    		dataList.add(new NameValuePair("payment[payment_method_id]", pagamentoCartoesDto.getInstituicao().getNumero()));
//		    	dataList.add(new NameValuePair("payment[split]", pagamentoCartoesDto.getParcelas().toString().length() > 1 ? pagamentoCartoesDto.getParcelas().toString() :  ("0" + pagamentoCartoesDto.getParcelas().toString())));
//		    	dataList.add(new NameValuePair("payment[card_name]", pagamentoCartoesDto.getNomeCartao()));
//		    	dataList.add(new NameValuePair("payment[card_number]", pagamentoCartoesDto.getNumber()));
//		    	dataList.add(new NameValuePair("payment[card_expdate_month]", pagamentoCartoesDto.getExpireMonth()));
//		    	dataList.add(new NameValuePair("payment[card_expdate_year]", pagamentoCartoesDto.getExpireYear()));
//		    	dataList.add(new NameValuePair("payment[card_cvv]", pagamentoCartoesDto.getCvv2()));
//	    	
//	    	} else {
//	    		dataList.add(new NameValuePair("payment[payment_method_id]", "6"));
//	    		dataList.add(new NameValuePair("payment[split]", "01"));
//	    	}
//	    	
//	    	
//	    	
//	    	
//	    	
//	    	NameValuePair[] data = dataList.toArray(new NameValuePair[dataList.size()]);
//	        method.setRequestBody(data);
//
//	        // Execute the method.
//	        int statusCode = client.executeMethod(method);
//
//	        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_CREATED) {
//	            throw new GatewayDePagamentoException(method.getStatusText());
//	        }
//	        
//	        responseBody = method.getResponseBody();
//	        
//	    } catch (HttpException e) {
//	    	method.releaseConnection();
//	    	throw new GatewayDePagamentoException(e.getMessage());
//	    } catch (IOException e) {
//	    	method.releaseConnection();
//	    	throw new GatewayDePagamentoException(e.getMessage());
//	    }
//	    
//	    method.releaseConnection();
//	    String xmlResposta = new String(responseBody);
//		
//		
//	    JSONObject jsonRetorno = XML.toJSONObject(xmlResposta);
//	    JSONObject response = jsonRetorno.getJSONObject("response");
//	  
////	    {"response":{"error_response":{"general_errors":{"general_error":{"code":"003039","message":"Vendedor inválido ou não encontrado"},"type":"array"}},"message_response":{"message":"error"}}}
//	    if(response.has("error_response")) {
//	    	JSONObject errorResponse = response.getJSONObject("error_response");
//	    	JSONObject generalErrors = errorResponse.getJSONObject("general_errors");
//	    	JSONObject generalError = generalErrors.getJSONObject("general_error");
//	    	
//	    	throw new GatewayDePagamentoException(generalError.getString("message"));
//	    }
//	    
//	    JSONObject dataResponse = response.getJSONObject("data_response");
//	    JSONObject transaction = dataResponse.getJSONObject("transaction");
//	    JSONObject transactionId = transaction.getJSONObject("transaction_id");
//	    JSONObject statusId = transaction.getJSONObject("status_id");
//	    JSONObject payment = transaction.getJSONObject("payment");
//	    
//	    
////	    {"response":{"data_response":{"transaction":{"token_transaction":"3ee8854cc5f1bf6eb188cab91031a423","customer":{"company_name":"","name":"Erik Scaranello 2","cnpj":"","trade_name":""}}},"message_response":{"message":"success"}}}
//	    
//	    
//	    
//	    compraEntity.setStatusPagamento(StatusPagamentoEnum.Sucesso);
//	    compraEntity.setIdPedido(String.valueOf(transactionId.getInt("content")));
//	    compraEntity.setTokenPedido(transaction.getString("token_transaction"));
//	    compraEntity.setFinalizado(Boolean.TRUE);
//	    
//	    compraEntity.setIntermediadorDePagamento(IntermediadorDePagamentoEnum.TRAY_CHECKOUT);
//	   
//	    if(compraEntity.getDatasDaCompra() != null) {
//	    	for(HistoricoDatasDeCompra dataDaCompra : compraEntity.getDatasDaCompra()) {
//		    	dataDaCompra.setDataAtualizada(Boolean.FALSE);
//		    }
//	    } else {
//	    	compraEntity.setDatasDaCompra(new ArrayList<HistoricoDatasDeCompra>());
//	    }
//	    
//	    HistoricoDatasDeCompra historicoDatasDeCompra = new HistoricoDatasDeCompra();
//	    historicoDatasDeCompra.setCompra(compraEntity);
//	    historicoDatasDeCompra.setDataAtualizada(Boolean.TRUE);
//	    historicoDatasDeCompra.setDataDaCompra(Calendar.getInstance());
//	    
//	    
//	    compraEntity.getDatasDaCompra().add(historicoDatasDeCompra);
//	    
//	    for(StatusTransacaoEnum valoresEnum : StatusTransacaoEnum.values()) {
//	    	if(Integer.parseInt(valoresEnum.getNumero()) == statusId.getInt("content")) {
//	    		compraEntity.setStatusTransacao(valoresEnum);
//	    	}
//	    }
//	    
//	    if(pagamentoCartoesDto != null) {
//	    	compraEntity.setTipoPagamento(TipoPagamentoEnum.CartaoCredito);
//	    	compraEntity.setParcelas(pagamentoCartoesDto.getParcelas());
//		    
//	    	CompraCartaoDeCredito compraCartaoDeCredito = new CompraCartaoDeCredito();
//		    compraCartaoDeCredito.setIdPagamento(String.valueOf(payment.get("tid")));
//		    
//		    compraCartaoDeCredito.setInstituicao(pagamentoCartoesDto.getInstituicao());
//		    
//		    compraCartaoDeCredito.setPrimeirosNumeros(pagamentoCartoesDto.getNumber().substring(0, 6));
//		    compraCartaoDeCredito.setUltimosNumeros(pagamentoCartoesDto.getNumber().substring(pagamentoCartoesDto.getNumber().length() - 4, pagamentoCartoesDto.getNumber().length()));
//		    compraCartaoDeCredito.setCompra(compraEntity);
//		    
////		    /*
////		     * Atualiza todas as tabelas envolvidas
////		     */
//		     
//		    compraCartaoDeCreditoDao.detachEntidade(compraCartaoDeCredito);
//		    compraCartaoDeCredito = compraCartaoDeCreditoDao.update(compraCartaoDeCredito);
//		    
//		    CompraCartaoDeCreditoDTO compraCartaoDeCreditoDto = dozerBeanMapper.map(compraCartaoDeCredito, CompraCartaoDeCreditoDTO.class);
//		    
//		    String retornoTemplate = templateEmails.compraCartaoDeCredito(compraCartaoDeCreditoDto);
//		    envioEmails.enviarEmailDeAvisoDeCompraCartaoDeCredito(compraCartaoDeCreditoDto, retornoTemplate);
//	    	
//	    } else {
//	    	/*
//	    	 * boleto
//	    	 */
//	    	compraEntity.setTipoPagamento(TipoPagamentoEnum.BoletoBancario);
//	    	compraEntity.setParcelas(1);
//	    	
////	    	{"response":{"data_response":{"transaction":{"transaction_id":{"type":"integer","content":63384},"status_id":{"type":"integer","content":4},"status_name":"Aguardando Pagamento","order_number":63384,"payment":{"linha_digitavel":"123123123123123131232131232131313211231321321","payment_method_name":"Boleto Bancario","price_payment":{"type":"decimal","content":200},"tid":{"nil":true}},"free":{"nil":true},"token_transaction":"02244f0119ff39973842d331a3e884c7","customer":{"company_name":"","name":"Erik Scaranello 2","cnpj":"","trade_name":""}}},"message_response":{"message":"success"}}}
//	    	CompraBoleto compraBoleto = new CompraBoleto();
//		    compraBoleto.setIdPagamento(String.valueOf(payment.getString("linha_digitavel")));
//		    compraBoleto.setUrlBoleto(payment.getString("url_payment"));
//		    compraBoleto.setVencimentoBoleto(VencimentoBoleto.getDataVencimentoBoleto());
//		    
//		    compraBoleto.setCompra(compraEntity);
//		    
//		    /*
//		     * Atualiza todas as tabelas envolvidas
//		     */
//		    compraBoletoDao.detachEntidade(compraBoleto);
//		    compraBoleto = compraBoletoDao.update(compraBoleto);
//		    
//		    CompraBoletoDTO compraBoletoDto = dozerBeanMapper.map(compraBoleto, CompraBoletoDTO.class);
//		    
//		    String retornoTemplate = templateEmails.compraBoleto(compraBoletoDto);
//		    envioEmails.enviarEmailDeAvisoDeCompraBoleto(compraBoletoDto, retornoTemplate);
//	    }
//	   
//	}
//
////	private JSONObject criarCarrinhoDeComprasTrayCheckout(PagamentoCartoesDTO pagamentoCartoesDto, Compra compraEntity) throws GatewayDePagamentoException {
////		
////		 
////	    HttpClient client = new HttpClient();
////	    PostMethod method = new PostMethod(TrayCheckoutAutenticacao.CARRINHO_DE_COMPRAS);
////	    
////	    method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
////	        new DefaultHttpMethodRetryHandler(3, false));
////	    
////	    byte[] responseBody = null;
////
////	    try {
////	        NameValuePair[] data = {
////	            /* Token da Conta do Lojista */
////	            new NameValuePair("token_account", TrayCheckoutAutenticacao.TOKEN),
////	        
////	            /* Dados do Produto 1 - Notebook Preto */
////	            new NameValuePair("transaction_product[][code]", compraEntity.getId().toString()),
////	            new NameValuePair("transaction_product[][description]", "Compra de id " + compraEntity.getId().toString() + " feita no dia " + new DatasTransformacao().calendarToString(Calendar.getInstance())),
////	            new NameValuePair("transaction_product[][quantity]", "1"),
////	            new NameValuePair("transaction_product[][price_unit]", compraEntity.getTotalPago().toString()),
////	            
////	            
////	        };
////
////	        method.setRequestBody(data);
////
////	        // Execute the method.
////	        int statusCode = client.executeMethod(method);
////
////	        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_CREATED) {
////	        	throw new GatewayDePagamentoException(method.getStatusText());
////	        } else {
////	        	responseBody = method.getResponseBody();
////	        }
////	        
////	        
////	    } catch (HttpException e) {
////	    	method.releaseConnection();
////	    	throw new GatewayDePagamentoException(e.getMessage());
////	    } catch (IOException e) {
////	    	method.releaseConnection();
////	    	throw new GatewayDePagamentoException(e.getMessage());
////	    }
////
////	    method.releaseConnection();
////	    String xmlResposta = new String(responseBody);
////	    
////	    JSONObject jsonRetorno = XML.toJSONObject(xmlResposta);
////	    
//////	    {"tmp_transaction":{"error_response":{"general_errors":{"general_error":{"code":"001001","message":"Token inválido ou não encontrado"},"type":"array"}},"message_response":{"message":"error"}}}
////	    
////	    JSONObject tmpTransaction = jsonRetorno.getJSONObject("tmp_transaction");
////	    
////	    if(tmpTransaction.has("error_response")) {
////	    	JSONObject errorResponse = tmpTransaction.getJSONObject("error_response");
////	    	JSONObject generalErrors = errorResponse.getJSONObject("general_errors");
////	    	JSONObject generalError = generalErrors.getJSONObject("general_error");
////	    	
////	    	throw new GatewayDePagamentoException(generalError.getString("message"));
////	    }
////	    
////	    return jsonRetorno;
////	}
//	
//	
//	private List<BandeiraValoresParcelasGatewayDePagamentoDTO> obterParcelamentoPorNumeroDeParcela(BigDecimal precoCompra) throws GatewayDePagamentoException {
//		 
//		logger.info("obterParcelamentoPorNumeroDeParcela(BigDecimal precoCompra)");
//		
//	    HttpClient client = new HttpClient();
//	    PostMethod method = new PostMethod(TrayCheckoutAutenticacao.PARCELAMENTO);
//	    
//	    method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
//	        new DefaultHttpMethodRetryHandler(3, false));
//	    byte[] responseBody = null;
//
//	    try {
//	        NameValuePair[] data = {
//	            /* Dados para Simulação de Parcelamentos */
//	            new NameValuePair("token_account", TrayCheckoutAutenticacao.TOKEN),
//	            new NameValuePair("price", precoCompra.toString())
//	        };
//
//	        method.setRequestBody(data);
//
//	        // Execute the method.
//	        int statusCode = client.executeMethod(method);
//
//	        if (statusCode != HttpStatus.SC_OK) {
//	        	throw new GatewayDePagamentoException(method.getStatusText());
//	        } else {
//	        	responseBody = method.getResponseBody();
//	        }
//	        
//	        responseBody = method.getResponseBody();
//	        
//	    } catch (HttpException e) {
//	    	method.releaseConnection();
//	    	throw new GatewayDePagamentoException(e.getMessage());
//	    } catch (IOException e) {
//	    	method.releaseConnection();
//	    	throw new GatewayDePagamentoException(e.getMessage());
//	    }
//	    
//	    method.releaseConnection();
//	    String xmlResposta = new String(responseBody);
//	    
//	    JSONObject jsonRetorno = XML.toJSONObject(xmlResposta);
//	    JSONObject transaction = jsonRetorno.getJSONObject("transaction");
//	    
//	    if(transaction.has("error_response")) {
//	    	JSONObject errorResponse = transaction.getJSONObject("error_response");
//	    	JSONObject errors = errorResponse.getJSONObject("errors");
//	    	JSONObject error = errors.getJSONObject("error");
//	    	
//	    	throw new GatewayDePagamentoException(error.getString("message"));
//	    }
//	    
//	    
//	    JSONObject dataResponse = transaction.getJSONObject("data_response");
//	    JSONObject paymentMethods = dataResponse.getJSONObject("payment_methods");
//	    JSONArray paymentMethod = paymentMethods.getJSONArray("payment_method");
////	    {"payment_method_id":{"type":"integer","content":3},
////	    ["split":{"type":"integer","content":12},"price_original":"7000.00","price_seller":"6552.30","splittings":{"splitting":[{"split":{"type":"integer","content":1},"split_rate":{"type":"integer","content":0},"value_split":"7000.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":2},"split_rate":{"type":"decimal","content":1.99},"value_split":3604.82,"value_transaction":7209.64,"addition_retention":"0.00"},{"split":{"type":"integer","content":3},"split_rate":{"type":"decimal","content":1.99},"value_split":2426.81,"value_transaction":7280.43,"addition_retention":"0.00"},{"split":{"type":"integer","content":4},"split_rate":{"type":"decimal","content":2.99},"value_split":1882.74,"value_transaction":7530.96,"addition_retention":"0.00"},{"split":{"type":"integer","content":5},"split_rate":{"type":"decimal","content":2.99},"value_split":1528.05,"value_transaction":7640.25,"addition_retention":"0.00"},{"split":{"type":"integer","content":6},"split_rate":{"type":"decimal","content":2.99},"value_split":1291.75,"value_transaction":"7750.50","addition_retention":"0.00"},{"split":{"type":"integer","content":7},"split_rate":{"type":"decimal","content":2.99},"value_split":1123.12,"value_transaction":7861.84,"addition_retention":"0.00"},{"split":{"type":"integer","content":8},"split_rate":{"type":"decimal","content":3.99},"value_split":1039.27,"value_transaction":8314.16,"addition_retention":"0.00"},{"split":{"type":"integer","content":9},"split_rate":{"type":"decimal","content":3.99},"value_split":941.02,"value_transaction":8469.18,"addition_retention":"0.00"},{"split":{"type":"integer","content":10},"split_rate":{"type":"decimal","content":3.99},"value_split":862.61,"value_transaction":"8626.10","addition_retention":"0.00"},{"split":{"type":"integer","content":11},"split_rate":{"type":"decimal","content":3.99},"value_split":798.61,"value_transaction":8784.71,"addition_retention":"0.00"},{"split":{"type":"integer","content":12},"split_rate":{"type":"decimal","content":3.99},"value_split":745.43,"value_transaction":8945.16,"addition_retention":"0.00"}],"type":"array"},"price_customer":"7000.00","payment_method_name":"Visa"},{"payment_method_id":{"type":"integer","content":4},"split":{"type":"integer","content":12},"price_original":"7000.00","price_seller":3582.12,"splittings":{"splitting":[{"split":{"type":"integer","content":1},"split_rate":{"type":"integer","content":0},"value_split":"7000.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":2},"split_rate":{"type":"integer","content":0},"value_split":"3500.00","value_transaction":"7000.00","addition_retention":209.64},{"split":{"type":"integer","content":3},"split_rate":{"type":"integer","content":0},"value_split":2333.33,"value_transaction":"7000.00","addition_retention":280.43},{"split":{"type":"integer","content":4},"split_rate":{"type":"integer","content":0},"value_split":"1750.00","value_transaction":"7000.00","addition_retention":351.68},{"split":{"type":"integer","content":5},"split_rate":{"type":"integer","content":0},"value_split":"1400.00","value_transaction":"7000.00","addition_retention":"423.40"},{"split":{"type":"integer","content":6},"split_rate":{"type":"integer","content":0},"value_split":1166.67,"value_transaction":"7000.00","addition_retention":495.56},{"split":{"type":"integer","content":7},"split_rate":{"type":"integer","content":0},"value_split":"1000.00","value_transaction":"7000.00","addition_retention":568.19},{"split":{"type":"integer","content":8},"split_rate":{"type":"integer","content":0},"value_split":"875.00","value_transaction":"7000.00","addition_retention":641.28},{"split":{"type":"integer","content":9},"split_rate":{"type":"decimal","content":1.99},"value_split":"857.20","value_transaction":"7714.80","addition_retention":"0.00"},{"split":{"type":"integer","content":10},"split_rate":{"type":"decimal","content":1.99},"value_split":778.88,"value_transaction":"7788.80","addition_retention":"0.00"},{"split":{"type":"integer","content":11},"split_rate":{"type":"decimal","content":1.99},"value_split":714.84,"value_transaction":7863.24,"addition_retention":"0.00"},{"split":{"type":"integer","content":12},"split_rate":{"type":"decimal","content":1.99},"value_split":661.51,"value_transaction":7938.12,"addition_retention":"0.00"}],"type":"array"},"price_customer":"7000.00","payment_method_name":"Mastercard"},{"payment_method_id":{"type":"integer","content":2},"split":{"type":"integer","content":12},"price_original":"7000.00","price_seller":5342.83,"splittings":{"splitting":[{"split":{"type":"integer","content":1},"split_rate":{"type":"integer","content":0},"value_split":"7000.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":2},"split_rate":{"type":"integer","content":0},"value_split":"3500.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":3},"split_rate":{"type":"integer","content":0},"value_split":2333.33,"value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":4},"split_rate":{"type":"integer","content":0},"value_split":"1750.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":5},"split_rate":{"type":"integer","content":0},"value_split":"1400.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":6},"split_rate":{"type":"integer","content":0},"value_split":1166.67,"value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":7},"split_rate":{"type":"integer","content":0},"value_split":"1000.00","value_transaction":"7000.00","addition_retention":568.19},{"split":{"type":"integer","content":8},"split_rate":{"type":"integer","content":0},"value_split":"875.00","value_transaction":"7000.00","addition_retention":641.28},{"split":{"type":"integer","content":9},"split_rate":{"type":"decimal","content":1.99},"value_split":"857.20","value_transaction":"7714.80","addition_retention":"0.00"},{"split":{"type":"integer","content":10},"split_rate":{"type":"decimal","content":1.99},"value_split":778.88,"value_transaction":"7788.80","addition_retention":"0.00"},{"split":{"type":"integer","content":11},"split_rate":{"type":"decimal","content":1.99},"value_split":714.84,"value_transaction":7863.24,"addition_retention":"0.00"},{"split":{"type":"integer","content":12},"split_rate":{"type":"decimal","content":1.99},"value_split":661.51,"value_transaction":7938.12,"addition_retention":"0.00"}],"type":"array"},"price_customer":"7000.00","payment_method_name":"Diners"},{"payment_method_id":{"type":"integer","content":5},"split":{"type":"integer","content":12},"price_original":"7000.00","price_seller":5342.83,"splittings":{"splitting":[{"split":{"type":"integer","content":1},"split_rate":{"type":"integer","content":0},"value_split":"7000.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":2},"split_rate":{"type":"integer","content":0},"value_split":"3500.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":3},"split_rate":{"type":"integer","content":0},"value_split":2333.33,"value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":4},"split_rate":{"type":"integer","content":0},"value_split":"1750.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":5},"split_rate":{"type":"integer","content":0},"value_split":"1400.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":6},"split_rate":{"type":"integer","content":0},"value_split":1166.67,"value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":7},"split_rate":{"type":"integer","content":0},"value_split":"1000.00","value_transaction":"7000.00","addition_retention":568.19},{"split":{"type":"integer","content":8},"split_rate":{"type":"integer","content":0},"value_split":"875.00","value_transaction":"7000.00","addition_retention":641.28},{"split":{"type":"integer","content":9},"split_rate":{"type":"decimal","content":1.99},"value_split":"857.20","value_transaction":"7714.80","addition_retention":"0.00"},{"split":{"type":"integer","content":10},"split_rate":{"type":"decimal","content":1.99},"value_split":778.88,"value_transaction":"7788.80","addition_retention":"0.00"},{"split":{"type":"integer","content":11},"split_rate":{"type":"decimal","content":1.99},"value_split":714.84,"value_transaction":7863.24,"addition_retention":"0.00"},{"split":{"type":"integer","content":12},"split_rate":{"type":"decimal","content":1.99},"value_split":661.51,"value_transaction":7938.12,"addition_retention":"0.00"}],"type":"array"},"price_customer":"7000.00","payment_method_name":"American Express"},{"payment_method_id":{"type":"integer","content":15},"split":{"type":"integer","content":1},"price_original":"7000.00","price_seller":"6524.70","splittings":{"splitting":{"split":{"type":"integer","content":1},"split_rate":{"type":"integer","content":0},"value_split":"7000.00","value_transaction":"7000.00","addition_retention":"0.00"},"type":"array"},"price_customer":"7000.00","payment_method_name":"Discover"},{"payment_method_id":{"type":"integer","content":16},"split":{"type":"integer","content":10},"price_original":"7000.00","price_seller":5342.83,"splittings":{"splitting":[{"split":{"type":"integer","content":1},"split_rate":{"type":"integer","content":0},"value_split":"7000.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":2},"split_rate":{"type":"integer","content":0},"value_split":"3500.00","value_transaction":"7000.00","addition_retention":"0.00"},{"split":{"type":"integer","content":3},"split_rate":{"type":"integer","content":0}
//	    
//	    
//	   List<BandeiraValoresParcelasGatewayDePagamentoDTO> listaBandeiraValoresParcelasGatewayDePagamentoDto = new ArrayList<BandeiraValoresParcelasGatewayDePagamentoDTO>();
//	    
//	    for(int i = 0; i < paymentMethod.length(); i++) {
//	    	BandeiraValoresParcelasGatewayDePagamentoDTO bandeiraValoresParcelasGatewayDePagamento = new BandeiraValoresParcelasGatewayDePagamentoDTO();
//	    	List<ParcelasValoresGatewayDePagamentoDTO> listaDeParcelasValoresGatewayDePagamentoDto = new ArrayList<ParcelasValoresGatewayDePagamentoDTO>();
//	    	
//	    	
//	    	JSONObject paymentMethodObject = paymentMethod.getJSONObject(i);
//	    	JSONObject paymentMethodId = paymentMethodObject.getJSONObject("payment_method_id");
//	    	
////	    	colocando a bandeira do cartao
//	    	for(InstituicaoEnum instituicaoEnum : InstituicaoEnum.values()) {
//	    		if(paymentMethodId.getInt("content") == Integer.parseInt(instituicaoEnum.getNumero())) {
//	    			bandeiraValoresParcelasGatewayDePagamento.setInstituicao(instituicaoEnum);
//		    	}
//	    	}
//	    	
//	    	if(bandeiraValoresParcelasGatewayDePagamento.getInstituicao() != null) {
//	    		JSONObject splittings = paymentMethodObject.getJSONObject("splittings");
//		    	JSONArray splittingArray = splittings.getJSONArray("splitting");
//		    	
//		    	for(int j = 0; j < splittingArray.length(); j++) {
//		    		ParcelasValoresGatewayDePagamentoDTO parcelasValoresGatewayDePagamentoDto = new ParcelasValoresGatewayDePagamentoDTO();	    		
//		    		JSONObject splittingObject = splittingArray.getJSONObject(j);
//		    		
//		    		Integer vezes = splittingObject.getJSONObject("split").getInt("content");
////		    		quando é em 1 vez apenas, o tray checkout traz como o vezes
//		    		if(vezes == 0) {
//		    			vezes = 1;
//		    		}
//		    		
//		    		
//		    		Double valorParcela = splittingObject.getDouble("value_split");
//		    		Double taxa = null;
//		    		if(splittingObject.has("split_rate")) {
//		    			taxa = splittingObject.getJSONObject("split_rate").getDouble("content");
//		    		} else {
//		    			taxa = Double.valueOf(0);
//		    		}
//		    		
//		    		Double valorTotal = splittingObject.getDouble("value_transaction");
//		    		
//		    		Map<BigInteger, BigDecimal> mapValores = new HashMap<BigInteger, BigDecimal>();
//		    		Map<BigInteger, String> mapValoresString = new HashMap<BigInteger, String>();
//		    		
////		    		vezes e parcela
//		    		BigDecimal valorParcelaDecimal = new BigDecimal(valorParcela, MathContext.DECIMAL64).setScale(2, BigDecimal.ROUND_HALF_EVEN);
//		    		mapValores.put(BigInteger.valueOf(vezes), valorParcelaDecimal);
//		    		mapValoresString.put(BigInteger.valueOf(vezes), new Precos().padraoAmericanoParaBrasileiro(valorParcelaDecimal));
//		    		
//		    		parcelasValoresGatewayDePagamentoDto.setMapVezesValor(mapValores);
//		    		parcelasValoresGatewayDePagamentoDto.setMapVezesValorString(mapValoresString);
//		    		
////		    		taxa
//		    		BigDecimal taxaDecimal = new BigDecimal(taxa, MathContext.DECIMAL64).setScale(2, BigDecimal.ROUND_HALF_EVEN);
//		    		parcelasValoresGatewayDePagamentoDto.setTaxa(taxaDecimal);
//		    		parcelasValoresGatewayDePagamentoDto.setTaxaString(new Precos().padraoAmericanoParaBrasileiro(taxaDecimal));
//		    		
////		    		valor total
//		    		BigDecimal valorTotalDecimal = new BigDecimal(valorTotal, MathContext.DECIMAL64).setScale(2, BigDecimal.ROUND_HALF_EVEN);
//		    		parcelasValoresGatewayDePagamentoDto.setTotalParcelamento(valorTotalDecimal);
//		    		parcelasValoresGatewayDePagamentoDto.setTotalParcelamentoString(new Precos().padraoAmericanoParaBrasileiro(valorTotalDecimal));
//		    		
////		    		insercao na lista com a bandeira
//		    		listaDeParcelasValoresGatewayDePagamentoDto.add(parcelasValoresGatewayDePagamentoDto);
//		    	}
//		    	
//		    	
//		    	bandeiraValoresParcelasGatewayDePagamento.setListaDeParcelasValoresGatewayDePagamento(listaDeParcelasValoresGatewayDePagamentoDto);
//		    	listaBandeiraValoresParcelasGatewayDePagamentoDto.add(bandeiraValoresParcelasGatewayDePagamento);
//	    	}
//	    }
//	    
//	    return listaBandeiraValoresParcelasGatewayDePagamentoDto;
//	}
//	
//	private List<ParcelasValoresGatewayDePagamentoDTO> adicionarValoresPrestacoes(List<ParcelasValoresGatewayDePagamentoDTO> listaDeParcelasParcelamentoTotalDto, List<ParcelasValoresGatewayDePagamentoDTO> listaDeParcelasGatewayDePagamentoDto) {
//		
//		logger.info("adicionarValoresPrestacoes(List<ParcelasValoresGatewayDePagamentoDTO> listaDeParcelasParcelamentoTotalDto, List<ParcelasValoresGatewayDePagamentoDTO> listaDeParcelasGatewayDePagamentoDto)");
//		
//		List<ParcelasValoresGatewayDePagamentoDTO> listaRetornoDto = new ArrayList<ParcelasValoresGatewayDePagamentoDTO>();
//		
//		ParcelasValoresGatewayDePagamentoDTO parcelaDeRetornoSomadoDto = new ParcelasValoresGatewayDePagamentoDTO();
//		for (ParcelasValoresGatewayDePagamentoDTO parcelasParcelamentoTotalDto : listaDeParcelasParcelamentoTotalDto) {
//			for (ParcelasValoresGatewayDePagamentoDTO parcelasGatewayDePagamentoDto : listaDeParcelasGatewayDePagamentoDto) {
//				
//				Iterator<BigInteger> iteratorParcelamentoTotalDto = parcelasParcelamentoTotalDto.getMapVezesValor().keySet().iterator();
//				
//				while(iteratorParcelamentoTotalDto.hasNext()) {
//					BigInteger quantidadePrestacaoParcelamentoTotalDto = iteratorParcelamentoTotalDto.next();
//				
//					Iterator<BigInteger> iteratorGatewayDePagamentoDto = parcelasGatewayDePagamentoDto.getMapVezesValor().keySet().iterator();
//					
//					while(iteratorGatewayDePagamentoDto.hasNext()) {
//						BigInteger quantidadePrestacaoGatewayDePagamentoDto = iteratorGatewayDePagamentoDto.next();
//						
//						if(quantidadePrestacaoParcelamentoTotalDto.equals(quantidadePrestacaoGatewayDePagamentoDto)) {
//							Precos precos = new Precos();
//							
//							parcelaDeRetornoSomadoDto.setTaxa(parcelasParcelamentoTotalDto.getTaxa());
//							parcelaDeRetornoSomadoDto.setTaxaString(precos.padraoAmericanoParaBrasileiro(parcelasParcelamentoTotalDto.getTaxa()));
//							
//							parcelaDeRetornoSomadoDto.setTotalParcelamento(parcelasParcelamentoTotalDto.getTotalParcelamento().add(parcelasGatewayDePagamentoDto.getTotalParcelamento()));
//							parcelaDeRetornoSomadoDto.setTotalParcelamentoString(precos.padraoAmericanoParaBrasileiro(parcelaDeRetornoSomadoDto.getTotalParcelamento()));
//							
//							
//							HashMap<BigInteger, BigDecimal> vezesValorNovo = new HashMap<BigInteger, BigDecimal>();
//							BigDecimal totalParcelamentoDecimal = parcelasParcelamentoTotalDto.getMapVezesValor().get(quantidadePrestacaoParcelamentoTotalDto).add(parcelasGatewayDePagamentoDto.getMapVezesValor().get(quantidadePrestacaoParcelamentoTotalDto));
//							
//							vezesValorNovo.put(quantidadePrestacaoParcelamentoTotalDto, totalParcelamentoDecimal);
//							
//							HashMap<BigInteger, String> vezesValorNovoString = new HashMap<BigInteger, String>();
//							vezesValorNovoString.put(quantidadePrestacaoParcelamentoTotalDto, precos.padraoAmericanoParaBrasileiro(totalParcelamentoDecimal));
//							
//							parcelaDeRetornoSomadoDto.setMapVezesValor(vezesValorNovo);
//							parcelaDeRetornoSomadoDto.setMapVezesValorString(vezesValorNovoString);
//							
//							listaRetornoDto.add(parcelaDeRetornoSomadoDto);
//						}
//					}			
//				}
//			}
//		}
//		
//		return listaRetornoDto;
//		
//	}
	
}
