package br.com.mercadodemusica.serviceImpl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mercadodemusica.dao.PorcentagemMercadoDeMusicaDao;
import br.com.mercadodemusica.dto.PorcentagemMercadoDeMusicaDTO;
import br.com.mercadodemusica.entities.PorcentagemMercadoDeMusica;
import br.com.mercadodemusica.service.PorcentagemService;
import br.com.mercadodemusica.transformacaoDeDados.Precos;

@Service
public class PorcentagemServiceImpl implements PorcentagemService {

	@Autowired
	private PorcentagemMercadoDeMusicaDao porcentagemMercadoDeMusicaDao;
	
	@Autowired
	private Mapper dozerBeanMapper;
	
//	@Override
//	@Transactional(readOnly = true)
//	public List<PorcentagemMercadoDeMusicaDTO> obterListaDePorcentagens() {
//		
//		logger.info("obterListaDePorcentagens()");
//		
//		List<PorcentagemMercadoDeMusica> listaDePorcentagens = porcentagemMercadoDeMusicaDao.selectAll();
//		
//		List<PorcentagemMercadoDeMusicaDTO> listaPorcentagensRetornoDto = new ArrayList<PorcentagemMercadoDeMusicaDTO>();
//		
//		for(PorcentagemMercadoDeMusica porcentagem : listaDePorcentagens){
//			PorcentagemMercadoDeMusicaDTO porcentagemDto = dozerBeanMapper.map(porcentagem, PorcentagemMercadoDeMusicaDTO.class);
//			porcentagemDto.setPorcentagemString(new Precos().padraoAmericanoParaBrasileiro(porcentagemDto.getPorcentagem()));
//			listaPorcentagensRetornoDto.add(porcentagemDto);
//		}
//		
//		return listaPorcentagensRetornoDto;
//	}
//
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void cadastroDeNovaPorcentagem(PorcentagemMercadoDeMusicaDTO porcentagemMercadoDeMusicaDto) {
//		
//		logger.info("cadastroDeNovaPorcentagem(PorcentagemMercadoDeMusicaDTO porcentagemMercadoDeMusicaDto)");
//		
//		if(porcentagemMercadoDeMusicaDto.getId() != null && porcentagemMercadoDeMusicaDto.getId().equals(BigInteger.valueOf(-1))) {
//			porcentagemMercadoDeMusicaDto.setId(null);
//		}
//		
//		Calendar hoje = Calendar.getInstance();
//		if(porcentagemMercadoDeMusicaDto.getDataDeMudancaNoSistemaString() != null && !porcentagemMercadoDeMusicaDto.getDataDeMudancaNoSistemaString().equals("")) {
//			porcentagemMercadoDeMusicaDto.setDataDeMudancaNoSistema(new DatasTransformacao().stringToCalendar(porcentagemMercadoDeMusicaDto.getDataDeMudancaNoSistemaString()));
//
//			if(porcentagemMercadoDeMusicaDto.getDataDeMudancaNoSistema().compareTo(hoje) == -1) {
//				porcentagemMercadoDeMusicaDto.setAtivo(Boolean.TRUE);
//				porcentagemMercadoDeMusicaDto.setDataDeMudancaNoSistema(hoje);
//			} else {
//				porcentagemMercadoDeMusicaDto.setAtivo(Boolean.FALSE);
//			}
//		} else {
//			porcentagemMercadoDeMusicaDto.setAtivo(Boolean.TRUE);
//			porcentagemMercadoDeMusicaDto.setDataDeMudancaNoSistema(hoje);
//		}
//		
//		if (porcentagemMercadoDeMusicaDto.getAtivo()) {
//			List<PorcentagemMercadoDeMusica> listaDePorcentagens = porcentagemMercadoDeMusicaDao.selectAll();
//			for(PorcentagemMercadoDeMusica porcentagem : listaDePorcentagens) {
//				if(porcentagem.getAtivo()) {
//					porcentagem.setAtivo(Boolean.FALSE);
//					porcentagemMercadoDeMusicaDao.update(porcentagem);
//				}
//			}
//		}
//		
//		porcentagemMercadoDeMusicaDto.setPorcentagem(new BigDecimal(StringUtils.replace(StringUtils.replace(porcentagemMercadoDeMusicaDto.getPorcentagemString(), ".", ""), ",", ".")));
//		
//		porcentagemMercadoDeMusicaDto.setDataDeInsercao(hoje);
//		
//		PorcentagemMercadoDeMusica porcentagemMercadoDeMusica = dozerBeanMapper.map(porcentagemMercadoDeMusicaDto, PorcentagemMercadoDeMusica.class);
//		porcentagemMercadoDeMusicaDao.update(porcentagemMercadoDeMusica);
//		porcentagemMercadoDeMusicaDao.flush();
//	}
//
//
//	@Override
//	@Transactional(readOnly = true)
//	public PorcentagemMercadoDeMusicaDTO obterPorcentagem(PorcentagemMercadoDeMusicaDTO porcentagemMercadoDeMusicaDto) {
//
//		logger.info("obterPorcentagem(PorcentagemMercadoDeMusicaDTO porcentagemMercadoDeMusicaDto)");
//		
//		PorcentagemMercadoDeMusica porcentagemMercadoDeMusica = dozerBeanMapper.map(porcentagemMercadoDeMusicaDto, PorcentagemMercadoDeMusica.class);
//		porcentagemMercadoDeMusica = porcentagemMercadoDeMusicaDao.selectOne(porcentagemMercadoDeMusica.getId());
//		return dozerBeanMapper.map(porcentagemMercadoDeMusica, PorcentagemMercadoDeMusicaDTO.class);
//	
//	}
//
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void excluirPorcentagem(PorcentagemMercadoDeMusicaDTO porcentagemMercadoDeMusicaDto) throws RegraDeNegocioException {
//		
//		logger.info("excluirPorcentagem(PorcentagemMercadoDeMusicaDTO porcentagemMercadoDeMusicaDto)");
//		
//		List<PorcentagemMercadoDeMusica> listaDePorcentagens = porcentagemMercadoDeMusicaDao.selectAll();
//		
//		if(listaDePorcentagens.size() == 1) {
//			throw new RegraDeNegocioException("É necessário haver ao menos 1 porcentagem no sistema");
//		}
//		
//		PorcentagemMercadoDeMusica porcentagemMercadoDeMusica = dozerBeanMapper.map(porcentagemMercadoDeMusicaDto, PorcentagemMercadoDeMusica.class);
//		porcentagemMercadoDeMusicaDao.delete( porcentagemMercadoDeMusicaDao.contains(porcentagemMercadoDeMusica) ? porcentagemMercadoDeMusica : porcentagemMercadoDeMusicaDao.update(porcentagemMercadoDeMusica));
//	
//	}
//
//
//	@Override
//	@Transactional(propagation = Propagation.REQUIRED)
//	public void ajusteDePorcentagensParaAtivo() {
//		
//		logger.info("ajusteDePorcentagensParaAtivo()");
//		
//		Calendar dataDeHoje = Calendar.getInstance();
//		List<PorcentagemMercadoDeMusica> listaDePorcentagens = porcentagemMercadoDeMusicaDao.selectAll();
//		
//		for(PorcentagemMercadoDeMusica porcentagem : listaDePorcentagens) {
//			porcentagem.setAtivo(Boolean.FALSE);
//			porcentagemMercadoDeMusicaDao.update(porcentagem);
//			
//		}
//		
//		Map<Integer, PorcentagemMercadoDeMusica> treeMapComparacao = new TreeMap<Integer, PorcentagemMercadoDeMusica>();
//		for(PorcentagemMercadoDeMusica porcentagem : listaDePorcentagens) {
//			if(porcentagem.getDataDeMudancaNoSistema().compareTo(dataDeHoje) == -1) {
//				
//				porcentagem.setAtivo(Boolean.TRUE);
//				porcentagemMercadoDeMusicaDao.update(porcentagem);
//				porcentagemMercadoDeMusicaDao.flush();
//				return;
//			
//			} else {
//				int dias = br.com.mercadodemusica.comparacoes.Datas.diasCorridosEntreUmaDataEOutra(dataDeHoje, porcentagem.getDataDeMudancaNoSistema());
//				treeMapComparacao.put(dias, porcentagem);
//			}
//		}
//		
//		ArrayList<PorcentagemMercadoDeMusica> arrayFinal = new ArrayList<PorcentagemMercadoDeMusica>(treeMapComparacao.values());
//		PorcentagemMercadoDeMusica porcentagemFinal = arrayFinal.get(0);
//		porcentagemFinal.setAtivo(Boolean.TRUE);
//		porcentagemMercadoDeMusicaDao.update(porcentagemFinal);
//		porcentagemMercadoDeMusicaDao.flush();
//	}
//
//
	@Override
	public PorcentagemMercadoDeMusicaDTO obterPorcentagemAtiva() {
		PorcentagemMercadoDeMusica porcentagemAtiva = porcentagemMercadoDeMusicaDao.obterPorcentagemAtiva();
		PorcentagemMercadoDeMusicaDTO porcentagemMercadoDeMusicaDTO = dozerBeanMapper.map(porcentagemAtiva, PorcentagemMercadoDeMusicaDTO.class);
		porcentagemMercadoDeMusicaDTO.setPorcentagemString(new Precos().padraoAmericanoParaBrasileiro(porcentagemMercadoDeMusicaDTO.getPorcentagem()));
	
		return porcentagemMercadoDeMusicaDTO;
	}
}
