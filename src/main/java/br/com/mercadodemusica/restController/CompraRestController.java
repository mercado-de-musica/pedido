package br.com.mercadodemusica.restController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.ProdutoCompraDTO;
import br.com.mercadodemusica.exceptions.ProdutoException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;
import br.com.mercadodemusica.service.CompraVendaService;


@RestController
@CrossOrigin
@RequestMapping(value = "/compra")
public class CompraRestController {
	
	@Autowired
	private CompraVendaService compraVendaService;
	
	@RequestMapping(value = "/insercaoCompra", method = RequestMethod.POST)
	public void insercaoCompra(@RequestBody ProdutoCompraDTO produtoCompraDto) {
		
		compraVendaService.insercaoCompra(produtoCompraDto);
		
	}
	
	
	@RequestMapping(value = "/comprasEfetuadasPelosUsuariosPorProduto/{idProdutoCrypt}", method = RequestMethod.GET)
	public List<ApresentacaoProdutoDTO> comprasEfetuadasPelosUsuariosPorProduto(@PathVariable("idProdutoCrypt") String idProdutoCrypt) throws ProdutoException, ClientProtocolException, JSONException, URISyntaxException, IOException, RegraDeNegocioException {
		
		return compraVendaService.obterComprasEfetuadasPelosUsuariosPorProduto(idProdutoCrypt);
		
	}
}
