package br.com.mercadodemusica.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mercadodemusica.dto.PorcentagemMercadoDeMusicaDTO;
import br.com.mercadodemusica.service.PorcentagemService;

@RestController
@RequestMapping(value = "/porcentagem")
public class PorcentagemMercadoDeMusicaRestController {
	
	@Autowired
	private PorcentagemService porcentagemService;
	
	@RequestMapping(value = "/obterPorcentagemAtiva")
	public PorcentagemMercadoDeMusicaDTO obterPorcentagemAtiva() {
		return porcentagemService.obterPorcentagemAtiva();
	}
}
