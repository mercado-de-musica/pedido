package br.com.mercadodemusica.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import br.com.mercadodemusica.dto.ApresentacaoProdutoDTO;
import br.com.mercadodemusica.dto.ProdutoCompraDTO;
import br.com.mercadodemusica.exceptions.ProdutoException;
import br.com.mercadodemusica.exceptions.RegraDeNegocioException;

public interface CompraVendaService {

	void insercaoCompra(ProdutoCompraDTO produtoCompraDto);

	List<ApresentacaoProdutoDTO> obterComprasEfetuadasPelosUsuariosPorProduto(String idProdutoCrypt) throws ProdutoException, ClientProtocolException, JSONException, URISyntaxException, IOException, RegraDeNegocioException;
	
//	List<InfosGeraisProdutoUsuarioDTO> produtosCompradosPorUsuarioLimite(UsuarioDTO usuarioDto, Integer limite, OrdemDaListaEnum ordemDaLista);
//	
//	List<InfosGeraisProdutoUsuarioDTO> produtosVendidosPorUsuarioLimite(UsuarioDTO usuarioDto, Integer limite, OrdemDaListaEnum ordemDaLista);
//
//	CompraDTO obterCompraCompleta(CompraDTO compraDto) throws CompraVendaException;
//
//	void inserirProdutosNaCompraFeitaRealizada(CompraDTO compraDto);
//
//	void verificacaoDeUsuarios(CompraDTO compraDto, UsuarioDTO usuarioDto) throws UsuarioException;
//	
//	CompraDTO obterCompraNaoFinalizadaDoDia(UsuarioDTO usuarioDto) throws UsuarioException;
//
//	String obterValorTotal(CompraDTO compraDto);
//
//	void verificarCarrinhoVazio(CompraDTO compraDoDia) throws CarrinhoDeCompraException;
//
//	CompraDTO removerCompraSemProduto(CompraDTO compraDto);
//
//	void updateNaPoliticaDeCompra(CompraDTO compraDto, List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosComAvisoDeEntrega);
//
//	List<String> obterAnosDeVencimentoDoCartaoDeCredito();
//
//	CompraDTO obterCompraFinalizadaDoDia(UsuarioDTO usuarioDto) throws UsuarioException;
//
//	List<CompraDTO> obterListaDeComprasFinalizadasPorUsuarioComprador(UsuarioDTO usuarioDto);
//	
//	List<CompraDTO> verificarProdutosComComprasFinalizadas(List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosInativos);
//
//	List<CompraDTO> obterListaDeComprasNaoFinalizadas(UsuarioDTO usuarioDto);
//
//	String obterValorTotalSemFrete(CompraDTO compraDto);
//
//	void updateCompraNaoRealizadaComCompraDoDia(CompraDTO compraDoDiaDto, CompraDTO compraParaFinalizarDto);
//
//	List<CompraDTO> obterCompraEmAnalise();
//
//	void updateNaCompra(CompraDTO compraDto);
//
//	List<InfosGeraisProdutoUsuarioDTO> obterComprasLiberadasParaPagamento(List<InfosGeraisProdutoUsuarioDTO> listaDeProdutosFinalizadosDto) throws RegraDeNegocioException;
//
//	List<InfosGeraisProdutoUsuarioDTO> obterComprasIndisponiveisParaPagamento(List<CompraDTO> listaDeComprasFinalizadasDto);
//
//	CompraBoletoDTO obterCompraPorBoleto(CompraDTO compraDto);
//
//	CompraDTO obterCompraPorProduto(InfosGeraisProdutoUsuarioDTO infosGeraisProdutoUsuario);
//
//	CompraCartaoDeCreditoDTO obterCompraPorCartaoDeCredito(CompraDTO compraDto);
//
//	String verificacaoGatewayDePagamentoMudancaStatus(CompraDTO compraDto) throws GatewayDePagamentoException, IOException;
}
