package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.HistoricoDatasDeCompraDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.HistoricoDatasDeCompra;

@Repository
public class HistoricoDatasDeCompraDaoImpl extends DaoImpl<HistoricoDatasDeCompra> implements HistoricoDatasDeCompraDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7464953559716753568L;
	
	private final static Logger logger = Logger.getLogger(HistoricoDatasDeCompraDaoImpl.class);

	public HistoricoDatasDeCompraDaoImpl() {
		super(HistoricoDatasDeCompra.class);
	}

}
