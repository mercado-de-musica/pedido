package br.com.mercadodemusica.daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.ProdutoCompraDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Compra;
import br.com.mercadodemusica.entities.Produto;
import br.com.mercadodemusica.entities.ProdutoCompra;

@Repository
public class ProdutoCompraDaoImpl extends DaoImpl<ProdutoCompra> implements ProdutoCompraDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProdutoCompraDaoImpl() {
		super(ProdutoCompra.class);
	}

	@Override
	@Transactional(readOnly = true)
	public ProdutoCompra obterPorProduto(Produto produto) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProdutoCompra.class);
		criteria.add(Restrictions.eq("produto", produto));
		criteria.setMaxResults(1);
		return (ProdutoCompra) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<ProdutoCompra> obterPorCompra(Compra compra) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProdutoCompra.class);
		criteria.add(Restrictions.eq("compra", compra));
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<ProdutoCompra> obterListaPorProduto(Produto produto) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ProdutoCompra.class);
		criteria.add(Restrictions.eq("produto", produto));
		return criteria.list();
	}
}
