package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.CompraBoletoDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.CompraBoleto;

@Repository
public class CompraBoletoDaoImpl extends DaoImpl<CompraBoleto> implements CompraBoletoDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6925674602773800721L;
	
	private final static Logger logger = Logger.getLogger(CompraBoletoDaoImpl.class);

	public CompraBoletoDaoImpl() {
		super(CompraBoleto.class);
	}

//	@Override
//	public CompraBoleto obterCompraBoletoPorCompra(Compra compra) {
//		
//		logger.info("obterCompraBoletoPorCompra(Compra compra)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<CompraBoleto> cq = cb.createQuery(CompraBoleto.class);
//		Root<CompraBoleto> compraBoletoRoot = cq.from(CompraBoleto.class);
//		
//		cq.where(cb.equal(compraBoletoRoot.<Compra>get("compra"),cb.parameter(Compra.class, "compra")));
//		cq.select(compraBoletoRoot);
//		
//		TypedQuery<CompraBoleto> q = entityManager.createQuery(cq);
//		q.setParameter("compra", compra);
//		
//		List<CompraBoleto> lista = q.getResultList();
//		
//		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
//	}
}
