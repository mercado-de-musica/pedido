package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.ParcelamentoDeValorDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.ParcelamentoDeValor;

@Repository
public class ParcelamentoDeValorDaoImpl extends DaoImpl<ParcelamentoDeValor> implements ParcelamentoDeValorDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 477306447743862055L;
	
	private final static Logger logger = Logger.getLogger(ParcelamentoDeValorDaoImpl.class);
	
	public ParcelamentoDeValorDaoImpl() {
		super(ParcelamentoDeValor.class);
	}

//	@Override
//	public ParcelamentoDeValor obterParcelamentoPorNumeroDeParcela(BigInteger quantidadeDeParcelas) {
//		
//		logger.info("obterParcelamentoPorNumeroDeParcela(BigInteger quantidadeDeParcelas)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<ParcelamentoDeValor> cq = cb.createQuery(ParcelamentoDeValor.class);
//		Root<ParcelamentoDeValor> parcelamentoDeValorRoot = cq.from(ParcelamentoDeValor.class);
//		
//		
//		Predicate predicateVezesParceladas = cb.equal(parcelamentoDeValorRoot.<BigInteger>get("vezesParceladas"),cb.parameter(BigInteger.class, "vezesParceladas"));
//		
//		cq.where(predicateVezesParceladas);
//		cq.select(parcelamentoDeValorRoot);
//		
//		TypedQuery<ParcelamentoDeValor> q = entityManager.createQuery(cq);
//		q.setParameter("vezesParceladas", quantidadeDeParcelas);
//		
//		List<ParcelamentoDeValor> lista = q.getResultList();
//		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
//		
//	}

}
