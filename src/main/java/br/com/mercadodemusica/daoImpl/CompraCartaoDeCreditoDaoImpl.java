package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.CompraCartaoDeCreditoDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.CompraCartaoDeCredito;

@Repository
public class CompraCartaoDeCreditoDaoImpl extends DaoImpl<CompraCartaoDeCredito> implements CompraCartaoDeCreditoDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3383707442918437188L;
	
	private final static Logger logger = Logger.getLogger(CompraCartaoDeCreditoDaoImpl.class);

	public CompraCartaoDeCreditoDaoImpl() {
		super(CompraCartaoDeCredito.class);
	}

//	@Override
//	public CompraCartaoDeCredito obterCompraCartaoDeCreditoPorCompra(Compra compra) {
//		
//		logger.info("obterCompraCartaoDeCreditoPorCompra(Compra compra)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<CompraCartaoDeCredito> cq = cb.createQuery(CompraCartaoDeCredito.class);
//		Root<CompraCartaoDeCredito> compraCartaoDeCreditoRoot = cq.from(CompraCartaoDeCredito.class);
//		
//		cq.where(cb.equal(compraCartaoDeCreditoRoot.<Compra>get("compra"),cb.parameter(Compra.class, "compra")));
//		cq.select(compraCartaoDeCreditoRoot);
//		
//		TypedQuery<CompraCartaoDeCredito> q = entityManager.createQuery(cq);
//		q.setParameter("compra", compra);
//		
//		List<CompraCartaoDeCredito> lista = q.getResultList();
//		
//		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
//		
//	}
}
