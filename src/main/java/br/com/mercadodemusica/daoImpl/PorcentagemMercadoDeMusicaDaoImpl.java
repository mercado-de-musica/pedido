package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.PorcentagemMercadoDeMusicaDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.PorcentagemMercadoDeMusica;

@Repository
public class PorcentagemMercadoDeMusicaDaoImpl extends DaoImpl<PorcentagemMercadoDeMusica> implements PorcentagemMercadoDeMusicaDao{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static Logger logger = Logger.getLogger(PorcentagemMercadoDeMusicaDaoImpl.class);
	
	public PorcentagemMercadoDeMusicaDaoImpl() {
		super(PorcentagemMercadoDeMusica.class);
	}

	@Override
	@Transactional(readOnly = true)
	public PorcentagemMercadoDeMusica obterPorcentagemAtiva() {
		
		logger.info("obterPorcentagemAtiva()");
			
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(PorcentagemMercadoDeMusica.class);
		criteria.add(Restrictions.eq("ativo", Boolean.TRUE));
		criteria.setMaxResults(1);

		return (PorcentagemMercadoDeMusica) criteria.uniqueResult();
	}
}
