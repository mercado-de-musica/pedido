package br.com.mercadodemusica.daoImpl;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.mercadodemusica.dao.CompraDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Compra;

@Repository
public class CompraDaoImpl extends DaoImpl<Compra> implements CompraDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompraDaoImpl() {
		super(Compra.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Compra> obterComprasPorIdComprador(BigInteger idComprador, BigInteger limiteDeCompras) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Compra.class);
		criteria.add(Restrictions.eq("idComprador", idComprador));
		criteria.setMaxResults(limiteDeCompras.intValue());
		criteria.addOrder(Order.desc("criacao"));
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Compra> obterComprasPorIpComprador(String ipComprador, BigInteger limiteDeCompras) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Compra.class);
		criteria.add(Restrictions.eq("ipComprador", ipComprador));
		criteria.setMaxResults(limiteDeCompras.intValue());
		criteria.addOrder(Order.desc("criacao"));
		
		return criteria.list();
	}
}
