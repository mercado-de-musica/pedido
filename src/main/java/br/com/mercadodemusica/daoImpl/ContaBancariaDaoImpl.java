package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.ContaBancariaDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.ContaBancaria;

@Repository
public class ContaBancariaDaoImpl extends DaoImpl<ContaBancaria> implements ContaBancariaDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7245164481333683017L;
	
	private final static Logger logger = Logger.getLogger(ContaBancariaDaoImpl.class);
	
	public ContaBancariaDaoImpl() {
		super(ContaBancaria.class);
	}

//	@Override
//	public List<ContaBancaria> obterContasDePagamentoPorUsuario(Usuario usuario) {
//		
//		logger.info("obterContasDePagamentoPorUsuario(Usuario usuario)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<ContaBancaria> cq = cb.createQuery(ContaBancaria.class);
//		Root<ContaBancaria> contaBancariaRoot = cq.from(ContaBancaria.class);
//		
//		Predicate predicateUsuario = cb.equal(contaBancariaRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario"));
//		
//		cq.where(predicateUsuario);
//		cq.select(contaBancariaRoot);
//		
//		TypedQuery<ContaBancaria> q = entityManager.createQuery(cq);
//		q.setParameter("usuario", usuario);
//		
//		return q.getResultList();
//	}
}
