package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.CompraVendaDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.Compra;

@Repository
public class CompraVendaDaoImpl extends DaoImpl<Compra> implements CompraVendaDao {
	
	
	private static final long serialVersionUID = 1L;
	
	private final static Logger logger = Logger.getLogger(CompraVendaDaoImpl.class);
	
	public CompraVendaDaoImpl() {
		super(Compra.class);
	}

//	public List<Compra> produtosCompradosPorUsuarioLimite(Usuario usuario, Integer limite, OrdemDaListaEnum ordemDaLista) {
//	
//		logger.info("produtosCompradosPorUsuarioLimite(Usuario usuario, Integer limite, OrdemDaListaEnum ordemDaLista)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Compra> cq = cb.createQuery(Compra.class);
//		Root<Compra> compraRoot = cq.from(Compra.class);
//		
//		Predicate predicateUsuario = cb.equal(compraRoot.<Usuario>get("comprador"),cb.parameter(Usuario.class, "comprador"));
//		Predicate predicateFinalizado = cb.equal(compraRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		
//		cq.where(predicateUsuario, predicateFinalizado);
//		cq.select(compraRoot);
//		
//		if(ordemDaLista.equals(OrdemDaListaEnum.DESC)) {
//			cq.orderBy(cb.desc(compraRoot.get("id")));
//		} else {
//			cq.orderBy(cb.asc(compraRoot.get("id")));
//		}
//		
//		
//		TypedQuery<Compra> q = entityManager.createQuery(cq);
//		
//		
//		q.setParameter("comprador", usuario);
//		q.setParameter("finalizado", Boolean.TRUE);
//		q.setMaxResults(limite);
//		
//		return q.getResultList();
//	}
//	
//	@Override
//	public List<Compra> obterCompraNaoFinalizadaPorUsuarioLimit(Usuario usuario, BigInteger limite, OrdemDaListaEnum ordemDaLista) {
//		
//		logger.info("obterCompraNaoFinalizadaPorUsuarioLimit(Usuario usuario, BigInteger limite, OrdemDaListaEnum ordemDaLista)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Compra> cq = cb.createQuery(Compra.class);
//		Root<Compra> compraRoot = cq.from(Compra.class);
//		
//		Predicate predicateUsuario = cb.equal(compraRoot.<Usuario>get("comprador"),cb.parameter(Usuario.class, "comprador"));
//		Predicate predicateFinalizado = cb.equal(compraRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		
//		cq.where(predicateUsuario, predicateFinalizado);
//		cq.select(compraRoot);
//		
//		if(ordemDaLista.equals(OrdemDaListaEnum.DESC)) {
//			cq.orderBy(cb.desc(compraRoot.get("id")));
//		} else {
//			cq.orderBy(cb.asc(compraRoot.get("id")));
//		}
//		
//		
//		TypedQuery<Compra> q = entityManager.createQuery(cq);
//		
//		q.setMaxResults(limite.intValue());
//		q.setParameter("comprador", usuario);
//		q.setParameter("finalizado", Boolean.FALSE);
//		
//		return q.getResultList();
//	}
//
//	@Override
//	public List<Compra> listaDeCompraDoDia(Usuario usuario) {
//		
//		logger.info("listaDeCompraDoDia(Usuario usuario)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Compra> cq = cb.createQuery(Compra.class);
//		Root<Compra> compraRoot = cq.from(Compra.class);
//		
//		Predicate predicateUsuario = cb.equal(compraRoot.<Usuario>get("comprador"),cb.parameter(Usuario.class, "comprador"));
//		Predicate predicateFinalizadoFalse = cb.equal(compraRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		
//		cq.where(predicateUsuario, predicateFinalizadoFalse);
//		cq.select(compraRoot);
//		
//		
//		TypedQuery<Compra> q = entityManager.createQuery(cq);
//		
//		q.setParameter("comprador", usuario);
//		q.setParameter("finalizado", Boolean.FALSE);
//		
//		return q.getResultList();
//	}
//
//	@Override
//	public List<Compra> obterCompraEmAnalise() {
//		
//		logger.info("obterCompraEmAnalise()");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Compra> cq = cb.createQuery(Compra.class);
//		Root<Compra> compraRoot = cq.from(Compra.class);
//		
//		Predicate predicateFinalizado = cb.equal(compraRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		Predicate predicateStatusTransacaoEmMonitoramento = cb.equal(compraRoot.<StatusTransacaoEnum>get("statusTransacao"),cb.parameter(StatusTransacaoEnum.class, "statusTransacao1"));
//		Predicate predicateStatusTransacaoAguardando = cb.equal(compraRoot.<StatusTransacaoEnum>get("statusTransacao"),cb.parameter(StatusTransacaoEnum.class, "statusTransacao2"));
//		Predicate predicateStatusTransacaoEmRecupercacao = cb.equal(compraRoot.<StatusTransacaoEnum>get("statusTransacao"),cb.parameter(StatusTransacaoEnum.class, "statusTransacao3"));
//		
//		Predicate predicateOrStatusTransacao = cb.or(predicateStatusTransacaoEmMonitoramento, predicateStatusTransacaoAguardando, predicateStatusTransacaoEmRecupercacao);
//		
//		cq.where(predicateOrStatusTransacao, predicateFinalizado);
//		cq.select(compraRoot);
//		
//		
//		TypedQuery<Compra> q = entityManager.createQuery(cq);
//		
//		q.setParameter("finalizado", Boolean.TRUE);
//		q.setParameter("statusTransacao1", StatusTransacaoEnum.EM_MONITORAMENTO);
//		q.setParameter("statusTransacao2", StatusTransacaoEnum.AGUARDANDO_PAGAMENTO);
//		q.setParameter("statusTransacao3", StatusTransacaoEnum.EM_RECUPERACAO);
//		
//		
//		return q.getResultList();
//	}
//
//	@Override
//	public List<Compra> obterListaDeCompraFinalizadaPorComprador(Usuario usuario) {
//		
//		logger.info("obterListaDeCompraFinalizadaPorComprador(Usuario usuario)");
//		
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Compra> cq = cb.createQuery(Compra.class);
//		Root<Compra> compraRoot = cq.from(Compra.class);
//		
//		Predicate predicateUsuario = cb.equal(compraRoot.<Usuario>get("comprador"),cb.parameter(Usuario.class, "comprador"));
//		Predicate predicateFinalizado = cb.equal(compraRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		
//		
//		
//		Predicate predicateStatusTransacaoPrimeira = cb.equal(compraRoot.<StatusTransacaoEnum>get("statusTransacao"),cb.parameter(StatusTransacaoEnum.class, "statusTransacao1"));
//		Predicate predicateStatusTransacaoSegunda = cb.equal(compraRoot.<StatusTransacaoEnum>get("statusTransacao"),cb.parameter(StatusTransacaoEnum.class, "statusTransacao2"));
//		Predicate predicateStatusTransacaoTerceira = cb.equal(compraRoot.<StatusTransacaoEnum>get("statusTransacao"),cb.parameter(StatusTransacaoEnum.class, "statusTransacao3"));
//		Predicate predicateStatusTransacaoQuarta = cb.equal(compraRoot.<StatusTransacaoEnum>get("statusTransacao"),cb.parameter(StatusTransacaoEnum.class, "statusTransacao4"));
//		
//		Predicate predicateOrStatusTransacao = cb.or(predicateStatusTransacaoPrimeira, predicateStatusTransacaoSegunda, predicateStatusTransacaoTerceira, predicateStatusTransacaoQuarta);
//		
//		
//		
//		cq.where(predicateUsuario, predicateFinalizado, predicateOrStatusTransacao);
//		cq.select(compraRoot);
//		
//		TypedQuery<Compra> q = entityManager.createQuery(cq);
//		
//		q.setParameter("comprador", usuario);
//		q.setParameter("statusTransacao1", StatusTransacaoEnum.EM_MONITORAMENTO);
//		q.setParameter("statusTransacao2", StatusTransacaoEnum.APROVADO);
//		q.setParameter("statusTransacao3", StatusTransacaoEnum.AGUARDANDO_PAGAMENTO);
//		q.setParameter("statusTransacao4", StatusTransacaoEnum.EM_RECUPERACAO);
//		q.setParameter("finalizado", Boolean.TRUE);
//		
//		
//		return q.getResultList();
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<InfosGeraisProdutoUsuario> produtosVendidosPorUsuarioLimite(Usuario usuario, Integer limite, OrdemDaListaEnum ordemDaLista) {
//		
//		logger.info("produtosVendidosPorUsuarioLimite(Usuario usuario, Integer limite, OrdemDaListaEnum ordemDaLista)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<InfosGeraisProdutoUsuario> cq = cb.createQuery(InfosGeraisProdutoUsuario.class);
//
//		Metamodel m = entityManager.getMetamodel();
//		EntityType<InfosGeraisProdutoUsuario> infosGeraisProdutoUsuario_ = m.entity(InfosGeraisProdutoUsuario.class);
//		
//		Root<InfosGeraisProdutoUsuario> infosGeraisProdutoUsuarioRoot = cq.from(InfosGeraisProdutoUsuario.class);
//		
//		Join<InfosGeraisProdutoUsuario, Compra> compraRoot = (Join<InfosGeraisProdutoUsuario, Compra>) infosGeraisProdutoUsuarioRoot.join(infosGeraisProdutoUsuario_.getSingularAttribute("compra"));
//	
//		Predicate predicateCompraFinalizadaTrue = cb.equal(compraRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		Predicate predicateProdutoInativo = cb.equal(infosGeraisProdutoUsuarioRoot.<Boolean>get("ativo"),cb.parameter(Boolean.class, "ativo"));
//		Predicate predicateStatusTransacao = cb.equal(compraRoot.<StatusTransacaoEnum>get("statusTransacao"),cb.parameter(StatusTransacaoEnum.class, "statusTransacao"));
//		Predicate predicateUsuario = cb.equal(infosGeraisProdutoUsuarioRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario"));
//		
//		cq.where(predicateCompraFinalizadaTrue, predicateProdutoInativo, predicateStatusTransacao, predicateUsuario);
//		cq.select(infosGeraisProdutoUsuarioRoot);
//	
//		TypedQuery<InfosGeraisProdutoUsuario> q = entityManager.createQuery(cq);
//		
//		q.setParameter("finalizado", Boolean.TRUE);
//		q.setParameter("ativo", Boolean.FALSE);
//		q.setParameter("statusTransacao", StatusTransacaoEnum.APROVADO);
//		q.setParameter("usuario", usuario);
//		q.setMaxResults(limite);
//		
//		if(ordemDaLista.equals(OrdemDaListaEnum.DESC)) {
//			cq.orderBy(cb.desc(infosGeraisProdutoUsuarioRoot.get("id")));
//		} else {
//			cq.orderBy(cb.asc(infosGeraisProdutoUsuarioRoot.get("id")));
//		}
//		
////		String query = q.unwrap(org.hibernate.Query.class).getQueryString();
////		System.out.println(query);
//		
//		return q.getResultList();
//	}
}
