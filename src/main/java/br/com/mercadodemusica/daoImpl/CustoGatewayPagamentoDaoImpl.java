package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.CustoGatewayPagamentoDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.CustoGatewayPagamento;

@Repository
public class CustoGatewayPagamentoDaoImpl extends DaoImpl<CustoGatewayPagamento> implements CustoGatewayPagamentoDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5776335123905592450L;
	
	private final static Logger logger = Logger.getLogger(CustoGatewayPagamentoDaoImpl.class);

	public CustoGatewayPagamentoDaoImpl() {
		super(CustoGatewayPagamento.class);
	}

//	@Override
//	public List<CustoGatewayPagamento> obterCustoDePagamentos() {
//
//		logger.info("obterCustoDePagamentos()");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<CustoGatewayPagamento> cq = cb.createQuery(CustoGatewayPagamento.class);
//		Root<CustoGatewayPagamento> custoGatewayPagamentoRoot = cq.from(CustoGatewayPagamento.class);
//		
//		cq.select(custoGatewayPagamentoRoot);
//		TypedQuery<CustoGatewayPagamento> q = entityManager.createQuery(cq);
//		
//		return q.getResultList();
//	}
	
	
}
