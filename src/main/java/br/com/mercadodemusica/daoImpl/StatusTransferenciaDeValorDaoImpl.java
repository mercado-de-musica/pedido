package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.StatusTransferenciaDeValorDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.StatusTransferenciaDeValor;

@Repository
public class StatusTransferenciaDeValorDaoImpl extends DaoImpl<StatusTransferenciaDeValor> implements StatusTransferenciaDeValorDao{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4491778424271241028L;
	
	private final static Logger logger = Logger.getLogger(StatusTransferenciaDeValorDaoImpl.class);

	public StatusTransferenciaDeValorDaoImpl() {
		super(StatusTransferenciaDeValor.class);
	}

//	@Override
//	public List<StatusTransferenciaDeValor> obterStatusTransferenciaPorTransferencia(TransferenciaValoresUsuario transferenciaUsuario) {
//		
//		logger.info("obterStatusTransferenciaPorTransferencia(TransferenciaValoresUsuario transferenciaUsuario)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<StatusTransferenciaDeValor> cq = cb.createQuery(StatusTransferenciaDeValor.class);
//		Root<StatusTransferenciaDeValor> statusTransferenciaDeValorRoot = cq.from(StatusTransferenciaDeValor.class);
//		
//		Predicate predicateTransferencia = cb.equal(statusTransferenciaDeValorRoot.<TransferenciaValoresUsuario>get("transferenciaValoresUsuario"),cb.parameter(TransferenciaValoresUsuario.class, "transferenciaValoresUsuario"));
//		
//		cq.where(predicateTransferencia);
//		cq.select(statusTransferenciaDeValorRoot);
//		
//		TypedQuery<StatusTransferenciaDeValor> q = entityManager.createQuery(cq);
//		
//		q.setParameter("transferenciaValoresUsuario", transferenciaUsuario);
//		
//		return q.getResultList();		
//	}
}
