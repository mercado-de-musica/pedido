package br.com.mercadodemusica.daoImpl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import br.com.mercadodemusica.dao.TransferenciaValoresUsuarioDao;
import br.com.mercadodemusica.daoGeneric.DaoImpl;
import br.com.mercadodemusica.entities.TransferenciaValoresUsuario;

@Repository
public class TransferenciaValoresUsuarioDaoImpl extends DaoImpl<TransferenciaValoresUsuario> implements TransferenciaValoresUsuarioDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7854092568560499957L;
	
	private final static Logger logger = Logger.getLogger(StatusTransferenciaDeValorDaoImpl.class);
	
	public TransferenciaValoresUsuarioDaoImpl() {
		super(TransferenciaValoresUsuario.class);
	}

//	@Override
//	public List<TransferenciaValoresUsuario> obterTransferenciaPorUsuario(Usuario usuario) {
//		
//		logger.info("obterTransferenciaPorUsuario(Usuario usuario)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<TransferenciaValoresUsuario> cq = cb.createQuery(TransferenciaValoresUsuario.class);
//		Root<TransferenciaValoresUsuario> transferenciaValoresUsuarioRoot = cq.from(TransferenciaValoresUsuario.class);
//		
//		Predicate predicateUsuario = cb.equal(transferenciaValoresUsuarioRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario"));
//		
//		cq.where(predicateUsuario);
//		cq.select(transferenciaValoresUsuarioRoot);
//		
//		TypedQuery<TransferenciaValoresUsuario> q = entityManager.createQuery(cq);
//		q.setParameter("usuario", usuario);
//		
//		return q.getResultList();
//	}
//	
//	@Override
//	public TransferenciaValoresUsuario obterTransferenciaEmAbertoPorUsuario(Usuario usuario, Boolean desistencia) {
//		
//		logger.info("obterTransferenciaEmAbertoPorUsuario(Usuario usuario, Boolean desistencia)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<TransferenciaValoresUsuario> cq = cb.createQuery(TransferenciaValoresUsuario.class);
//		Root<TransferenciaValoresUsuario> transferenciaValoresUsuarioRoot = cq.from(TransferenciaValoresUsuario.class);
//		
//		Predicate predicateUsuario = cb.equal(transferenciaValoresUsuarioRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario"));
//		Predicate predicateFinalizado = cb.equal(transferenciaValoresUsuarioRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		Predicate predicateDesistencia = cb.equal(transferenciaValoresUsuarioRoot.<Boolean>get("desistencia"),cb.parameter(Boolean.class, "desistencia"));
//		
//		cq.where(predicateUsuario, predicateFinalizado, predicateDesistencia);
//		cq.select(transferenciaValoresUsuarioRoot);
//		
//		TypedQuery<TransferenciaValoresUsuario> q = entityManager.createQuery(cq);
//		
//		q.setParameter("usuario", usuario);
//		q.setParameter("finalizado", Boolean.FALSE);
//		q.setParameter("desistencia", desistencia);
//		
//		List<TransferenciaValoresUsuario> lista = q.getResultList();
//		
//		return CollectionUtils.isEmpty(lista) ? null : lista.get(0);
//	}
//
//	@Override
//	public List<TransferenciaValoresUsuario> obterListaTransferenciaEmAbertoPorUsuario(Usuario usuario) {
//		
//		logger.info("obterListaTransferenciaEmAbertoPorUsuario(Usuario usuario)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<TransferenciaValoresUsuario> cq = cb.createQuery(TransferenciaValoresUsuario.class);
//		Root<TransferenciaValoresUsuario> transferenciaValoresUsuarioRoot = cq.from(TransferenciaValoresUsuario.class);
//		
//		Predicate predicateUsuario = cb.equal(transferenciaValoresUsuarioRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario"));
//		Predicate predicateFinalizado = cb.equal(transferenciaValoresUsuarioRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		
//		cq.where(predicateUsuario, predicateFinalizado);
//		cq.select(transferenciaValoresUsuarioRoot);
//		
//		TypedQuery<TransferenciaValoresUsuario> q = entityManager.createQuery(cq);
//		
//		q.setParameter("usuario", usuario);
//		q.setParameter("finalizado", Boolean.FALSE);
//				
//		return q.getResultList();
//	}
//
//	@Override
//	public List<TransferenciaValoresUsuario> obterTransferenciaEmAberto() {
//		
//		logger.info("obterTransferenciaEmAberto()");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<TransferenciaValoresUsuario> cq = cb.createQuery(TransferenciaValoresUsuario.class);
//		Root<TransferenciaValoresUsuario> transferenciaValoresUsuarioRoot = cq.from(TransferenciaValoresUsuario.class);
//		
//		Predicate predicateFinalizado = cb.equal(transferenciaValoresUsuarioRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		
//		cq.where(predicateFinalizado);
//		cq.select(transferenciaValoresUsuarioRoot);
//		
//		TypedQuery<TransferenciaValoresUsuario> q = entityManager.createQuery(cq);
//		
//		q.setParameter("finalizado", Boolean.FALSE);
//		
//		return q.getResultList();
//	}
//	
//	@Override
//	public List<TransferenciaValoresUsuario> obterTransferenciaFinalizadasPorUsuario(Usuario usuario) {
//		
//		
//		logger.info("obterTransferenciaFinalizadasPorUsuario(Usuario usuario)");
//		
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<TransferenciaValoresUsuario> cq = cb.createQuery(TransferenciaValoresUsuario.class);
//		Root<TransferenciaValoresUsuario> transferenciaValoresUsuarioRoot = cq.from(TransferenciaValoresUsuario.class);
//		
//		Predicate predicateUsuario = cb.equal(transferenciaValoresUsuarioRoot.<Usuario>get("usuario"),cb.parameter(Usuario.class, "usuario"));
//		Predicate predicateFinalizado = cb.equal(transferenciaValoresUsuarioRoot.<Boolean>get("finalizado"),cb.parameter(Boolean.class, "finalizado"));
//		
//		cq.where(predicateUsuario, predicateFinalizado);
//		cq.select(transferenciaValoresUsuarioRoot);
//		
//		TypedQuery<TransferenciaValoresUsuario> q = entityManager.createQuery(cq);
//		
//		q.setParameter("usuario", usuario);
//		q.setParameter("finalizado", Boolean.TRUE);
//		
//		return q.getResultList();
//	}
//
//	@Override
//	public TransferenciaValoresUsuario obterPorInfosGeraisProdutoUsuario(InfosGeraisProdutoUsuario infosGeraisProdutoUsuario) {
//		
//		logger.info("obterPorInfosGeraisProdutoUsuario(InfosGeraisProdutoUsuario infosGeraisProdutoUsuario)");
//		
//		Session session = this.entityManager.unwrap(Session.class);
//		
//		SQLQuery query = session.createSQLQuery("SELECT transferenciaValoresUsuario.* from transferencia_valores_usuario as transferenciaValoresUsuario inner join "
//				+ "transferencia_valores_usuario_infos_gerais_produto_usuario on "
//				+ "transferenciaValoresUsuario.id = transferencia_valores_usuario_infos_gerais_produto_usuario.transferencia_valores_usuario_id where "
//				+ "transferencia_valores_usuario_infos_gerais_produto_usuario.listainfosgeraisprodutousuario_id = " + infosGeraisProdutoUsuario.getId()).addEntity(TransferenciaValoresUsuario.class);
//		
//		return (TransferenciaValoresUsuario) query.uniqueResult();
//	}
}
